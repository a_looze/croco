<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Config variables for models
    |--------------------------------------------------------------------------
    |
    | Allow to use model data with view data output
    | for example model store "status" field that could be 1 or 0
    | in view form its equivalent to "yes" or "no" respectively
    | so status is array [0 => "no", 1 => yes]
    |
    */

    'test' => 'data',

    'users' => [],

    'articles' => [
        'visible' => [
            0 => 'main.article.no',
            1 => 'main.article.yes',
        ],
        'commented' => [
            0 => 'main.article.no',
            1 => 'main.article.yes',
        ],
        'type_id' => [
            1 => 'main.article.audio',
            2 => 'main.article.video',
            3 => 'main.article.image',
        ],
    ],

    'files' => [
        'type' => [
            1 => 'main.article.video',
            2 => 'main.article.audio',
            3 => 'main.article.image',
        ]
    ]

];
