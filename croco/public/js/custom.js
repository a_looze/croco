$(function() {

    if(!window.FormData && !window.File) {
        alert('NO FILE API!');
        console.log('NO FILE API!');
    }

    // VALIDATION RULES
    var fileImageTypes = [
        'image/png',
        'image/jpeg',
        'image/jpg',
        'image/png'
    ];

    var $loader = $('#loader-wrapper');

    function showHideAlert(target, msg, action)
    {
        var $target = $('#'+target);
        $target.text(msg);
        if (action == 'toggle') {
            $target.toggle();
        } else if (action == 'show') {
            $target.show();
        } else if (action == 'hide') {
            $target.hide();
        }
    }

    function checkImage(fileType)
    {
        for (var j = 0; j < fileImageTypes.length; j++) {
            var ext = fileImageTypes[j];
            if (fileType.toLowerCase() == ext.toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    /**
     * draw preview
     *
     * @param file
     * @param target
     * @param from
     */
    function drawPreview(file, target, from)
    {
        // from input or lib
        if (from == 'input') {
            $('#'+target).attr('src', file);
            /*if (file) {
                var reader = new FileReader();
                reader.readAsDataURL(file, 'UTF-8');
                reader.onload = function (event) {
                    var result = event.target.result;
                    $('#'+target).attr('src', event.target.result);

                };
            }*/
        } else {
            $('#'+target).attr('src', file);
        }
    }

    /***** FILE UPLOAD ******/

    // NAVIGATION

    // in file lib blocks
    $('.show-lib-block').on('click', function() {
        $('.lib-block').show();
        $('.link-block').hide();
    });

    // GET CATEGORY

    $('.cat').on('click', function() {
        $("input[name='category_id']").val($(this).data('id'));
        $('.step1').hide();
        $('.step50').toggleClass('step50 step100');
        $('.step-long').addClass('step50');
        $('.step2').show();
        console.log('click');
    });

    /* CONFIG */
    var prev = [];
    var prev_from_lib;
    var $previewInput = $('.preview-from-input'); // input button
    var $libLink = $('.show-file-lib'); // link to show modal
    var $inputButton = $('.input-button');
    var $canselFile = $('.cansel-file');



    /* PREVIEW FROM LIB OR INPUT */

    // trigger for $previewInput
    $inputButton.on('click', function () {
        $('#'+$(this).data('target')).trigger('click');
        return false;
    });

    // SET PREVIEW FROM INPUT
    $previewInput.on('change', function (e) {

        var pr = e.currentTarget.files[0]; //Files[0] = 1st file
        var $inp = $(this);

        if (pr) {
            prev[0] = pr;
            var reader = new FileReader();
            reader.readAsDataURL(pr, 'UTF-8');
            reader.onload = function (event) {

                // validation
                if ($inp.data('error').length) {
                    // validate type
                    if (!checkImage(pr.type)) {
                        // @todo: translate
                        showHideAlert($inp.data('error'), 'не подходящего типа', 'show');
                        return false;
                    }
                    // validate size
                    if (pr.size > 1024 * 1024 * 19) {
                        // @todo: translate
                        showHideAlert($inp.data('error'), 'файл слишком большой', 'show');
                        return false;
                    }
                }

                // hide errors
                showHideAlert('error-file', '', 'hide');
                // show file name
                $('.preview-info').text(pr.name);

                // remove id preview from hidden form input
                $('#' + $($inp).data('remove')).val('');
                // hide msg block
                $('.msg-block-preview').hide();

                // draw preview
                if ($($inp).data('draw').length) {
                    console.log('draw from input');
                    console.log($($inp).data('draw'));
                    drawPreview(event.target.result, $($inp).data('draw'), 'input');
                }

            };
        }

    });


    /* FILES FROM LIB IN MODAL */

    $libLink.on('click', function (e) {
        console.log('click');
        var $libModalId = $(this).data('modal');
        $('.'+$libModalId).modal('show');
        $('.'+$libModalId).data('link', $(this).attr('id'));
        return false;
    });

    // file item
    $('.lib-item').on('click', function (e) {
        // get the modal
        var $thisModal = $(this).closest('.modal');
        // get config link
        var $config = $('#'+$thisModal.data('link'));
        // set data to hide input
        $('#'+$config.data('target')).val($(this).data('id'));
        // remove data from input
        $('#'+$config.data('remove')).val('');

        if ($config.data('action') == 'preview') {
            prev.shift();

            //
            if ($config.data('draw').length) {
                var imageFromLib = $(this).find('img').attr('src');
                drawPreview(imageFromLib, $config.data('draw'), 'lib');
                console.log('draw from lib');
            }

            // show msg block
            $('.msg-block-preview').show();

        } else {
            // hide preview input and link to lib if type == 3; remove data
            // show if not
            if ($(this).data('type') == 3) {
                prev.shift();
                $previewInput.prop('disabled', true).val('');
                $libLink.each(function(){
                    if ($(this).data('action') == 'preview') $(this).parent().hide();
                });
                // hide msg for preview
                $('.msg-block-preview').hide();
            } else {
                $previewInput.prop('disabled', false);
                $libLink.parent().show();
            }

            // show msg block
            $('.msg-block').show();
        }

        $thisModal.modal('hide');

        return false;
    });

    // hide msg and remove value
    $canselFile.on('click', function() {
        $(this).parent().hide();
        $('#'+$(this).data('remove')).val('');
        var $canselAction = $(this).data('action');
        if ($canselAction == 'file') {
            $libLink.parent().show();
        }
        return false;
    });


    /**************************************** */
    /*                ARTICLE                 */
    /**************************************** */

    /* ADD FILE FOR ARTICLE FORM INPUT */

    Dropzone.options.up = {

        uploadMultiple: false,
        parallelUploads: 1,
        autoProcessQueue: false,
        maxFiles: 1,
        clickable: '#add-file, #up',
        accept: function(file, done) {
            // hide msg block
            $('.msg-block').hide();

            done();
        },
        success: function(file, response) {

            $loader.hide();

            if (response.code == 422) {
                showHideAlert('error-article', response.error, 'show');
            }

            if (response.code == 201) {
                // steps
                $('.step-long').toggleClass('step50 step100');
                window.location.href = response.action;
            }

            // hide msg
            $('.msg-block').hide();

        },

        error: function(file, response) {

            $loader.hide();

            // msg from validation
            if (response.file && response.file.length) {
                showHideAlert('error-article', response.file[0], 'show');
            }

        },

        init: function() {
            var myDropzone = this;
            var submitButton = document.querySelector("#submit-all");

            submitButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $loader.show();

                if (myDropzone.getQueuedFiles().length > 0) {
                    console.log(myDropzone.getQueuedFiles().length);
                    myDropzone.processQueue();
                } else {
                    // if prev
                    if ($('#up').find("input[name='file_from_lib']").val()) {
                        console.log('file from lib ' + $('#up').find("input[name='file_from_lib']").val());
                        var fl = {status: 'added'};
                        myDropzone.uploadFiles([fl]); //send file
                    } else {
                        $loader.hide();
                        // @todo: translate msg
                        showHideAlert('error-article', 'Добавьте файл!', 'show');
                    }
                }
                return false;
            });

            this.on("maxfilesexceeded", function(file){
                this.removeAllFiles();
                this.addFile(file);
                return false;
            });

        }

    };


    /* ADD ARTICLE PREVIEW */

    Dropzone.options.preview = {

        uploadMultiple: false,
        parallelUploads: 1,
        autoProcessQueue: false,
        maxFiles: 1,
        clickable: '#add-preview, #preview',
        accept: function(file, done) {
            // hide msg block
            $('.msg-block').hide();

            done();
        },
        success: function(file, response){
            if(response.code == 422) {
                showHideAlert('error-preview', response.error, 'show');
            }

            // steps
            $('.step2-2').hide();
            $('.step2-3').show();
            $('.step-long').toggleClass('step50 step75');

            // hide msg
            $('.msg-block').hide();
        },

        error: function(file, response) {
            // msg from validation
            if (response.file && response.file.length) {
                showHideAlert('error-preview', response.file[0], 'show');
            }

        },

        init: function() {
            var myDropzone = this;
            var submitButton = document.querySelector("#submit-preview");

            submitButton.addEventListener("click", function(e) {
                myDropzone.options.url = $('#preview').attr('action');
                e.preventDefault();
                e.stopPropagation();
                if (myDropzone.getQueuedFiles().length > 0) {
                    console.log(myDropzone.getQueuedFiles().length);
                    myDropzone.processQueue();
                } else {
                    if ($('#preview').find("input[name='file_from_lib']").val()) {
                        console.log('file from lib ' + $('#preview').find("input[name='file_from_lib']").val());
                        var fl = {status: 'added'};
                        myDropzone.uploadFiles([fl]); //send file
                    } else {
                        var fl = {status: 'added'};
                        myDropzone.uploadFiles([fl]);
                    }
                }
                return false;
            });

            this.on("maxfilesexceeded", function(file){
                this.removeAllFiles();
                this.addFile(file);
                return false;
            });

            this.on("sending", function(file, xhr, data) {
                data.append("tags", $('.tags').val());
            });

        }

    };


    /* ADD ANSWER */

    Dropzone.options.answer = {

        uploadMultiple: false,
        parallelUploads: 1,
        autoProcessQueue: false,
        maxFiles: 1,
        clickable: '#add-answer, #answer',
        accept: function(file, done) {

            // check if empty prev and $previewInput
            console.log('check if empty prev ' + prev.length);
            console.log('check if empty $previewInput ' + $previewInput.val());

            // hide msg block
            $('.msg-block').hide();

            done();
        },
        success: function(file, response) {

            $loader.hide();

            if (response.code == 422) {
                showHideAlert('error-answer', response.error, 'show');
            }

            // reload page
            if (response.code == 201 && response.type == 'front') {
                window.location.href = response.action;

                // if article show page
                if ($(".submit-answer-front").length) {
                    // steps
                    $('#add-answer-front').hide();
                    $('#success-answer').show();
                }
            }

            if (response.code == 201 && response.type == 'profile') {
                // profile
                if ($("#answer-create").length) {
                    // steps
                    $('#answer-create').hide();
                    $('#answer-create').html('');
                    $('#success-answer').show();

                    $('#create-answer-button').hide();
                    $('#edit-answer-button').attr('href', response.action).show();
                }
            }

            // hide msg block
            $('.msg-block').hide();
            $('.msg-block-preview').hide();

        },

        error: function(file, response) {

            $loader.hide();

            var msg = '';
            // msg from validation = file
            if (response.file && response.file.length) {
                msg = msg + response.file[0];
            }
            showHideAlert('error-answer', msg, 'show');
        },

        init: function() {
            var myDropzone = this;
            var submitButton = document.querySelector("#submit-answer");

            submitButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $loader.show();

                if (myDropzone.getQueuedFiles().length > 0) {
                    console.log(myDropzone.getQueuedFiles().length);
                    myDropzone.processQueue();
                } else if ($('#answer').find("input[name='file_from_lib']").val()) {
                    console.log('file from lib ' + $('#answer').find("input[name='file_from_lib']").val());
                    var fl = { status: 'added' };
                    myDropzone.uploadFiles([fl]); //send empty
                } else {
                    $loader.hide();
                    // @todo: translate msg
                    showHideAlert('error-answer', 'Добавьте файл!', 'show');
                }
                return false;
            });

            this.on("maxfilesexceeded", function(file){
                this.removeAllFiles();
                this.addFile(file);
                return false;
            });

            this.on("sending", function(file, xhr, data) {
                // add preview
                if (prev.length > 0) {
                    for(i=0;i<prev.length;i++) {
                        data.append('preview', prev[i]);
                    }
                }
            });

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
            });

        }

    };

    /**************************************** */
    /*            END ARTICLE                 */
    /**************************************** */


    /******************************************/
    /*      AVATAR, BANNER CROP               */
    /******************************************/


    // hide (avatar or banner) link edit
    var mediaUser = '';
    $('.mediauser').on('click', function () {
        mediaUser = $(this).data('name');
    });

    // transform cropper dataURI output to a Blob which Dropzone accepts
    function dataURItoBlob(dataURI) {
        var byteString = atob(dataURI.split(',')[1]);
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], { type: 'image/jpeg' });
    }

    var fileToSend = [];

    // modal window template
    var modalTemplate = $('.usermedia-module');

    Dropzone.options.up2 = {

        uploadMultiple: false,
        parallelUploads: 1,
        autoProcessQueue: false,
        maxFiles: 1,
        url: 'profile/usermedia',
        clickable: '.headerEdit, .avatarEdit',
        accept: function(file, done) {

            // hide other link
            if (mediaUser == 'avatar') {
                $('.headerEdit').hide();
            } else {
                $('.avatarEdit').hide();
            }

            $loader.show();

            // initialize FileReader which reads uploaded file
            var reader = new FileReader();
            //reader.readAsDataURL(file);
            reader.readAsDataURL(file);
            reader.onloadend = function () {
                // add uploaded and read image to modal
                //$cropperModal.find('.image-container').html($img);
                //$img.attr('src', reader.result);

                // banner and avatar crop containers
                if (mediaUser == 'banner') {
                    $('#contentHeader').css("background", 'url('+reader.result+') no-repeat');
                    console.log('banner');
                } else {
                    $('.userAvatar').attr('src', reader.result);
                    console.log('banner');
                }


            };

            $loader.hide();

            $('#submit-banner').show();

            done();
        },
        success: function(file, response) {

            $loader.hide();

            if (response.code == 401) {
                alert('error');
            } else if (response.code == 201) {
                // show edit link and hide submit button
                $('.mediauser').show();
                $('#submit-banner').hide();
            }
        },

        error: function(file, response) {
            //alert(response[mediaUser]);
        },

        init: function() {
            var myDropzone = this;
            var submitButton = document.querySelector("#submit-banner");

            submitButton.addEventListener("click", function(e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();

                $loader.show();

                myDropzone.processQueue();
                return false;
            });

            this.on("maxfilesexceeded", function(file){
                console.log("No more files please!");
                this.removeAllFiles();
                this.addFile(file);
                return false;
            });

            this.on("sending", function(file, xhr, data) {
                 data.append(mediaUser, file);
            });

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
            });

        }

    };

    /******************************************/
    /*       END AVATAR, BANNER CROP          */
    /******************************************/


    /******************************************/
    /*        ADD FILE TO LIB                 */
    /******************************************/

    Dropzone.options.lib = {

        uploadMultiple: false,
        parallelUploads: 1,
        autoProcessQueue: false,
        maxFiles: 1,
        clickable: '#add-file-to-lib, #lib',
        accept: function(file, done) {

            // hide msg block
            $('.msg-block').hide();

            // if image - hide lib link; else - show
            if (checkImage(file.type)) {
                $libLink.each(function(){
                   if ($(this).data('action') == 'preview') $(this).parent().hide();
                });
                $previewInput.prop('disabled', true);
                // clear value
                prev.shift();
                $previewInput.val();
                $('#'+$libLink.data('target')).val('');
            } else {
                $libLink.parent().show();
                $previewInput.prop('disabled', false);
            }
            done();
        },
        success: function(file, response){

            $loader.hide();

            if (response.code == 422) {
                showHideAlert('error-file', response.error, 'show');
            } else if (response.code == 201) {
                window.location.href = response.action;
            }
        },

        error: function(file, response) {

            $loader.hide();

            var msg = '';
            // msg from validation = file
            if (response.file && response.file.length) {
                msg = msg + response.file[0];
            }
            // msg from validation = preview
            if (response.preview && response.preview.length) {
                msg = msg + "\n" + response.preview[0];
            }
            showHideAlert('error-file', msg, 'show');
        },

        init: function() {
            var myDropzone = this;
            var submitButton = document.querySelector("#submit-lib");

            submitButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $loader.show();

                if (myDropzone.getQueuedFiles().length > 0) {
                    console.log(myDropzone.getQueuedFiles().length);
                    myDropzone.processQueue();
                } else {
                    $loader.hide();
                    // @todo: translate msg
                    showHideAlert('error-file', 'Добавьте файл!', 'show');
                }
                return false;
            });

            this.on("maxfilesexceeded", function(file){
                this.removeAllFiles();
                this.addFile(file);
                return false;
            });

            this.on("sending", function(file, xhr, data) {
                if (prev.length > 0) {
                    for(i=0;i<prev.length;i++) {
                        data.append('preview', prev[i]);
                    }
                }

            });

        }

    };

    /******************************************/
    /*       END ADD FILE TO LIB              */
    /******************************************/


    /**************************/
    /*        COMMENT         */
    /**************************/

    Dropzone.options.comment = {

        uploadMultiple: false,
        parallelUploads: 1,
        autoProcessQueue: false,
        maxFiles: 1,
        clickable: '#add-comment, #comment',
        accept: function(file, done) {

            // check if empty prev and $previewInput
            console.log('check if empty prev ' + prev.length);
            console.log('check if empty $previewInput ' + $previewInput.val());

            // hide msg block
            $('.msg-block').hide();

            // if image - hide lib link; else - show
            if (checkImage(file.type)) {
                $libLink.each(function(){
                    if ($(this).data('action') == 'preview') $(this).parent().hide();
                });
                $previewInput.prop('disabled', true);
                // clear value
                prev.shift();
                $previewInput.val('');
                $('#'+$libLink.data('target')).val('');
            } else {
                $libLink.parent().show();
                $previewInput.prop('disabled', false);
            }

            done();
        },
        success: function(file, response){

            $loader.hide();

            // @todo: make html modal alerts
            if(response.code == 422) {
                showHideAlert('error-comment', response.error, 'show');
            }

            // reload page
            if(response.code == 201) {
                $('#success-comment').show();
                location.reload();
            }

        },

        error: function(file, response) {

            $loader.hide();

            var msg = '';
            // msg from validation = file
            if (response.file && response.file.length) {
                msg = msg + response.file[0];
            }
            // msg from validation = preview
            if (response.preview && response.preview.length) {
                msg = msg + "\n" + response.preview[0];
            }

            if (response.content && response.content.length) {
                msg = msg + "\n" + response.content[0];
            }

            showHideAlert('error-comment', msg, 'show');
        },

        init: function() {
            var myDropzone = this;
            if ($('#submit-comment').length == 0) return;
            var submitButton = document.querySelector("#submit-comment");


            submitButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $loader.show();

                if (myDropzone.getQueuedFiles().length > 0) {
                    console.log(myDropzone.getQueuedFiles().length);
                    myDropzone.processQueue();
                } else if ($('#comment').find("input[name='file_from_lib']").val()) {
                    console.log('file from lib ' + $('#comment').find("input[name='file_from_lib']").val());
                    var fl = { status: 'added' };
                    myDropzone.uploadFiles([fl]); //send file from lib
                } else {
                    var fl = { status: 'added' };
                    myDropzone.uploadFiles([fl]); //send empty
                }
                return false;
            });

            this.on("maxfilesexceeded", function(file){
                this.removeAllFiles();
                this.addFile(file);
                return false;
            });

            this.on("sending", function(file, xhr, data) {
                // add preview
                if(prev.length > 0) {
                    for(i=0;i<prev.length;i++) {
                        data.append('preview', prev[i]);
                    }
                }
            });

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
            });

        }

    };

    /**************************/
    /*      END COMMENT       */
    /**************************/

    /* SIGNS */
    $('.more').on('click', function() {
        var $target = $(this).data('target');
        $('.'+$target).toggleClass('min-height');
    });

    // change form input values
    $('.has-target').on('change', function() {
        $('#'+$(this).data('target')).val($(this).val());
    });

    // show by target
    $('.show-by-target').on('click', function() {
        $('#'+$(this).data('target')).toggle();

        if ($(this).data('action') == 'comment') {
            // change input
            $('#target-id').val($(this).data('id'));
            $('#target-comment').val($(this).data('comment'));
        }

        return false;
    });

    // scroll to comment form
    $('.comment-choose').on('click', function() {

        // scroll
        var $to = $('#'+$(this).data('scrollto')).offset().top - 500;
        $('html, body').animate({
            scrollTop: $to
        }, 1000);

        $('#add-comment-front').show();

        // chang input
        $('#target-id').val($(this).data('id'));
        $('#target-comment').val($(this).data('target'));


        return false;
    });

});