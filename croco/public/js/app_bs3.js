if ($('input').hasClass('tags')) {
    var tags = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: 'ajax/tags',
            cache: false,
            filter: function(list) {
                return $.map(list, function(cityname) {
                    return { name: cityname };
                });
            }
        }
    });
    tags.initialize();

    /**
     * Typeahead
     */
    var elt = $('.example_typeahead > > input');
    elt.tagsinput({
        typeaheadjs: {
            name: 'tags',
            displayKey: 'name',
            valueKey: 'name',
            source: tags.ttAdapter()
        }
    });
}



