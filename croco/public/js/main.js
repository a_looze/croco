/**
 * Главный js файл для управления проектом
 */

$(function() {
    // helpers
    var typeof_string           = typeof '',
        typeof_undefined        = typeof undefined,
        typeof_function         = typeof function () {},
        typeof_object           = typeof {},
        isTypeOf                = function (item, type) { return typeof item === type; },
        isString                = function (item) { return isTypeOf(item, typeof_string); },
        isUndefined             = function (item) { return isTypeOf(item, typeof_undefined); },
        isFunction              = function (item) { return isTypeOf(item, typeof_function); },
        isObject                = function (item) { return isTypeOf(item, typeof_object); };
    var here = document.location.pathname;
    var mainUrl = window.location.protocol + '//' + window.location.hostname + '/ajax/',
        commfi = [],
        tpl = {
            parent : 'body',
            class : 'stub',
            route : 'base',
            target : '#stub',
            paginate: false,
            event : 'click',
            before : false,
            after : false,
            params : {}
        },
        add = function(obj) {
            var ind = parseInt(this.length);
            for (var i in tpl) {
                if (isUndefined(obj[i])) {
                    obj[i] = tpl[i];
                }
            }
            this[ind] = obj;
        },

        push = function(obj) {
            add.call(commfi, obj);
        };

    function resetAutoPosition() {
        var previews =[];
        var outer;

        var issue1 = $('#autoposition').find('.item').length;
        var issue2 = $('#files-outer').find('.item').length;

        if (issue1 == 0 && issue2 == 0) return;

        if (issue1 != 0) {
            outer = '#autoposition';
        } else {
            outer = '#files-outer';
        }

        previews = $(outer + ' .item').map(function(){
            return $(this).html();
        });

        if ($(outer).length) {
            $(outer).jresponsive({
                transormation: 'animation',
                min_size: 250,
                max_size: 350,
                height: 250,
                hspace: 4,
                vspace: 0,
                content_array: previews,
                class_name: 'item'
            });
        }

    }

    function checkWindowSize() {
        var w = $('body').width();
        if (w < 480) {
            $('body').addClass('mobile');
        } else {
            $('body').removeClass('mobile');
        }
    }

    function correctPosition() {
        return;
        // if ($('#qW').length == 0) return;
        // var h = $('#qW').height() + 26;
        // // console.log(h);
        // $('div.answerWrap').css({'margin-top': h + 'px'});
    }

    function menuChain() {
        return;

        // if ($('body').hasClass('mobile')) return;
        // if ($('#qW').length == 0) return;

        // if ($('#navbar').hasClass('navbarMini')) {
        //     $('#qW').animate({'margin-left':'62px', 'width':'70%'}, 500);
        //     $('#questSide').animate({'margin-left':'62px', 'width':'70%'}, 500);
        // } else {
        //     $('#qW').animate({'margin-left':'265px', 'width':'50%'}, 500);
        //     $('#questSide').animate({'margin-left':'265px', 'width':'50%'}, 500);
        // }
        // setTimeout(function() {
        //     correctPosition();            
        // }, 700);
    }

    setTimeout(function() {
        // устанавливаем класс для body 
        checkWindowSize();
        // подключаем плагин для динамического размещения превью
        resetAutoPosition();
        // зазор под вопросом
        correctPosition();
        // menu fix
        menuChain();
    }, 700);

    
    $(window).on('resize', function() {
        checkWindowSize();
    });

   

    ////////////// Добавляем функционал
    
    //////////// реальные роуты
    // подписка/отписка на канал/категорию    
    push({
        class : 'trpbut',
        route : 'subscribe',
        before : function() {
            $('#loader-wrapper').show();
        },
        target : function(data) {
            if (!isUndefined(data.redirect) && data.redirect != '') {
                location.href = data.redirect;
            } else {
                $('.trpbut').html(data.body.button);
                $('#score').html(data.body.text);
            }
            $('#loader-wrapper').hide();
        },
        params : {type : $('.trpbut').data('subs'), from : here}
    });

    // бесконечная подгрузка в категории
    var catPage = 1;
    var sortBy = 'd';
    var sortDirAsc = false; 

    push({
        class : 'pushable',
        route : 'catposts',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            // console.log(DOMevent);

            catPage++;

            // для поиска
            if (isString($('#sstr').val()) && $('#sstr').val() != '') {
                here+= '/' + $('#sstr').val();
            }

            this.params = {page : catPage, sort: sortBy, asc: sortDirAsc, from : here};
            $('#loader-wrapper').show();
            return true; 
        },
        target : function(d) {
            if (!isUndefined(d.redirect) && d.redirect != '') {
                location.href = d.redirect;
            } else {
                var buttonDiv = $('#autoposition').find('.item:last');
                buttonDiv.detach();

                $('#autoposition').append(d.body.code);

                if (d.body.next) {
                    buttonDiv.appendTo('#autoposition');
                }

                resetAutoPosition();
                $('#loader-wrapper').hide();
            }
        }
    });

    // сортировки на страницах с каталогами
    push({
        class : 'sP',
        route : 'catposts',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            
            if (elm.hasClass('active')) {
                sortDirAsc = !sortDirAsc;
            } else {
                elm.addClass('active').siblings('.active').removeClass('active');
                sortDirAsc = false;                
            }
            sortBy = elm.data('sort');
            catPage = 1;

            // для поиска
            if (isString($('#sstr').val()) && $('#sstr').val() != '') {
                here+= '/' + $('#sstr').val();
            }
            
            this.params = {page : catPage, sort: sortBy, asc: sortDirAsc, from : here};
            $('#loader-wrapper').show();
            return true; 
        },
        target : function(d) {
            // console.log(d);
            if (!isUndefined(d.redirect) && d.redirect != '') {
                location.href = d.redirect;
            } else {
                $('#autoposition').html(d.body.code);

                resetAutoPosition();
                $('#loader-wrapper').hide();
            }
        }
    });

    // manager can rm this
    push({
        class : 'onlyrm',
        route : 'rm',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            var target = elm.data('target');
            var iid = elm.data('iid');
            this.params = {target: target, iid: iid};
            if (confirm('Вы уверены?')) {
                $('#loader-wrapper').show();
                return true;
            } else {
                return false;
            }
        },
        target : function(d) {
            $('#loader-wrapper').hide();
            if (!isUndefined(d.status) && d.status == 'OK') {
                alert('Удалено');
                location.reload();
            } else {
                alert('Не удалено');
            }
        },
        params : {}
    });

    // manager can rm this and ban user
    push({
        class : 'rmban',
        route : 'banrm',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            var target = elm.data('target');
            var iid = elm.data('iid');
            this.params = {target: target, iid: iid};
            if (confirm('Вы уверены?')) {
                $('#loader-wrapper').show();
                return true;
            } else {
                return false;
            }
        },
        target : function(d) {
            $('#loader-wrapper').hide();
            if (!isUndefined(d.status) && d.status == 'OK') {
                alert('Удалено, автор забанен');
                location.reload();
            } else {
                alert('Не удалено');
            }
        },
        params : {}
    });

    // user claim this
    push({
        class : 'claim',
        route : 'claim',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            var target = elm.data('target');
            var iid = elm.data('iid');
            this.params = {target: target, iid: iid};
            // return confirm('Вы уверены?');
        },
        target : function(d) {
            // if (!isUndefined(d.redirect) && d.redirect != '') {
            //     alert('Бан установлен');
            //     location.href = d.redirect;
            // } else {
            //     alert('Бан не установлен');
            // }
            alert ('Жалоба отправлена');
        },
        params : {}
    });

    // show author answer
    push({
        class : 'showauthoranswer',
        before : function(DOMevent) {
            $('div.aaconfirm').show();
            return false;
        },
        params : {}
    });
    push({
        class : 'mobAuthorAnswer',
        before : function(DOMevent) {
            $('div.aaconfirm').show();
            return false;
        },
        params : {}
    });

    // like/dislike
    push({
        class : 'like',
        route : 'rating',
        before : function(DOMevent) {
            DOMevent.stopPropagation();
            var elm = $(DOMevent.target);

            if (elm.data('target') == false) {
                return false;
            }
            var target = elm.data('target');
            var iid = elm.data('iid');
            this.params = {target: target, iid: iid};
            this.target = elm;

            // убрать когда определимся с дизайном
            // return confirm('Лайкаем?');

            $('#loader-wrapper').show();
        },
        after : function() {
            $('#loader-wrapper').hide();
        }
    });

    // set user settings
    push({
        class : 'setting-check',
        route : 'setting',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);

            var target = elm.prop('name');

            if (elm.prop('checked')) {
                elm.val('1');
                console.log('check');
            } else {
                elm.val('0');
                console.log('uncheck');
            }

            if (elm.hasClass('article-setting')) {
                $('#'+elm.data('target')).val(elm.val());
                return false;
            }

            console.log(elm.val());

            this.params = {target: target, value: elm.val()};

            return true;
        },
        after: function () {
            console.log(this.params);
        }
    });

    // sort links
    push({
        class : 'sortIco',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            elm.toggleClass('sortActive');
            $('#sortParam').slideToggle('medium');
            return false;
        }
    });

    // open/close userlist links
    push({
        class : 'more',
        before : function(DOMevent) {
            var elm = $('#subsdiv');
            elm.toggleClass('subscribe');
            return false;
        }
    });

    // mobile-based template
    // see checkWindowSize();
    push({
        class : 'srchicon',
        before : function() {
            if ($('body').hasClass('mobile')) {
                $('#search').toggleClass('activeSearch');
                $('input[name=search]').toggle();
            }
            return false;
        }
    });

    // сортировки на страницах с файлами
    push({
        class : 'file-sort',
        route : 'filesort',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);

            if (elm.hasClass('active')) {
                sortDirAsc = !sortDirAsc;
            } else {
                elm.addClass('active').siblings('.active').removeClass('active');
                sortDirAsc = false;
            }
            sortBy = elm.data('sort');
            catPage = 1;
            this.params = {page : catPage, sort: sortBy, asc: sortDirAsc, from : here};
            $('#loader-wrapper').show();
            return true;
        },
        target : function(d) {
            // console.log(d);
            if (!isUndefined(d.redirect) && d.redirect != '') {
                location.href = d.redirect;
            } else {
                $('#files-outer').html(d.body.code);
                resetAutoPosition();
                $('#loader-wrapper').hide();
            }
        }
    });

    // ищем юзера, которому адресован вопрос
    push({
        class : 'choice-for-whom',
        route : 'whom',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            var inp = $('#'+elm.data('input'));
            this.params = {url: inp.val()};
            return true;
        },
        target : function (d, DOMevent) {
            var elm = $(DOMevent.target);
            if (d.status == 201) {
                $('#'+elm.data('target')).val(d.user_id);
            }
            $('#'+elm.data('msg')).html(d.body.code);
        }
    });

    // авторская пометка на ответах
    var targetId;
    push({
        class : 'authorsay',
        route : 'authorsay',
        before : function(DOMevent) {
            // авторский клик не должен разворачивать/сворачивать контейнер с ответом
            DOMevent.stopPropagation();
            $('#loader-wrapper').show();

            var elm = $(DOMevent.target);
            var say = elm.data('what');
            var target = elm.data('target');
            targetId = '#as' + target;
            this.params = {from: here, what: say, target: target};
        },
        target : function(d) {
            if (!isUndefined(d.redirect) && d.redirect != '') {
                location.href = d.redirect;
            } else {
                $(targetId).html(d.body);
            }
        },
        after : function() {
            $('#loader-wrapper').hide();
        },
        params : {}
    });

    // user accept/decline lookup author answer
    push({
        class : 'aaconfirmbut',
        route : 'setcook',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);
            var act = elm.data('a');
            $('div.aaconfirm').hide();

            if (act == '0') {
                return false;
            }

            $('div.aacontent').show();
            $('.showauthoranswer').detach();
            $('.mobAuthorAnswer').detach();
            $('.aaconfirm').detach();

            return true;
        },
        target : function(d) {
            // нужно просто установить куку, ответ не важен
        },
        params : {type : 'aaview', from : here}
    });

    // visibility other posts
    push({
        class : 'vis',
        route : 'setcook',
        before : function(DOMevent) {
            var elm = $(DOMevent.target);

            elm.toggleClass('active');
            $('#otherQuest div.browse').toggleClass('visActive');
            return true;
        },
        target : function(d) {
            // нужно просто установить куку, ответ не важен
        },
        params : {type : 'covis'}
    });

    // console.log(commfi);
    var ind, elm, cb;

    commfi.forEach(function(elm, ind, comfi) {
        // console.log(elm);
        // console.log('added: ' + elm.event + ' on ' + elm.parent + ' > .' + elm.class);

        $(elm.parent).on(elm.event, '.' + elm.class, function(event) {
            // console.log('fired: ' + elm.event + ' on .' + elm.class);
            if (isFunction(elm.before)) {
                cb = elm.before.call(elm, event);
            } else {
                cb = true;
            }
            // console.log('before: ' + cb); 

            if (false !== cb) {
                // console.log('request: ' + mainUrl + elm.route);
                // console.log('params: ');
                // console.log(elm.params); 
                $.getJSON(mainUrl + elm.route, elm.params, function(d) {
                    if (isUndefined(d) || !isObject(d)) {
                        console.log('Error #1 in ' + elm.event + ' on ' + elm.parent + ' > .' + elm.class);
                    } else if (isFunction(elm.target)) {
                        console.log(d.mess.warn);
                        console.log(d.mess.success);
                        elm.target.call(elm, d, event);
                    } else {
                        // console.log(elm.target);
                        if (!isUndefined(d.mess.error) && d.mess.error != '') {
                            console.log(d.mess.error);
                        } else if (!isUndefined(d.redirect) && d.redirect != '') {
                            document.location = d.redirect;
                        } else {
                            console.log(d.mess.warn);
                            console.log(d.mess.success);
                            $(elm.target).html(d.body);
                        }
                    }
                }).then(function() {
                    if (isFunction(elm.after)) {
                        elm.after.call(elm);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown){
                    console.log('Error #2 in ' + elm.event + ' on .' + elm.class);
                    // console.log(jqXHR.statusText);
                    // console.log(jqXHR);
                    // console.log(textStatus);
                    // console.log(errorThrown);
                });
            }
            //error(jqXHR, textStatus, errorThrown)

            
        });

        // пока отключаем, флудит
        // if (elm.event == 'refresh') {
        //     setInterval(function() {
        //         $('.' + elm.class).trigger('refresh');
        //     }, elm.interval?elm.interval:10000);
        // }
    });

    /////////////// доп. установки 

    // все ссылки с классом 'a.stayhere' и любым href отключаем - чтобы не убегало по href="#"
    $('body').on('click', 'a.stayhere', function(e) {
        e.preventDefault();
        return false;
    });

    // article
    
    // questBox
    // if ($('#questionWrap').length != 0) { 
    //     var qbHt = $('#qW').height();
    //     $('#questionWrap').css({'height': qbHt + 'px'});
    // }
    // if ($('#questionWrap').length != 0) { 
    //     // alert('1');
    //     var qbHt = $('#questionWrap').height();
    //     // alert('height: ' + qbHt);
    //     $(document).scroll(function() {
    //        qbHt = $('#questionWrap').height()+14;
    //         var pos = $(document).scrollTop();
    //         if (pos > qbHt) {
    //             $('#questionWrap').addClass('compact');
    //             $('#questSide').css({'margin-top': (qbHt-66) + 'px'});
    //             $('.question').css({'margin-top': '-' + qbHt + 'px'});
    //         } else {
    //             $('#questionWrap').removeClass('compact').removeClass('compact2');
    //             $('#questSide').css({'margin-top': '0px'});
    //             $('.question').css({'margin-top': '0px'});
    //         }
    //     });
    // }    

    $('.expand').on('click', function() {
        // $('div.question div.questBox').slideToggle(200);
        // setTimeout(correctPosition, 250); sub2
        // $('#questionWrap').toggleClass('compact2');
        $('.sub2').toggleClass('compact2');
        // var pos = $(document).scrollTop();
        // if (pos > 0) {
        //     $('body,html').animate({scrollTop: '0'}, 900, 'swing');
        // }
    });

    // $('.infoBox').on('click', function() {
    //     // $('#questionWrap').toggleClass('compact2');
    //     var pos = $(document).scrollTop();
    //     if (pos > 0) {
    //         $('body,html').animate({scrollTop: '0'}, 900, 'swing');
    //     }
    // });

    $('.hash').on('click', function() {
        $('div.head div.htags').slideToggle();
    });    

    // catalog
    // if ($('#otherQuest').length != 0) {
    //     var oqTopOfs = $('#otherQuest').offset();
    //     var oqTop = oqTopOfs.top;
    //     var oqHt = $('#otherQuest').height();
    //     var wHt = $(window).height();

    //     $(document).scroll(function() {
    //         var pos = $(document).scrollTop();
    //         if (pos >= oqTop+oqHt-wHt) {
    //             $('#otherQuest').addClass('oqfix');
    //         } else {
    //             $('#otherQuest').removeClass('oqfix');
    //         }
    //     });
    // }

    $(document).scroll(function() {
        if ($('.autofix').length == 0) return;

        var pos = $(document).scrollTop();
        if ($('body').hasClass('mobile')) {
            var maxPos = 130;
        } else {
            var maxPos = 319;
        }

        if (pos >= maxPos) {
            $('.fixHead').addClass('fixx');
        } else {
            $('.fixHead').removeClass('fixx');
        }
    });

    // toggle menu
    $('#menu-ico').click(function () {
        $('#navbar').toggleClass('navbarMini', 500);
        $('#content').toggleClass('contFull', 500);
        //$(this).find('span').toggleClass('active');
        menuChain();
        return false;
        
    });

    $('.img, .questBox').on('click', function() {
        $(this).toggleClass('realHeight');
    });

    $('.baron').baron({
        root: '.baron',
        scroller: '.baron__scroller',
        bar: '.baron__bar',
        scrollingCls: '_scrolling',
        draggingCls: '_dragging'
    }); 

    $('#oqBws').baron({
        root: '.baron2',
        scroller: '.baron__scroller',
        bar: '.baron__bar',
        scrollingCls: '_scrolling',
        draggingCls: '_dragging'
    });  

    // modal
    $('.imgitemansw').on('click', function() {
        var iid = $(this).data('iid');
        $('#md' + iid).show();
        $('body').addClass('modalon');
    });

    $('.closeModal').on('click', function() {
        var iid = $(this).data('iid');
        $('#md' + iid).hide();
        $('body').removeClass('modalon');
    });

    $('.modalBg').on('click', function(e) {
        // console.log(e.target);
        if($(e.target).hasClass('modalBg')) {
            var iid = $(this).data('iid');
            $('#md' + iid).hide();
            $('body').removeClass('modalon');
        }
    });

    // $('.srav').on('click', function() {
    //     $(this).siblings('.modalItemp').toggle();
    //     $(this).siblings('.modalItema').toggleClass('single');
    // });

    $('.notifyWrap').find('a').each(function(i, e) {
        var hr = $(e).attr('href');
        if (hr) {
            hr = hr.replace('new.', '');
            $(e).attr('href', hr);
        }
        
    });
    
    console.log('main.js load: OK');
});
