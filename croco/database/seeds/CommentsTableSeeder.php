<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('comments')->delete();
        
        \DB::table('comments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 36,
                'target' => 'post',
                'user_id' => 3,
                'content' => '1231',
                'file_id' => 0,
                'preview_file_id' => 0,
                'rating' => 0,
                'created_at' => '2016-10-19 17:55:09',
                'updated_at' => '2016-10-19 17:55:09',
            ),
            1 => 
            array (
                'id' => 3,
                'parent_id' => 49,
                'target' => 'post',
                'user_id' => 79,
                'content' => 'атащштщт',
                'file_id' => 327,
                'preview_file_id' => 0,
                'rating' => 0,
                'created_at' => '2016-10-21 09:43:38',
                'updated_at' => '2016-10-21 09:43:38',
            ),
            2 => 
            array (
                'id' => 4,
                'parent_id' => 49,
                'target' => 'post',
                'user_id' => 79,
                'content' => 'ыуаАУЫаЫуаы',
                'file_id' => 0,
                'preview_file_id' => 0,
                'rating' => 0,
                'created_at' => '2016-10-21 09:43:49',
                'updated_at' => '2016-10-21 09:43:49',
            ),
            3 => 
            array (
                'id' => 5,
                'parent_id' => 49,
                'target' => 'post',
                'user_id' => 79,
                'content' => '',
                'file_id' => 330,
                'preview_file_id' => 0,
                'rating' => 0,
                'created_at' => '2016-10-21 09:44:03',
                'updated_at' => '2016-10-21 09:44:03',
            ),
        ));
        
        
    }
}
