<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'about.title',
                'alias' => 'about',
                'description' => 'about.desc',
                'keywords' => 'about.keyw',
                'content' => 'about.content',
                'template' => 'about',
                'bgimg' => 'storage/pages/bgimg/34/57cd54ed7f8cd.jpg',
                'active' => 1,
                'visible' => 1,
                'priority' => 0,
                'created_at' => '2016-09-05 11:12:02',
                'updated_at' => '2016-09-05 14:12:37',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'foruser.title',
                'alias' => 'foruser',
                'description' => 'foruser.desc',
                'keywords' => 'foruser.keyw',
                'content' => 'foruser.content',
                'template' => 'text',
                'bgimg' => 'storage/pages/bgimg/7a/57cd7dae4cf1e.jpg',
                'active' => 1,
                'visible' => 1,
                'priority' => 1,
                'created_at' => '2016-09-05 14:14:06',
                'updated_at' => '2016-09-05 14:14:06',
            ),
        ));
        
        
    }
}
