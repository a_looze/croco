<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(LangsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        // $this->call(EventsTableSeeder::class);
        // $this->call(PagesTableSeeder::class);

        /////////////////////// подключать либо этот блок для честных данных...
        // $this->call(UsersTableSeeder::class);
        // $this->call(ArticlesTableSeeder::class);
        // $this->call(FilesTableSeeder::class);
        // $this->call(CommentsTableSeeder::class);
        // $this->call(AnswersTableSeeder::class);
        // $this->call(TagsTableSeeder::class);
        // $this->call(ArticleTagsTableSeeder::class);
        
        /////////////////////// ...либо этот - для фейковых тестовых
        // $this->call(FakeUsersTableSeeder::class);
        // $this->call(FakeFilesTableSeeder::class);
        // $this->call(FakeArticlesTableSeeder::class);
        // $this->call(FakeCommentsTableSeeder::class);
        // $this->call(FakeAnswersTableSeeder::class);
        // $this->call(FakeTagsTableSeeder::class);
        // $this->call(FakeArticleTagsTableSeeder::class);
        $this->call('CategoriesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('FilesTableSeeder');
        $this->call('ArticlesTableSeeder');
        $this->call('AnswersTableSeeder');
        $this->call('CommentsTableSeeder');
        $this->call('EventsTableSeeder');
        $this->call('PagesTableSeeder');
        $this->call('TagsTableSeeder');
        $this->call('ArticleTagsTableSeeder');
    }
}
