<?php

use Illuminate\Database\Seeder;

class LangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('langs')->delete();
        
        \DB::table('langs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'lkey' => 'en',
                'cname' => 'Английский',
                'ownname' => 'English',
                'priority' => 0,
                'active' => 1,
                'created_at' => '2016-08-19 22:33:27',
                'updated_at' => '2016-08-19 22:33:27',
            ),
            1 => 
            array (
                'id' => 2,
                'lkey' => 'ru',
                'cname' => 'Русский',
                'ownname' => 'Русский',
                'priority' => 1,
                'active' => 1,
                'created_at' => '2016-08-19 22:34:23',
                'updated_at' => '2016-08-19 22:34:23',
            ),
            2 => 
            array (
                'id' => 3,
                'lkey' => 'fr',
                'cname' => 'Французский',
                'ownname' => 'France',
                'priority' => 2,
                'active' => 0,
                'created_at' => '2016-08-19 22:35:04',
                'updated_at' => '2016-08-19 22:35:04',
            ),
        ));
        
        
    }
}
