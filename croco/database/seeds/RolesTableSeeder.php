<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role' => 0,
                'title' => 'webuser',
            ),
            1 => 
            array (
                'id' => 2,
                'role' => 1,
                'title' => 'webuser1',
            ),
            2 => 
            array (
                'id' => 3,
                'role' => 2,
                'title' => 'webuser2',
            ),
            3 => 
            array (
                'id' => 4,
                'role' => 3,
                'title' => 'webuser3',
            ),
            4 => 
            array (
                'id' => 5,
                'role' => 4,
                'title' => 'webuser4',
            ),
            5 => 
            array (
                'id' => 6,
                'role' => 5,
                'title' => 'webuser5',
            ),
            6 => 
            array (
                'id' => 7,
                'role' => 6,
                'title' => 'manager',
            ),
            7 => 
            array (
                'id' => 8,
                'role' => 7,
                'title' => 'manager1',
            ),
            8 => 
            array (
                'id' => 9,
                'role' => 8,
                'title' => 'manager2',
            ),
            9 => 
            array (
                'id' => 10,
                'role' => 9,
                'title' => 'root',
            ),
        ));
    }
}
