<?php

use Illuminate\Database\Seeder;

class FakeUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();

        // вначале настоящие юзеры, чтобы пустило в админку
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'a.looze@gmail.com',
                'alias' => 'YTllMmJiO1',
                'password' => '$2y$10$k9/XkbNC0o0hfEEmzaVfze9PqU/j0jkf.13G3YZUnEw9QKISE9wxS',
                'role' => 9,
                'country_id' => 96,
                'avatar' => '',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => '0alrfJiT18wFNEcHarzfxbepMXuBjQ3aqdKqybyLvGdiTso5BvpjdqAcjp3t',
                'created_at' => NULL,
                'updated_at' => '2016-08-17 20:37:27',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'alooze',
                'email' => 'a.looz.e@gmail.com',
                'alias' => 'YTllMmJiO2',
                'password' => '$2y$10$FfxfEDF7830SbnDPPy.z7.j9If4leBHnCy2HkY7NyCpF1D.LYe3Ja',
                'role' => 9,
                'country_id' => 78,
                'avatar' => '',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => NULL,
                'created_at' => '2016-08-17 20:37:55',
                'updated_at' => '2016-08-17 20:37:55',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'shcoder',
                'email' => 'sitemart@gmail.com',
                'alias' => 'YTllMmJiO3',
                'password' => '$2y$10$yQVxDUpWe8OmM6SUzWyMyOWTQyE4YI/KlIrwI/rWHkOWrivcGqyJG',
                'role' => 9,
                'country_id' => 99,
                'avatar' => 'assets/media/users/u3/avatar/bellfield.png',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => NULL,
                'created_at' => '2016-08-18 14:36:49',
                'updated_at' => '2016-08-22 19:45:16',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'user',
                'email' => 'a.l.o.o.z.e@gmail.com',
                'alias' => 'YTllMmJiO4',
                'password' => '$2y$10$jGSrK9KKNPxOf3KPLgmv9eVPP9XLID0rHXHDw/VS3s6tgGzkSfjhW',
                'role' => 0,
                'country_id' => 0,
                'avatar' => '',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => 'RNNZbgu6lMA8n5ACrFl0JubwcfKotMQx0hmQVMHfk1yigH43vkBvMfIMgCsg',
                'created_at' => '2016-08-19 17:04:58',
                'updated_at' => '2016-08-21 15:34:22',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'root',
                'email' => 'root@site.com',
                'alias' => 'YTllMmJiO5',
                'password' => '$2y$10$HEwh2c1Mbcx1qYdg2Zq30uYDZ/CwYADJAUQ0XtD.OQJ3IfWbNB5cK',
                'role' => 9,
                'country_id' => 1,
                'avatar' => '',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => NULL,
                'created_at' => '2016-08-21 17:05:25',
                'updated_at' => '2016-08-21 17:05:25',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Kote8',
                'email' => 'sitemart8@yandex.ua',
                'alias' => 'YTllMmJiO68',
                'password' => '$2y$10$WuIam6Qordv66kyfAcb9L.6WFdxN9pUou7Qm1FXAsX3fr3YYLW9pe',
                'role' => 0,
                'country_id' => 73,
                'avatar' => 'assets/media/default/default_user.jpg',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => 'VOikbPZ8Ux7ekzWNH7JOF0YWmGr0CU24hQJghl9Sr28dfKKB00zPzKgB16VX',
                'created_at' => '2016-08-29 13:03:03',
                'updated_at' => '2016-08-30 09:40:52',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Kote',
                'email' => 'sitemart@yandex.ua',
                'alias' => 'YTllMmJiO6',
                'password' => '$2y$10$WuIam6Qordv66kyfAcb9L.6WFdxN9pUou7Qm1FXAsX3fr3YYLW9pe',
                'role' => 0,
                'country_id' => 73,
                'avatar' => 'assets/media/default/default_user.jpg',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => 'VOikbPZ8Ux7ekzWNH7JOF0YWmGr0CU24hQJghl9Sr28dfKKB00zPzKgB16VX',
                'created_at' => '2016-08-29 13:03:03',
                'updated_at' => '2016-08-30 09:40:52',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Kote 2',
                'email' => 'sitemart@ya.ru',
                'alias' => 'YTllMmJiOA',
                'password' => '$2y$10$coLzTb7YP7j0xfql8s81T.TKN21VmaLZ62MrzMawi/Z4zEji8AhjS',
                'role' => 0,
                'country_id' => 18,
                'avatar' => 'assets/media/default/default_user.jpg',
                'isblocked' => 0,
                'blocked_at' => 0,
                'blocked_untill' => 0,
                'lastlogin_at' => 0,
                'remember_token' => NULL,
                'created_at' => '2016-09-01 09:06:49',
                'updated_at' => '2016-09-01 09:06:49',
            ),
        ));
        
        // и 50 фейковых
        for ($i = 0; $i < 50; $i++) {
            $user = factory(App\User::class)->create();
        }
        
    }
}
