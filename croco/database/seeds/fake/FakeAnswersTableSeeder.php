<?php

use Illuminate\Database\Seeder;

class FakeAnswersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('answers')->delete();
        
        for ($i = 0; $i < 100; $i++) {
            factory(App\Answer::class)->create();
        }
        
    }
}
