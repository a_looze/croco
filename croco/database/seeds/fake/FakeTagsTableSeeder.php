<?php

use Illuminate\Database\Seeder;

class FakeTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tags')->delete();
        
        for ($i = 0; $i < 100; $i++) {
            factory(App\Tag::class)->create();
        }
        
    }
}
