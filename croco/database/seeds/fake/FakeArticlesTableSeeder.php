<?php

use Illuminate\Database\Seeder;

class FakeArticlesTableSeeder extends Seeder
{

    /**
     * Fake generator
     */
    public function run()
    {
        \DB::table('articles')->delete();

        // создаем 50 статей
        for ($i = 0; $i < 50; $i++) {
            factory(App\Article::class)->create();
        }
    }
}
