<?php

use Illuminate\Database\Seeder;

class FakeFilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('files')->delete();
        
        
        // создаем 50 записей о файле
        for ($i = 0; $i < 50; $i++) {
            factory(App\File::class)->create();
        }
    }
}
