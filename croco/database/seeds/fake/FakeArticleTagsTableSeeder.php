<?php

use Illuminate\Database\Seeder;

class FakeArticleTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('article_tags')->delete();
        
        for ($i = 0; $i < 200; $i++) {
            factory(App\ArticleTag::class)->create();
        }
        
    }
}
