<?php

use Illuminate\Database\Seeder;

class FakeCommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('comments')->delete();
        
        for ($i = 0; $i < 200; $i++) {
            factory(App\Comment::class)->create();
        }
        
    }
}
