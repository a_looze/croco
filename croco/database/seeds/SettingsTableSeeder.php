<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'site_name',
                'value' => 'Commfi',
                'created_at' => '2016-08-21 17:00:32',
                'updated_at' => '2016-08-21 17:00:32',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'nophoto_path',
                'value' => 'images/nofoto.jpg',
                'created_at' => '2016-09-10 19:32:28',
                'updated_at' => '2016-09-10 19:32:28',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'manager_mail',
                'value' => 'a.looze@gmail.com, a.looz.e@gmail.com',
                'created_at' => '2016-09-19 14:58:19',
                'updated_at' => '2016-09-19 14:58:19',
            ),
        ));
        
        
    }
}
