<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'alias' => 'play',
                'letter' => 'm',
                'cname' => 'Сыграй вопрос',
                'langkey' => 'main.playcat',
                'bgimg' => 'storage/categories/bgimg/df/5805e7daee330.jpg',
                'parent_id' => 0,
                'visible' => 1,
                'active' => 1,
                'priority' => 0,
                'created_at' => '2016-08-21 16:57:04',
                'updated_at' => '2016-10-18 09:14:03',
            ),
            1 => 
            array (
                'id' => 2,
                'alias' => 'dance',
                'letter' => 'd',
                'cname' => 'Станцуй вопрос',
                'langkey' => 'main.dancecat',
                'bgimg' => 'storage/categories/bgimg/73/5805e9cedf740.jpg',
                'parent_id' => 0,
                'visible' => 1,
                'active' => 1,
                'priority' => 1,
                'created_at' => '2016-08-21 16:58:57',
                'updated_at' => '2016-10-18 09:22:22',
            ),
            2 => 
            array (
                'id' => 3,
                'alias' => 'draw',
                'letter' => 'p',
                'cname' => 'Нарисуй вопрос',
                'langkey' => 'main.drawcat',
                'bgimg' => 'storage/categories/bgimg/3c/5805e7eba1880.jpg',
                'parent_id' => 0,
                'visible' => 1,
                'active' => 1,
                'priority' => 2,
                'created_at' => '2016-08-21 17:00:05',
                'updated_at' => '2016-10-18 09:14:19',
            ),
            3 => 
            array (
                'id' => 4,
                'alias' => 'move',
                'letter' => 'g',
                'cname' => 'Задай вопрос движениями',
                'langkey' => 'main.movecat',
                'bgimg' => 'storage/categories/bgimg/4f/5805e800492f8.jpg',
                'parent_id' => 0,
                'visible' => 1,
                'active' => 1,
                'priority' => 4,
                'created_at' => '2016-08-23 16:39:47',
                'updated_at' => '2016-10-18 09:14:40',
            ),
            4 => 
            array (
                'id' => 5,
                'alias' => 'voice',
                'letter' => 'v',
                'cname' => 'Задай вопрос голосом',
                'langkey' => 'main.voicecat',
                'bgimg' => 'storage/categories/bgimg/41/5805e80f1957b.jpg',
                'parent_id' => 0,
                'visible' => 1,
                'active' => 1,
                'priority' => 5,
                'created_at' => '2016-09-03 15:52:57',
                'updated_at' => '2016-10-18 09:14:55',
            ),
        ));
        
        
    }
}
