<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('events')->delete();
        
        \DB::table('events')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Успешная авторизация',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-08-29 09:07:14',
                'updated_at' => '2016-08-29 09:07:14',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Отказ авторизации',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-08-29 09:07:30',
                'updated_at' => '2016-08-29 09:07:30',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Подписка на канал',
                'type' => '',
                'bcost' => 10,
                'scost' => 5,
                'created_at' => '2016-08-29 09:09:01',
                'updated_at' => '2016-09-04 17:48:35',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Отписка от канала',
                'type' => '',
                'bcost' => -10,
                'scost' => -5,
                'created_at' => '2016-09-02 20:41:15',
                'updated_at' => '2016-09-04 17:48:52',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Подписка на категорию',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-09-04 17:49:19',
                'updated_at' => '2016-09-04 17:49:19',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Отписка от категории',
                'type' => '',
                'bcost' => -10,
                'scost' => 0,
                'created_at' => '2016-09-04 17:49:34',
                'updated_at' => '2016-09-04 17:49:34',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Лайк вопроса',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-09-12 14:15:49',
                'updated_at' => '2016-09-12 14:15:49',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Дизлайк вопроса',
                'type' => '',
                'bcost' => -10,
                'scost' => 0,
                'created_at' => '2016-09-12 14:16:07',
                'updated_at' => '2016-09-12 14:16:07',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Лайк ответа',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-09-12 14:16:26',
                'updated_at' => '2016-09-12 14:16:26',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Дизлайк ответа',
                'type' => '',
                'bcost' => -10,
                'scost' => 0,
                'created_at' => '2016-09-12 14:16:49',
                'updated_at' => '2016-09-12 14:16:49',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Лайк комментария',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-09-12 14:17:11',
                'updated_at' => '2016-09-12 14:17:11',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Дизлайк комментария',
                'type' => '',
                'bcost' => -10,
                'scost' => 0,
                'created_at' => '2016-09-12 14:17:27',
                'updated_at' => '2016-09-12 14:17:27',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Просмотр темы',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-09-12 14:43:19',
                'updated_at' => '2016-09-12 14:43:19',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Менеджер создал юзера',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-09-19 20:05:07',
                'updated_at' => '2016-09-19 20:05:07',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Менеджер удалил юзера',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-09-19 20:05:53',
                'updated_at' => '2016-09-19 20:05:53',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Юзер добавил вопрос',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-09-19 20:05:54',
                'updated_at' => '2016-09-19 20:05:54',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Юзер оставил комментарий',
                'type' => '',
                'bcost' => 3,
                'scost' => 0,
                'created_at' => '2016-09-19 20:05:55',
                'updated_at' => '2016-09-19 20:05:55',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Юзер дал ответ на вопрос',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-09-19 20:05:56',
                'updated_at' => '2016-09-19 20:05:56',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Юзеру задали вопрос',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-09-19 20:05:57',
                'updated_at' => '2016-09-19 20:05:57',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Автор отметил ответ как верный',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-10-16 23:43:22',
                'updated_at' => '2016-10-16 23:46:03',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Автор отметил ответ как неверный',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-10-16 23:43:36',
                'updated_at' => '2016-10-16 23:43:36',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Бонус за реферальную ссылку',
                'type' => '',
                'bcost' => 10,
                'scost' => 0,
                'created_at' => '2016-10-16 23:43:37',
                'updated_at' => '2016-10-16 23:43:37',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Удаление вопроса пользователя',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-10-22 17:27:50',
                'updated_at' => '2016-10-22 17:27:50',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Удаление ответа пользователя',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-10-22 17:28:11',
                'updated_at' => '2016-10-22 17:28:11',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Удаление Комментария пользователя',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-10-22 17:28:40',
                'updated_at' => '2016-10-22 17:28:40',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Бан пользователя',
                'type' => '',
                'bcost' => 0,
                'scost' => 0,
                'created_at' => '2016-10-22 17:31:15',
                'updated_at' => '2016-10-22 17:31:15',
            ),
        ));
        
        
    }
}
