<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'alias' => str_random($faker->numberBetween(8,10)),
        'role' => 0,
        'country_id' => $faker->numberBetween(0,241),
        'avatar' => 'images/demo/noavatar.jpg',
        'banner' => 'images/demo/bgJesse.jpg',
        'isblocked' => 0,
        'blocked_at' => 0,
        'blocked_untill' => 0,
        'notice_article_like' => 1,
        'notice_answer_like' => 1,
        'notice_comment_like' => 1,
        'notice_referral_sign' => 1,
        'lastlogin_at' => 0,
    ];
});

// вопросы
$factory->define(App\Article::class, function (Faker\Generator $faker) {
    $files = App\File::where('type_id', '3')->get();
    $cnt = count($files) - 1;
    return [
        'alias' => str_random($faker->numberBetween(8,10)),
        'title' => $faker->company,
        'file_id' => $faker->numberBetween(1,50),
        'preview_file_id' => $files[$faker->numberBetween(1,$cnt)]->id,
        // 'answer_file_id' => $faker->numberBetween(1,50),
        // 'answer_preview_file_id' => $files[$faker->numberBetween(1,$cnt)]->id,
        'user_id' => $faker->numberBetween(5,55),
        'category_id' => $faker->numberBetween(1,5),
        'views' => $faker->numberBetween(0,500),
        'rating' => $faker->numberBetween(0,100),
        'visible' => 1,
        'banned' => 0,
        'deleted' => 0,
    ];
});

// ответы
$factory->define(App\Answer::class, function (Faker\Generator $faker) {
    $files = App\File::where('type_id', '3')->get();
    $cnt = count($files) - 1;
    return [
        'article_id' => $faker->numberBetween(0,50),
        'user_id' => $faker->numberBetween(1,55),
        'file_id' => $faker->numberBetween(1,50),
        'preview_file_id' => $files[$faker->numberBetween(1,$cnt)]->id,
        'author_say' => $faker->numberBetween(0,3),
        'rating' => $faker->numberBetween(0,200),
    ];
});

// комментарии
$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    $files = App\File::where('type_id', '3')->get();
    $cnt = count($files) - 1;
    return [
        'parent_id' => $faker->numberBetween(0,50),
        'target' => 'post',
        'user_id' => $faker->numberBetween(1,55),
        'content' => $faker->realText($maxNbChars = 500, $indexSize = 2),
        'file_id' => $faker->numberBetween(0,50),
        'preview_file_id' => $files[$faker->numberBetween(1,$cnt)]->id,
        'rating' => $faker->numberBetween(0,200),
    ];
});

// теги
$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->unique()->word,
        'type' => 'tag',
    ];
});

// теги для статей
$factory->define(App\ArticleTag::class, function (Faker\Generator $faker) {
    return [
        'article_id' => $faker->numberBetween(0,50),
        'tag_id' => $faker->numberBetween(0,100),
    ];
});

// файлы
$factory->define(App\File::class, function (Faker\Generator $faker) {
    $aliasAr = [
        '1' => [
            '51ab4e1619a56',
            '51c4901a8d42d',
            '522e1f22e9347',
            ],
        '2' => [
            '456789123',
            '456789123',
            '456789123',
            ],
        '3' => [
            '51ac571d2c85d',
            '51c750158c72c',
            '51afb222719f9',
            ],
    ];

    $type = $faker->numberBetween(1,3);
    $file = $aliasAr[$type][$faker->numberBetween(0,2)];

    return [
        'title' => $faker->word,
        'description' => $faker->realText($maxNbChars = 100, $indexSize = 2),
        'alias' => $file,
        'type_id' => $type,
        'user_id' => $faker->numberBetween(5,55),
    ];
});

