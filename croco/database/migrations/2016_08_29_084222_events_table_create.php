<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventsTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // $table->integer('readed')->default('0'); // 0 - new, 1 - viewed
            $table->string('type'); // reserved
            $table->integer('bcost')->default('0'); // стоимость в баллах для "покупателя"
            $table->integer('scost')->default('0'); // стоимость в баллах для "продавца"
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
