<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventlogs', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('event_id'); 
            $table->integer('user_id');
            $table->integer('target_id');
            $table->string('comment'); //для id 0 или 3 можно добавить небольшое описание
            // $table->integer('created_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventlogs');
    }
}
