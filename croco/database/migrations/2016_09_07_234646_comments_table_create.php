<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentsTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('target')->default('article'); // reserved; default 'article', can be 'comment', 'answer'
            $table->integer('user_id'); // author
            $table->text('content');
            $table->integer('file_id'); 
            $table->integer('preview_file_id'); // preview for comments file
            $table->smallInteger('rating'); // 'like's count
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
