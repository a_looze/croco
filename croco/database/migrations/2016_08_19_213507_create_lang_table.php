<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('langs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lkey')->unique(); // ru, en etc
            $table->string('cname'); // русский, английский etc
            $table->string('ownname'); // название языка на самом языке
            $table->smallInteger('priority'); // reserved
            $table->smallInteger('active')->default('1'); // reserved
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('langs');
    }
}
