<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnswersTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id');
            $table->integer('user_id'); // author
            $table->integer('file_id'); 
            $table->integer('preview_file_id'); // preview for answers file
            $table->smallInteger('author_say'); // 0 - nothing, 1 - good, 2 - bad, 3 - owner answer
            $table->smallInteger('rating'); // 'like's count
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
