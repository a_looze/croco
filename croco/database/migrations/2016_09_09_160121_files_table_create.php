<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilesTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('alias'); // filename without extensions
            $table->integer('type_id'); // 1 - video, 2 - audio, 3 - image
            $table->integer('user_id'); // owner
            $table->integer('preview_id'); // id файла превью
            // $table->smallInteger('raw'); // reserved, готов ли файл к использованию на фронте (для видео)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
