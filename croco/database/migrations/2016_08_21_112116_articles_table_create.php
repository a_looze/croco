<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticlesTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique(); 
            $table->string('title'); 
            $table->integer('file_id');
            $table->integer('preview_file_id'); // preview for article
            $table->integer('user_id');
            $table->integer('category_id');
            $table->integer('for_user_id');
            $table->integer('views')->default('0'); // count of views
            $table->smallInteger('rating')->default('0'); // 'like's count
            $table->smallInteger('visible')->default('1'); // is visible
            $table->smallInteger('banned')->default('0'); // is banned article
            $table->smallInteger('deleted')->default('0'); // is delete article
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
