<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagesTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title'); 
            $table->string('alias');
            $table->string('description'); 
            $table->string('keywords'); 
            $table->string('content');  
            $table->string('template')->default('text');  
            $table->string('bgimg');   
            $table->smallInteger('active');
            $table->smallInteger('visible'); 
            $table->smallInteger('priority')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
