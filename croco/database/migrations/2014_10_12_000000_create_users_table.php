<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name'); // modx_web_user_attributes - fullname
            $table->string('email')->unique();
            $table->string('alias')->unique();
            $table->string('password'); // modx_web_users - password
            $table->smallInteger('role'); // 0 - min, 9 - root
            $table->integer('country_id'); // modx_web_user_attributes - country
            $table->string('avatar'); // modx_web_user_attributes - mobilephone
            $table->string('banner');
            $table->smallInteger('isblocked'); // 0 - yes, 1 - no, 2 - forewer
            $table->integer('blocked_at');
            $table->integer('blocked_untill');
            $table->smallInteger('notice_article_like')->default(1); // 1 - yes, 0 - no
            $table->smallInteger('notice_answer_like')->default(1);; // 1 -yes, 0 - no
            $table->smallInteger('notice_comment_like')->default(1);; // 1 -yes, 0 - no
            $table->smallInteger('notice_referral_sign')->default(1);; // 1 -yes, 0 - no
            $table->integer('lastlogin_at');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
