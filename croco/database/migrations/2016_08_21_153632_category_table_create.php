<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoryTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique();
            $table->string('letter')->unique(); 
            $table->string('cname'); 
            $table->string('langkey'); 
            $table->string('bgimg'); 
            $table->integer('parent_id')->default('0');
            $table->smallInteger('visible')->default('1'); 
            $table->smallInteger('active')->default('1'); 
            $table->smallInteger('priority')->default('0'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
