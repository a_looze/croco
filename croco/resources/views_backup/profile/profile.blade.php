@extends('layouts.app')

@section('content')
<div class="container">
    @if (Auth::guest())
        <!--  -->
    @else
        @include('profile.menu')
    @endif

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Кабинет пользователя</div>

                <div class="panel-body">
                    Вы авторизованы как {{ Auth::user()->name }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
