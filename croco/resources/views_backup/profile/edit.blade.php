@extends('layouts.app')

@section('content')
<div class="container">
    @if (Auth::guest())
        <!--  -->
    @else
        @include('profile.menu')
    @endif

    <style>

    .container{
        margin-top:20px;
    }
    .image-preview-input {
        position: relative;
    	overflow: hidden;
    	margin: 0px;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }
    .image-preview-input input[type=file] {
    	position: absolute;
    	top: 0;
    	right: 0;
    	margin: 0;
    	padding: 0;
    	font-size: 20px;
    	cursor: pointer;
    	opacity: 0;
    	filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }

    </style>

    <div class="row">
        <div class="col-md-5">

            <p>
                 @if(Session::has('message'))
                    {{ Session::get('message') }}
                 @endif
            </p>



            @if (count($errors) > 0)
            	<div class="alert alert-danger">
            		<strong>Whoops!</strong> There were some problems with your input.<br><br>
            		<ul>
            			@foreach ($errors->all() as $error)
            				<li>{{ trans($error) }}</li>
            			@endforeach
            		</ul>
            	</div>
            @endif

            <div class="form-area">
                <form role="form" method="POST" action="{{ action('UserController@save') }}" enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <input type="hidden" name="id" value="{{ Auth::user()->id  }}"/>

                    <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Данные:</h3>
                    <div class="form-group">
                        <input type="text" class="form-control" value="{{ Auth::user()->name  }}" id="name" name="name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value="{{ Auth::user()->email  }}" id="email" name="email" placeholder="Email" required>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="country_id" data-style="btn-primary">
                            <option value="99">Кения</option>
                            <option value="100">Уругвая</option>
                            <option value="101">Нигерия</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">Загрузить</span>
                                    <input type="file" value="{{ Auth::user()->avatar  }}" accept="image/png, image/jpeg, image/gif" name="img"/> <!-- rename it -->
                                </div>
                            </span>

                        </div><!-- /input-group image-preview [TO HERE]-->
                    </div>


                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Сохранить</button>
                </form>
            </div>
        </div>
        <div class="col-md-7">
            <br/>
            avatar - {{ Auth::user()->avatar  }}
            <br />
            <img src="/{{ Auth::user()->avatar  }}" title="{{ Auth::user()->name  }}" />
        </div>
    </div>
</div>
@endsection
