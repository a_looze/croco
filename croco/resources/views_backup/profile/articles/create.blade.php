@extends('layouts.app')


@section('content')
<div class="container">
    @if (Auth::guest())
        <!--  -->
    @else
        @include('profile.menu')
    @endif

    <style>

    .container{
        margin-top:20px;
    }
    .image-preview-input {
        position: relative;
    	overflow: hidden;
    	margin: 0px;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }
    .image-preview-input input[type=file] {
    	position: absolute;
    	top: 0;
    	right: 0;
    	margin: 0;
    	padding: 0;
    	font-size: 20px;
    	cursor: pointer;
    	opacity: 0;
    	filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }

    </style>

    <!-- <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="{{action('ArticlesController@create')}}">{{trans('main.add')}}</a>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <!-- Success msg -->
            <p>
                 @if(Session::has('message'))
                    {{ Session::get('message') }}
                 @endif
            </p>


            <!-- error msg -->
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ trans($error) }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-area" style="width: 500px">
                <form role="form" method="POST" action="{{ action('ArticlesController@store') }}" enctype="multipart/form-data">

                    <input type="hidden" name="_method" value="post"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <br style="clear:both">

                    <h3 style="margin-bottom: 25px; text-align: center;">Добавить новую тему:</h3>

                    <div class="form-group">
                        <input type="text" class="form-control" value="" id="title" name="title" placeholder="Title" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value="" id="content" name="content" placeholder="Content">
                    </div>

                    <div class="form-group">
                        <span>Категория</span>
                        <select class="form-control" name="category_id" data-style="btn-primary">
                            <option value="">Выберите категорию</option>
                             @foreach($categories as $category)

                            <option value="{{ $category->id }}">{{ $category->cname }}</option>

                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <span>Что будем грузить</span>
                        <select class="form-control" name="type_id" data-style="btn-primary">
                           @foreach($_mod['articles']['type_id'] as $key => $type)
                                   <option value="{{ $key }}">{{ trans($type) }}</option>
                           @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <span>Превью для темы (изображение)</span>
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">Загрузить</span>
                                    <input type="file" value="" accept="image/png, image/jpeg, image/gif" name="img"/> <!-- rename it -->
                                </div>
                            </span>

                        </div><!-- /input-group image-preview [TO HERE]-->
                    </div>

                    <div class="form-group">
                        <span>Публиковать</span>
                        <select class="form-control" name="visible" data-style="btn-primary">
                            <option value="1">Да</option>
                            <option value="0">Нет</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <span>Разрешить комментарии</span>
                        <select class="form-control" name="commented" data-style="btn-primary">
                            <option value="1">Да</option>
                            <option value="0">Нет</option>
                        </select>
                    </div>


                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Сохранить</button>
                </form>
            </div>
            
        </div>
    </div>
    
</div>
@endsection