@extends('layouts.app')


@section('content')
<div class="container">
    @if (Auth::guest())
        <!--  -->
    @else
        @include('profile.menu')
    @endif

    <style>

    .container{
        margin-top:20px;
    }
    .image-preview-input {
        position: relative;
    	overflow: hidden;
    	margin: 0px;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }
    .image-preview-input input[type=file] {
    	position: absolute;
    	top: 0;
    	right: 0;
    	margin: 0;
    	padding: 0;
    	font-size: 20px;
    	cursor: pointer;
    	opacity: 0;
    	filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }

    </style>

    <!-- <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="{{action('ArticlesController@create')}}">{{trans('main.add')}}</a>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br />
            <!-- Success msg -->
            <p>
                 @if(Session::has('message'))
                    {{ Session::get('message') }}
                 @endif
            </p>

            <div class="row">
                <div class="col-md-4">
                    <img src="/{{ $article->img_preview  }}" title="{{ $article->title  }}" />
                    <br />
                    <a href="{{action('ArticlesController@edit',['id'=>$article->id])}}" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span>{{trans('main.edit')}}</a>
                </div>
                <div class="col-md-8">
                    <table class="table table-striped">
                        <tr>
                            <td>Название:</td>
                            <td>{{ $article->title }}</td>
                        </tr>
                        <tr>
                            <td>Контент:</td>
                            <td>{{ $article->content }}</td>
                        </tr>
                        <tr>
                            <td>Категория:</td>
                            <td>{{ trans($category->langkey) }}</td>
                        </tr>
                        <tr>
                            <td>Тип:</td>
                            <td>{{ trans($_mod['articles']['type_id'][$article->type_id])  }}</td>
                        </tr>
                        <tr>
                            <td>Показывать:</td>
                            <td>{{ trans($_mod['articles']['visible'][$article->visible])  }}</td>
                        </tr>
                        <tr>
                            <td>Комментировать:</td>
                            <td>{{ trans($_mod['articles']['visible'][$article->commented])  }}</td>
                        </tr>
                    </table>
                </div>
            </div>


            
        </div>
    </div>
    
</div>
@endsection