@extends('layouts.app')


@section('content')
<div class="container">
    @if (Auth::guest())
        <!--  -->
    @else
        @include('profile.menu')
    @endif

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="{{action('ArticlesController@create',[])}}" class="btn btn-primary pull-right">{{trans('main.add')}}</a>
        </div>
    </div>

    @if (count($articles) < 1)
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {{trans('main.emptyarticles')}}
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table class="table table-striped">
            <tr>
                <th>id</th>
                <th>Название</th>
                <th>Тип</th>
                <th style="width: 20%">Редактировать</th>
                <th style="width: 20%">Удалить</th>
            </tr>

            @foreach ($articles as $key => $article)
            <tr>
                <th>{{ ++$key }}</th>
                <td> <a href="{{action('ArticlesController@show',['id'=>$article->id])}}">{{ $article->title }}</a></td>
                <td>{{ trans($_mod['articles']['type_id'][$article->type_id])  }}</td>
                <td> <a href="{{action('ArticlesController@edit',['id'=>$article->id])}}" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span>{{trans('main.edit')}}</a></td>
                <td> <a href=""
                    data-url="{{action('ArticlesController@destroy',['id'=>$article->id])}}"
                    data-id="{{ $article->id  }}"
                    class="btn btn-danger btn-xs del"><span class="glyphicon glyphicon-remove"></span>{{trans('main.delete')}}</a></td>
            </tr>
            @endforeach
            </table>

            {{ $articles->links()  }}

        </div>
    </div>
    @endif
    
</div>
@endsection