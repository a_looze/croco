@extends('layouts.app')


@section('content')
<div class="container">
    @if (Auth::guest())
        <!--  -->
    @else
        @include('profile.menu')
    @endif

    <!-- <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="{{action('ArticlesController@create')}}">{{trans('main.add')}}</a>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="form-area">
                <form role="form" method="POST" action="{{ action('ArticlesController@store') }}" enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <br style="clear:both">

                    <h3 style="margin-bottom: 25px; text-align: center;">Добавить новую тему:</h3>

                    <div class="form-group">
                        <input type="text" class="form-control" value="" id="title" name="title" placeholder="Title" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value="" id="content" name="content" placeholder="Content" required>
                    </div>

                    <div class="form-group">
                        <span>Категория</span>
                        <select class="form-control" name="category_id" data-style="btn-primary">
                            <option value="">Выберите категорию</option>
                             @-foreach($categories as $category)

                            <option value="{-{$category->id}}">{-{$-category->title}}</option>

                            @-endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">Загрузить</span>
                                    <input type="file" value="{{ Auth::user()->avatar  }}" accept="image/png, image/jpeg, image/gif" name="img"/> <!-- rename it -->
                                </div>
                            </span>

                        </div><!-- /input-group image-preview [TO HERE]-->
                    </div>

                    <div class="form-group">
                        <span>Публиковать</span>
                        <select class="form-control" name="isvisible" data-style="btn-primary">
                            <option value="1">Да</option>
                            <option value="1">Нет</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <span>Разрешить комментарии</span>
                        <select class="form-control" name="iscomment" data-style="btn-primary">
                            <option value="1">Да</option>
                            <option value="1">Нет</option>
                        </select>
                    </div>


                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Сохранить</button>
                </form>
            </div>

        </div>
    </div>
    
</div>
@endsection