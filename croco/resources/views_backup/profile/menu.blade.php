    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">{{trans('main.ownmenu')}}</div> -->
                <div class="panel-body">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/profile') }}">{{trans('main.profile')}}</a></li>
                        <li><a href="{{ url('/profile/edit') }}">{{trans('main.personal')}}</a></li>
                        <li><a href="{{ url('/profile/articles') }}">{{trans('main.articles')}}</a></li>
                        <!-- <li><a href="{{ url('/admin') }}"><i class="fa fa-btn fa-cog"></i>{{trans('main.admin')}}</a></li>
                        <li><a href="{{ url('/admin') }}"><i class="fa fa-btn fa-cog"></i>{{trans('main.admin')}}</a></li>
                        <li><a href="{{ url('/admin') }}"><i class="fa fa-btn fa-cog"></i>{{trans('main.admin')}}</a></li> -->
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>