<?php

return [

    'langswitcher' => 'Change',
    'locale'              => [
        'en' => 'English',
        'ru'    => 'Russian',
        'fr'  => 'France',
    ],
    'current_lang' => 'Language is "english"',
    'sitestart' => 'Main page',
    'profile' => 'Profile',
    'admin' => 'Admin panel',
    'ownmenu' => 'My menu',
    'personal' => 'My data',
    'questions' => 'Мои вопросы',
    'articles' => 'My themes',
    'emptyarticles' => 'Empty list',
    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'show_down' => 'Show Down',
    'show_up' => 'Close Up',
    'search' => 'Поиск',

    'messages' => [
        'updated' => 'The data was updated',
    ],

    // меню
    'mstart' => 'Start',
    'mprofile' => 'Profile',
    'mnew' => 'New',
    'mpopul' => 'Pop',
    'mrev' => 'Recently viewed',
    'mlike' => 'Liked',
    'mrec' => 'Recomended',
    'seemore' => 'see more',
    'more' => 'more',
    'mtagsearch' => 'Search by hashtag #:tag',
    'msearch' => 'Search by :str',
    'search' => 'Search',
    'users' => 'Users',


    'article' => [
        'yes' => 'Yes',
        'no' => 'No',
        'audio' => 'audio',
        'video' => 'video',
        'image' => 'image',
    ],

    'correctansw' => 'Correct answer',
    'wrongansw' => 'Wrong answer',
    'qansw' => 'Автор еще не оценил ответ',

    // функции админа
    'banpost' => 'Ban question',
    'bancomment' => 'Ban comment',
    'bananswer' => 'Ban answer',

    // названия категорий
    'playcat' => 'Play question',
    'dancecat' => 'Dance question',
    'drawcat' => 'Draw question',
    'movecat' => 'Ask with movies',
    'voicecat' => 'Voice ask',

    'settings' => 'Настройки',
    'categoryh1' => 'Questions',
    'articleh2' => 'Question',
    'answers' => 'Answers',
    'sortby' => 'Sort by',
    'emptycat' => 'This category empty',
    'unsubscribe' => 'Unsubscribe',
    'subscribes' => 'Subscribes',
    'subscribe' => 'Subscribe',
    'subscribers' => '{0} subscribers|{1} subscriber|[2,Inf] subscribers',
    'views' => 'views',
    'addanswer' => 'Add answer',
    'seeauthoranswer' => 'Show author answer',
    'authoranswer' => 'Author answer',
    'areyousure' => 'Are you sure? Comm will be disabled for this question',
    'comments' => 'Comments',
    'addcomment' => 'Add comment',
    'otherarticles' => 'Another questions',
    'warnings' => 'Warnings',
    'library' => 'Библиотека файлов',
    'add_file_to_library' => 'Добавить файл в библиотеку',
    'by_date' => 'По дате добавления',
    'by_type' => 'По типу',
    'by_popularity' => 'По популярности',
    'by_question' => 'По наличию ответов',
    'save' => 'Save',
    'own_data' => 'Личные данные',
    'name' => 'Имя (псевдоним)',
    'country' => 'Страна',
    'change_preview' => 'Изменить превью',
    'cancel' => 'Отмена',

    // авторизация
    'enter' => 'Вход',
    'registration' => 'Регистрация',
    'forgot_password' => 'Забыли пароль?',
    'welcome' => 'Добро пожаловать',
    'your_name' => 'Ваше имя или псевдоним',
    'your_email' => 'Ваш e-mail',
    'create_password' => 'Придумайте пароль',
    'repeat_password' => 'Повторте пароль',
    'logout' => 'Выход',
    'no_country' => 'Страна не выбрана',
    'old_password' => 'Старый пароль',
    'change_password' => 'Изменить пароль',

    // страница настроек для юзера
    'notice_article_like' => 'Уведомлять об отметках "мне нравится" для вопросов',
    'notice_answer_like' => 'Уведомлять об отметках "мне нравится" для ответов',
    'notice_comment_like' => 'Уведомлять об отметках "мне нравится" для комментариев',
    'notice_referral_sign' => 'Уведомлять о регистрации по вашей ссылке',

    // страница уведомлений
    'notices' => 'Уведомления',
    'added_new_answer' => 'добавил новый ответ на',
    'your_question' => 'ваш вопрос',
    'your_answer' => 'Ваш ответ',
    'has_been_blocked' => 'заблокировали',
    'your_warnings' => 'Вам вынесено предупреждение',
    'added_new_question' => 'добавил новый вопрос',
    'your_question_has_been_blocked' => 'Ваш вопрос заблокировали',
    'your_are_in_black_list' => 'Вы занесены в черный список',
    'your_position' => 'вы заняли',
    'in_common_rating' => 'в общем рейтинге сайта',
    'at_the_end_of_the_period' => 'По итогам периода',

    // страница бонусов
    'bonuses' => 'Бонусы',
    'count_bonuses' => 'бонусов на счете',
    'bonus_rating' => 'Ваше место в рейтинге',
    'earn_bonuses' => 'Зарабатывай Comm`ы приглашая друзей по своей уникальной ссылке',
    'copy' => 'Скопировать',
    'for_what_bonuses' => 'За что еще начисляются Comm`ы',
    'question' => 'Вопрос',
    'answer' => 'Ответ',
    'right_answer' => 'Ответ правильны(совпал с ответом автора)',
    'interesting_question' => 'Интересный вопрос',
    'for_every' => 'за каждые',
    'like' => 'Отметить лайком',
    'hit_question_answer' => 'Хит вопрос, ответ',
    'likes_during_month' => 'лайков(набранные в течение месяца).',
    'comment' => 'Комментарий',
    'friends_registration' => 'Регистрация друга',
    'subscribe_to' => 'Подписка на автора\канал',
    'dont_in_rating' => 'Вы не участвуете в рейтинге',

    // страница добавления статьи
    'add_question'  => 'Добавить вопрос',
    'choose_category' => 'Выберите раздел',
    'upload_file' => 'Загрузить файл',
    'choose_category_description' => 'Раздел, где ты задаешь вопрос с помощью музыки на любых инструментах, свистом, на губах ,с помощью подручных предметов и т.д.',
    'upload_question_file' => 'Загрузить файл вопроса',
    'drop_file_here' => 'Перетащите сюда ваш файл или кликните по области',
    'add_file' => 'Добавить файл',
    'you_can_choose_file_from' => 'Также можно выбрать файл из',
    'from_your_library' => 'вашей библиотеки',
    'upload_question' => 'Загрузить вопрос',
    'add_question_preview' => 'Добавить привью для вопроса',
    'next' => 'Пропустить',
    'add_answer' => 'Добавить ответ',
    'next_and_save' => 'Пропустить и сохранить изменения',
    'add_answer_preview' => 'Добавить привью для отвтета',
    'congrats' => 'МОЛОДЕЦ!',
    'go_to_profile' => 'Перейти к профилю',
    'edit_file_library' => 'Редактировать файл из библиотеки',
    'edit_question' => 'Редактировать вопрос',
    'preview' => 'Превью',
    'file_will_be_added_from_library' => 'ФАЙЛ БУДЕТ ДОБАВЛЕН ИЗ БИБЛИОТЕКИ',
    'description' => 'Описание',
    'tags' => 'Теги',
    'let_see_article' => 'Показывать вопрос',
    'choice_for_whom' => 'Вставьте ссылку на профиль пользователя, кому адресован вопрос',
    'add_comment' => 'Добавить Комментарий',

    // страница вопроса
    'success_answer' => 'Ваш ответ был добавлен и находится в обработке',
    'success_comment' => 'Ваш комментарий был добавлен и находится в обработке',
    'answer_comment' => 'Комментировать',
    'claim' => 'Пожаловаться',

    // страница добавления файла
    'file_type_error' => 'Файл неправильного типа',
    'choose_file' => 'Выберите файл',
    'file_add_success' => 'Файлы были успешно добавлены в вашу библиотеку!',
    'add_file_to_lib_again' => 'Доавить еше файлы в библиотеку?',
    'file_name' => 'Название файла',
    'file_description' => 'Описание файла',

    // ответы
    'edit_answer' => 'Редактировать ответ',
    'create_answer' => 'Добавить свой ответ',

    // статические страницы
    'about' => [
        'title' => 'About',
        'desc' => 'About',
        'keyw' => 'About',
        'content' => 'About',
    ],
    
    'foruser' => [
        'title' => 'For users',
        'desc' => 'For users',
        'keyw' => 'For users',
        'content' => 'For users',
    ],

    // тексты писем
    'mail' => [
        'subj' => 'Сообщение с сайта Commfi',
        'h1' => 'Добрый день, :username',
        'successreg' => 'Поздравляем! Вы успешно зарегистрировались',
        'successedit' => 'Ваши данные были успешно изменены',
        'eventsubs' => 'Пользователь :user подписался на ваш канал',
        'eventnewarticle' => 'В категории :cat появились новые вопросы',
        'eventlike' => 'Пользователь :user отметил ваш вопрос :article',
        'eventdislike' => 'Пользователь :user снял отметку с вашего вопроса :article',
        'eventalike' => 'Пользователь :user отметил ваш ответ в теме :article',
        'eventadislike' => 'Пользователь :user снял отметку с вашего ответа в теме :article',
        'eventclike' => 'Пользователь :user отметил ваш комментарий в теме :article',
        'eventcdislike' => 'Пользователь :user снял отметку с вашего комментария в теме :article',
        'eventaok' => 'Ваш ответ в теме :article отмечен как верный',
        'eventabad' => 'Ваш ответ в теме :article отмечен как неверный',
        'eventaddbonus' => 'Вам начислено :sum бонусов',
        'eventdelbonus' => 'У вас списано :sum бонусов',
    ],

    // оповещения о событиях
    'event' => [
        'bonusadd' => 'Начислены бонусы :sum',
        'bonusdel' => 'Списаны бонусы :sum',
        'authorres1' => 'Ответ верный',
        'authorres0' => 'Ответ неверный',
    ],

    // Уведомления
    // '19' - соотвествует id = 19 из таблицы events
    'notice' => [
        '19' => 'Пользователь :user задал Вам вопрос :article',
    ],

    // JS VALIDATION
    'js_validation' => [
        'error_size' => 'Файл слишком большой',
        'error_type' => 'Не подходящего типа',
        'error_no_files_added' => 'Добавьте файл!',
    ],

];
