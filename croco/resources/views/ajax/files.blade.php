@foreach($files as $file)
<div class="item">
    <div class="img">
        <a href="{{ action('FileController@edit', ['id' => $file['id']]) }}"><img class="imgitem" src="{{ $file['preview'] }}"></a>
        <div class="stats">
            <div class="icons">
                <img class="ico" src="images/icon/{{ $file['type_id'] }}.png">
            </div>
        </div>
    </div>
    <div class="userinfo">
        <div class="user">
            <div>
                <p class="pubDate">{{ $file['created_at'] }}</p>
            </div>
        </div>
    </div>
</div>
@endforeach