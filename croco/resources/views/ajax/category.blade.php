@foreach ($articles as $article)
    <div class="item">
        <div class="img">
            <a href="{{ url('/v/' . $article->alias) }}">
                @if ($article->preview)
                <img class="imgitem" src="{{ $article->preview->resize(300, 200) }}">
                @elseif ($article->attach->type_id == 3)
                <img class="imgitem" src="{{ $article->attach->resize(300, 200) }}">
                @else
                <img class="avatar" src="{{ config('filesystems.default_path.article_preview') }}">
                @endif
            </a>
            <div class="stats">
                <div class="soc">
                    <span class="like" data-target="false" data-iid="{{ $article->id}}">{{ $article->rating }}</span>
                    <span class="answer">{{ $article->answers->count() }}</span>
                </div>
                <div class="icons">
                    <span class="ttp" data-title="{{ $langCountry[$article->author->country_id] }}">
                        <img class="country" src="images/flag/{{ $article->author->country_id }}.jpg">
                    </span>
                    
                    <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                </div>
            </div>
        </div>
        <div class="userinfo">
            <div class="user">                    
                <a href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}" title="{{ $article->author->name }}">
                    @if ($article->author->avatar)
                        <img class="avatar" src="{{ $article->author->avatar }}"> 
                    @else
                        <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                    @endif
                </a>
                <div>
                    <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}">
                        {{ $article->author->name }}
                    </a> 
                    <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                </div>
            </div>
            <div class="views">
                <p>{{ trans('main.views') }}</p>
                <p>{{ $article->views }}</p>
            </div>
        </div>
    </div>
@endforeach
@if ($hasMorePages && $page == 1)
    <div class="item empty" id="div4button">
        <a href="javascript:void(0);" class="greybut center pushable" id="morebutton">{{ trans('main.more') }}</a>
    </div>
@endif