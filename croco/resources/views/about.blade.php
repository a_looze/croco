@extends('layouts.app')

@section('content')
<div id="section">
    <div class="section chat">
        <div class="mask"></div>
        <div class="wrap">
            <div>
                <noindex><h3>{{ trans('main.about.1') }}</h3></noindex>
                {!! trans('main.about.2') !!}
            </div>
        </div>
    </div>

    <div class="section look">
        <div class="wrap">
            <div>
                <noindex><h3>{{ trans('main.about.3') }}</h3></noindex>
                {!! trans('main.about.4') !!}
            </div>
        </div>
    </div>

    <div class="section game">
        <div class="wrap">
            <div>
                <noindex><h3>{{ trans('main.about.5') }}</h3></noindex>
                {!! trans('main.about.6') !!}
                
                <div id="aboutVideo">
                    <video id="video" class="video-js vjs-fluid placeholder" controls 
                                        poster="images/thumbs/79/691-a-850-540.jpg"
                                        data-setup='{ "controls": true, "autoplay": false}' width="850px" height="540px">

                            <source src="assets/media/users/79/1/691.mp4" type='video/mp4'>
                            <source src="assets/media/users/79/1/691.webm" type='video/webm'>
                            <source src="assets/media/users/79/1/691.ogv" type='video/ogg'>
                    </video>  
                </div> 
                <a href="javascript:;" class="gameMore">{{ trans('main.more') }}</a>
            </div>
        </div>
    </div> 
    
     
                
    <div class="section gameComm">
        <div class="wrap">
            <div>
                <h4>{{ trans('main.about.10') }}</h4>
                <div class="bbWrap">    
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.11') }}</div>
                        <div class="bbPrice"><span>+ 10 <img src="images/logo.png"></span></div>
                        <div class="bbText">{{ trans('main.about.12') }}</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.13') }}</div>
                        <div class="bbPrice"><span>+ 10 <img src="images/logo.png"></span></div>
                        <div class="bbText">{{ trans('main.about.14') }}</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.15') }}</div>
                        <div class="bbPrice"><span>+ 15 <img src="images/logo.png"></span></div>
                        <div class="bbText">{{ trans('main.about.16') }}</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.17') }}</div>
                        <div class="bbPrice"><span>+ 55 <img src="images/logo.png"></span></div> 
                        <div class="bbText">{{ trans('main.about.18') }}</div> 
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.19') }}</div> 
                        <div class="bbPrice"><span>+ 23 <img src="images/logo.png"></span></div> 
                        <div class="bbText">{{ trans('main.about.20') }}</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.21') }}</div> 
                        <div class="bbPrice"><span>+ 1000 <img src="images/logo.png"></span></div>
                        <div class="bbText">{{ trans('main.about.22') }}</div> 
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.23') }}</div> 
                        <div class="bbPrice"><span>+ 4 <img src="images/logo.png"></span></div>
                        <div class="bbText">{{ trans('main.about.24') }}</div> 
                    </div>
                    <div class="bb">
                        <div class="bbName">{{ trans('main.about.25') }}</div> 
                        <div class="bbPrice"><span>+ 5 <img src="images/logo.png"></span></div> 
                        <div class="bbText">{{ trans('main.about.26') }}</div> 
                    </div>
                    <div class="bb last">
                        <div class="bbName">{{ trans('main.about.27') }}</div> 
                        <div class="bbPrice"><span>+ 1 <img src="images/logo.png"></span></div> 
                        <div class="bbText">{{ trans('main.about.28') }}</div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section world">
        <div class="mask"></div>
        <div class="wrap">
            <div>
                <noindex><h3>{{ trans('main.about.7') }}</h3></noindex>
                {!! trans('main.about.8') !!}
                <a href="{{ url('/register')  }}" class="redbut bigbut">{{ trans('main.about.9') }}</a>
            </div>
        </div>
    </div>

</div>
@endsection