@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
        <h3>{{ trans('main.subscribes') }}</h3>
    </div>
    <div class="subscribe min-height">
        @foreach ($subscribes as $subs)
        <div class="subItem">
            <a href="{{ action('UserPagesController@index', ['alias' => $subs->channel->alias]) }}">
                <img src="{{ $subs->channel->avatar ? : config('filesystems.default_path.user_avatar') }}">
                {{ $subs->channel->name }}
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
