@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
      <h3>{{ trans('main.settings') }}</h3>
    </div>
    <div class="browse">

    @if(Session::has('message'))
        <div id="success-answer">
            <p>{{ trans(Session::get('message')) }}</p>
        </div>
    @endif

    @if (count($errors) > 0)
        <div id="error-answer">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ trans($error) }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ action('UserController@update') }}">

        {{ csrf_field() }}

        <input type="hidden" name="notice_article_like" value="{{ Auth::user()->notice_article_like }}" id="notice_article_like"/>
        <input type="hidden" name="notice_answer_like" value="{{ Auth::user()->notice_answer_like }}" id="notice_answer_like"/>
        <input type="hidden" name="notice_comment_like" value="{{ Auth::user()->notice_comment_like }}" id="notice_comment_like"/>
        <input type="hidden" name="notice_referral_sign" value="{{ Auth::user()->notice_referral_sign }}" id="notice_referral_sign"/>

        <div class="settingRow">
            <h5>- {{ trans('main.own_data') }} -</h5>
            <div class="settingItem">
                <h6>{{ trans('main.name') }}</h6>
                <input type="text" placeholder="{{ trans('main.name') }}" name="name" value="{{ Auth::user()->name  }}">
                 @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                 @endif
            </div>
            <div class="settingItem">
                <h6>E-mail</h6>
                <input type="email" placeholder="Ваш e-mail" name="email" value="{{ Auth::user()->email  }}" disabled>
            </div>
            <div class="settingItem">
                <h6>{{ trans('main.country') }}</h6>
                <select name="country_id">
                @foreach($countries as $id => $country)
                    @if ($id == Auth::user()->country_id)
                        <option value="{{ $id  }}" selected>{{ $country }}</option>
                    @else
                        <option value="{{ $id  }}">{{ $country }}</option>
                    @endif
                @endforeach
                </select>
            </div>
        </div>

        <div class="settingRow">
            <h5>- {{ trans('main.change_password') }} -</h5>
            <div class="settingItem">
                <h6>{{ trans('main.old_password') }}</h6>
                <input type="password" name="old_password">
                @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('old_password') }}</strong>
                          </span>
                      @endif
            </div>
            <div class="settingItem">
                <h6>{{ trans('main.create_password')  }}</h6>
                <input  type="password" name="password">
                @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
            </div>
            <div class="settingItem">
                <h6>{{ trans('main.repeat_password') }}</h6>
                <input type="password" name="password_confirmation">
                @if ($errors->has('password_confirmation'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password_confirmation') }}</strong>
                          </span>
                      @endif
            </div>
        </div>


        <div class="settingRow">
            <h5>- {{ trans('main.warnings') }} -</h5>
            <div class="settingItem">
                 <i>{{ trans('main.notice_article_like') }}</i>
                 <span class="switch switch-green">
                    <input class="setting-check article-setting" data-target="notice_article_like" name="notice_article_like-check" type="checkbox" id="s1" {{ Auth::user()->notice_article_like ? 'checked' : '' }}>
                    <label for="s1" data-on="On" data-off="Off"></label>
                 </span>
                 <div class="clear"></div>
            </div>
            <div class="settingItem">
                <i>{{ trans('main.notice_answer_like') }}</i>
                <span class="switch switch-green">
                  <input class="setting-check article-setting" data-target="notice_answer_like" name="notice_answer_like-check"  type="checkbox" id="s2" {{ Auth::user()->notice_answer_like ? 'checked' : '' }}>
                  <label for="s2" data-on="On" data-off="Off"></label>
                </span>
                <div class="clear"></div>
            </div>
             <div class="settingItem">
                <i>{{ trans('main.notice_comment_like') }}</i>
                <span class="switch switch-green">
                  <input class="setting-check article-setting" data-target="notice_comment_like" name="notice_comment_like-check" type="checkbox" id="s3" {{ Auth::user()->notice_comment_like ? 'checked' : '' }}>
                  <label for="s3" data-on="On" data-off="Off"></label>
                </span>
                <div class="clear"></div>
            </div>

            <div class="settingItem">
                <i>{{ trans('main.notice_referral_sign') }}</i>
                <span class="switch switch-green">
                  <input class="setting-check article-setting" data-target="notice_referral_sign" name="notice_referral_sign-check" type="checkbox" id="s4" {{ Auth::user()->notice_referral_sign ? 'checked' : '' }}>
                  <label for="s4" data-on="On" data-off="Off"></label>
                </span>
                <div class="clear"></div>
            </div>

        </div>

        <button type="submit" class="greenbut">{{ trans('main.save') }}</button>

    </form>

    </div>
        <div class="clear"></div>
</div>
@endsection
