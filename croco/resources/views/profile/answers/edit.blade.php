@extends('layouts.app')

@section('content')

<div class="row">

    <div class="head">
        <h3>{{ trans('main.edit_answer')}}</h3>
    </div>

    <div class="browse">
        <div class="contWrap">

              <!-- Success msg -->

             @if(Session::has('message'))
                 <div id="success-answer">
                     <p>
                        {{ trans(Session::get('message')) }}
                    </p>
                </div>
             @endif

            <div class="editFile">
                <div class="editImg">
                    <a href="javascript:void(0)" class="input-button" data-target="preview-from-input">
                        <img id="preview-image-file" src="{{ $answer->previewPath }}">
                        <span>{{ trans('main.change_preview') }}</span>
                    </a>
                </div>

                <form style="display:none;"
                    action="{{ action('AnswerController@update', ['id' => $answer->id]) }}"
                    method="post"
                    id="lib"
                    name="preview"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put"/>
                    <input type="hidden" name="title" value="{{ $answer->title }}" id="title" />
                    <input id="preview-from-input"
                           data-draw="preview-image-file"
                           data-error="error-file"
                           class="preview-from-input redbut" type="file" name="file" value=""/>
                    <textarea id="description" style="display: none;" name="description">{{ $answer->description }}</textarea>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

                <!-- ERROR -->
                <p id="error-file" class="error-msg-block"></p>
                @if ($answer->author_say == 3)
                <a href="{{ action('ArticlesController@edit', ['id' => $answer->article_id]) }}" class="yellbut margin-20">{{ trans('main.edit_question')}}</a>
                @endif
                <!-- SUBMIT FORM -->
                <a href="javascript:void(0);" class="greenbut margin-20" onclick="document.getElementById('lib').submit()">{{ trans('main.save')}}</a>

            </div>
        </div>
    </div>

    <div class="clear"></div>

</div>
@endsection
