@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">

    <div class="head">
        <h3>{{ trans('main.edit_question')}}</h3>
    </div>



    <div class="browse">
        <div class="contWrap">

             <!-- Success msg -->
             @if(Session::has('message'))
             <div id="success-answer">
                <p>
                   {{ trans(Session::get('message')) }}
                </p>
             </div>
             @endif

            <div class="editFile" id="article-edit">
                <h4>{{ trans('main.edit_question') }}</h4>
                <div class="editImg">
                    <a href="javascript:void(0)" class="input-button" data-target="preview-from-input">
                        <img id="preview-image-file" src="{{ $article->previewPath }}">
                        <span>{{ trans('main.change_preview') }}</span>
                    </a>
                </div>

                <form style="display:none;"
                    action="{{ action('ArticlesController@update', ['id' => $article->id]) }}"
                    method="post"
                    id="article"
                    name="preview"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put"/>
                    <input type="hidden" name="title" value="{{ $article->title }}" id="title" />
                    <input type="hidden" name="file_from_lib" class="preview-from-lib" id="file-from-lib" value=""/>
                    <input type="hidden" name="visible" value="{{ $article->visible }}" id="visible"/>
                    <input id="preview-from-input"
                           data-draw="preview-image-file"
                           data-error="error-article"
                           class="preview-from-input redbut" type="file" name="file" value="{{ $article->previewPath }}"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

                <!-- TITLE -->
                <!-- <h4>{{ trans('main.file_name') }}</h4>
                <div class="editImg"><input class="has-target" type="text" placeholder="{{ trans('main.file_name') }}" data-target="title" value="{{ $article->title }}"></div> -->

                <!-- TAGS -->
                <!-- <h4 class="margin-20">{{ trans('main.tags') }}</h4>
                <div class="example example_typeahead">
                    <div class="bs-example">
                        <input type="text" name="tags" form="article" class="tags" value="{{ $tagsString }}" />
                    </div>
                </div> -->  

                <!-- SETTINGS -->
                <div class="settingRow margin-20">
                    <h5>- {{ trans('main.settings') }} -</h5>
                    <div class="settingItem">
                        <i>{{ trans('main.let_see_article') }}</i>
                        <span class="switch switch-green">
                          <input class="setting-check article-setting" form="article" data-target="visible" name="visible-check" type="checkbox" id="s1" {{ $article->visible ? 'checked' : '' }}>
                          <label for="s1" data-on="On" data-off="Off"></label>
                        </span>
                    </div>
                </div>

                <!-- ERROR -->
                <p id="error-article" class="error-msg-block"></p>

                @if (!is_null($answer))
                <a href="{{ $answer->action }}" id="edit-answer-button" class="yellbut margin-20">{{ trans('main.edit_answer')}}</a>
                @else
                <a href="javascript:void(0);" id="create-answer-button" data-target="answer-create" class="yellbut margin-20 show-by-target">{{ trans('main.create_answer')}}</a>
                <a href="javascript:void(0);" id="edit-answer-button" class="yellbut margin-20" style="display: none;">{{ trans('main.edit_answer')}}</a>
                @endif

                <!-- SUBMIT FORM -->
                <a href="javascript:void(0);" class="redbut margin-20" onclick="document.getElementById('article').submit()">{{ trans('main.save')}}</a>

            </div>

            @if (is_null($answer))
            <div id="success-answer" style="display: none">
                <p>{{ trans('main.success_answer') }}</p>
            </div>

            <!-- ADD ANSWER -->
            <div class="editFile" style="" id="answer-create">

                <h4>{{ trans('main.add_answer')}}</h4>

                <form id="answer" action="{{ action('AnswerController@store') }}" class="dropzone"  enctype="multipart/form-data">

                    <div class="add-answer">
                        <span>{{ trans('main.drop_file_here')}}</span>
                        <a id="add-answer" href="javascript:void(0);" class="greenbut dz-clickable">{{ trans('main.add_answer')}}</a>
                        <div>
                            <span>{{ trans('main.you_can_choose_file_from')}}
                            <a id="file-from-lib-link-3" class="show-file-lib" href="javascript:void(0);"
                                data-modal="lib-modal"
                                data-target="file-from-lib-3"
                                data-remove=""
                                data-action="file">{{ trans('main.from_your_library')}}
                            </a>
                            </span>
                        </div>
                    </div>

                    <input type="hidden" name="article_id" value="{{ $article->id }}"/>
                    <input type="hidden" name="author_say" value="3"/>
                    <input type="hidden" name="_method" value="post"/>
                    <input type="hidden" name="file_from_lib" id="file-from-lib-3" value=""/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                </form>

                <!-- msg block -->
                <p class="msg-block"> {{ trans('main.file_will_be_added_from_library')  }} <a class="greybut cansel-file" data-remove="file-from-lib-3" href="javascript:void(0)">Отмена</a></p>
                <!-- /msg block -->

                <!-- ERROR -->
                <p id="error-answer" class="error-msg-block"></p>

                <!-- submit -->
                <br />
                <a id="submit-answer" href="javascript:void(0);" class="yellbut margin-20">{{ trans('main.save')}}</a>

            </div>
            @endif

        </div>
    </div>

    <div class="clear"></div>

</div>
@endsection
