@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
   <div class="head">
       <h3>{{ trans('main.subscribes') }}</h3>
       <a href="javascript:void(0)" data-target="subscribe" class="more">{{ trans('main.show_down') }}</a>
   </div>
   <div class="subscribe">
       <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
        <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
        <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
        <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
        <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
        <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
        <div class="subItem">
           <a href="#"><img src="images/demo/user.jpg">Pires Gomes</a>
       </div>
       <div class="subItem">
           <a href="#"><img src="images/demo/user2.jpg">Ninja MasterMaster</a>
       </div>
   </div>
</div>

<div class="row chanel">
    <div class="head">
        <h3>{{ trans('main.categoryh1') }}</h3>
        <div id="sort">
            <a href="#" class="sortIco">{{ trans('main.sortby') }}</a>
            <div id="sortParam">
                <a href="#" class="sP">{{ trans('main.by_date') }}</a>
                <a href="#" class="sP active">{{ trans('main.by_popularity') }}</a>
                <a href="#" class="sP">{{ trans('main.by_type') }}</a>
                <a href="#" class="sP">{{ trans('main.by_question') }}</a>
            </div>
        </div>
    </div>
        <div class="browse rsmall">
            <div class="item">
                <div class="img">
                    <a href="#"><img class="imgitem" src="images/demo/pic.jpg"></a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like">13</span>
                            <span class="answer newAnswer">6</span>
                        </div>
                        <div class="icons">
                            <img class="country" src="images/demo/ca.jpg">
                            <img class="ico" src="images/icon/video.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="#" title="Wild Animals"><img src="images/demo/user.jpg"</a>
                        <div>
                            <a class="userName" href="#">Wild Animals</a>
                            <p class="pubDate">10.11.2016</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>просмотров</p>
                        <p>100 556</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="img">
                    <a href="#"><img class="imgitem" src="images/demo/pic.jpg"></a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like">13</span>
                            <span class="answer">6</span>
                        </div>
                        <div class="icons">
                            <img class="country" src="images/demo/ca.jpg">
                            <img class="ico" src="images/icon/video.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="#" title="Wild Animals"><img src="images/demo/user.jpg"</a>
                        <div>
                            <a class="userName" href="#">Wild Animals</a>
                            <p class="pubDate">10.11.2016</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>просмотров</p>
                        <p>100 556</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="img">
                    <a href="#"><img class="imgitem" src="images/demo/pic.jpg"></a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like">13</span>
                            <span class="answer">6</span>
                        </div>
                        <div class="icons">
                            <img class="country" src="images/demo/ca.jpg">
                            <img class="ico" src="images/icon/video.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="#" title="Wild Animals"><img src="images/demo/user.jpg"</a>
                        <div>
                            <a class="userName" href="#">Wild Animals</a>
                            <p class="pubDate">10.11.2016</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>просмотров</p>
                        <p>100 556</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="img">
                    <a href="#"><img class="imgitem" src="images/demo/pic.jpg"></a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like">13</span>
                            <span class="answer">6</span>
                        </div>
                        <div class="icons">
                            <img class="country" src="images/demo/ca.jpg">
                            <img class="ico" src="images/icon/video.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="#" title="Wild Animals"><img src="images/demo/user.jpg"</a>
                        <div>
                            <a class="userName" href="#">Wild Animals</a>
                            <p class="pubDate">10.11.2016</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>просмотров</p>
                        <p>100 556</p>
                    </div>
                </div>
            </div>
             <div class="item">
                <div class="img">
                    <a href="#"><img class="imgitem" src="images/demo/pic.jpg"></a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like">13</span>
                            <span class="answer">6</span>
                        </div>
                        <div class="icons">
                            <img class="country" src="images/demo/ca.jpg">
                            <img class="ico" src="images/icon/video.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="#" title="Wild Animals"><img src="images/demo/user.jpg"</a>
                        <div>
                            <a class="userName" href="#">Wild Animals</a>
                            <p class="pubDate">10.11.2016</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>просмотров</p>
                        <p>100 556</p>
                    </div>
                </div>
            </div>
             <div class="item">
                <div class="img">
                    <a href="#"><img class="imgitem" src="images/demo/pic.jpg"></a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like">13</span>
                            <span class="answer">6</span>
                        </div>
                        <div class="icons">
                            <img class="country" src="images/demo/ca.jpg">
                            <img class="ico" src="images/icon/video.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="#" title="Wild Animals"><img src="images/demo/user.jpg"</a>
                        <div>
                            <a class="userName" href="#">Wild Animals</a>
                            <p class="pubDate">10.11.2016</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>просмотров</p>
                        <p>100 556</p>
                    </div>
                </div>
            </div>

        </div>

    <div class="clear"></div>
</div>
@endsection
