@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">

    <div class="head">
        <h3>{{ trans('main.questions') }}</h3>
        <a href="{{ action('ArticlesController@create') }}" class="yellbut">{{ trans('main.add_question') }}</a>
    </div>

    <div class="browse rsmall" id="autoposition">
        @if (count($articles) < 1)
        <div class="item empty">{{ trans('main.emptycat') }}</div>
        @endif

        @foreach ($articles as $article)
            <div class="item">
                <div class="img">
                    
                    <a href="{{ action('ArticlesController@edit', ['id' => $article->id]) }}" class="queRed"></a>
                    @if ($article->link)
                        <a href="{{ url('/v/' . $article->alias) }}">
                            <img class="imgitem" src="{{ $article->preview ? $article->preview->resize(300, 180) : ''}}">
                        </a>
                    @else
                        <span title="Файл на конвертации">
                            <img class="imgitem" src="{{ $article->preview ? $article->preview->resize(300, 180) : ''}}">
                        </span>
                    @endif

                    <div class="stats">
                        <div class="soc">
                            <span class="like" data-target="false" data-iid="{{ $article->id}}">{{ $article->rating }}</span>
                            @if ($article->alert)
                            <span class="answer newAnswer">{{ $article->answers()->where('author_say', '<>', '3')->count() }}</span>
                            @else
                            <span class="answer">{{ $article->answers->count() }}</span>
                            @endif
                        </div>
                        <div class="icons">
                            <img class="country" src="images/flag/{{ $article->author->country_id}}.jpg">
                            <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user"> 
                        <div>
                            {{ $article->title ? : 'No title' }}
                            <!-- @if ($article->link)
                                ~~
                            @else
                                !!
                            @endif -->
                            <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>{{ trans('main.views') }}</p>
                        <p>{{ $article->views }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="clear"></div>

</div>
@endsection
