@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="contWrap">
    <h3>{{ trans('main.add_question')}}</h3>

    <div class="stepsAdd">
        <div class="step">
            <h4>{{ trans('main.choose_category')}}</h4>
            <div><span class="step50"></span></div>
        </div>
        <div class="step long">
            <h4>{{ trans('main.upload_file')}}</h4>
            <div><span class="step-long"></span></div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="stepAction">

        <!-- SET CATEGORY -->
        <div class="step1">

            @foreach ($cats as $cat)
                <a href="javascript:void(0)" class="cat" data-id="{{$cat->id}}">
                <div>
                    <img src="images/{{$cat->letter}}Ava.jpg">
                    <span>
                        <h4>{{trans($cat->langkey)}}</h4>
                        <p>{{ trans($cat->langkey . 'desc') }}</p>
                    </span>
                </div>
                </a>
            @endforeach

        </div>

        <!-- UPLOAD ARTICLE FILE -->
        <div class="step2">
            <h4>{{ trans('main.upload_question_file')}}</h4>
            <form id="up" action="{{ action('ArticlesController@store') }}" class="dropzone" enctype="multipart/form-data">

                <div class="add-file">
                    <span>{{ trans('main.drop_file_here')}}</span>
                    <br/>
                    <a id="add-file" href="javascript:void(0);" class="greenbut dz-clickable">{{ trans('main.add_file')}}</a>
                    <div>
                        <span>{{ trans('main.you_can_choose_file_from')}}
                        <a id="file-from-lib-link" class="show-file-lib" href="javascript:void(0);"
                            data-modal="lib-modal"
                            data-target="file-from-lib"
                            data-remove=""
                            data-action="file">{{ trans('main.from_your_library')}}
                        </a>
                        </span>
                    </div>
                </div>

                <input type="hidden" name="category_id" value=""/>
                <input type="hidden" name="_method" value="post"/>
                <input type="hidden" name="file_from_lib" id="file-from-lib" value=""/>
                <input type="hidden" name="whom" id="whom" value="" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            </form>

            <!-- msg block -->
            <p class="msg-block"> {{ trans('main.file_will_be_added_from_library')  }} <a class="greybut cansel-file" data-remove="file-from-lib" href="javascript:void(0)">Отмена</a></p>
            <!-- /msg block -->

            <!-- user target -->
            <h4>{{ trans('main.choice_for_whom')}}</h4>
            <p id="whom-msg"></p>
            <div class="margin-20">
                <input type="text" name="whom" id="whom-input" value="" />
                <a href="javascript:void(0)" class="choice-for-whom greenbut" data-input="whom-input" data-target="whom" data-msg="whom-msg">{{ trans('main.search') }}</a>
            </div>

            <!-- ERROR -->
            <p id="error-article" class="error-msg-block"></p>

            <a id="submit-all" href="javascript:void(0);" class="redbut">{{ trans('main.upload_question')}}</a>
        </div>

    </div>

</div>

@endsection
