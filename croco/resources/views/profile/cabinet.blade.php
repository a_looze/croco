@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
      <h3>{{ trans('main.profile') }}</h3>
    </div>
    <div class="browse">

        <div class="settingRow">
            <h5>- {{ trans('main.own_data') }} -</h5>
            <div class="settingItem">
                <h6>{{ trans('main.name') }}</h6>
                {{ Auth::user()->name  }}
            </div>
            <div class="settingItem">
                <h6>E-mail</h6>
                {{ Auth::user()->email  }}
            </div>
            <div class="settingItem">
                <h6>{{ trans('main.country') }}</h6>
                @if (Auth::user()->country_id != 0)
                {{ $countries[Auth::user()->country_id] }}
                @else
                {{ trans('main.no_country') }}
                @endif
            </div>

            <a class="greenbut" href="{{ url('/profile/edit') }}">{{trans('main.personal')}}</a>

        </div>
    </div>
</div>
@endsection
