@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
      <h3>{{ trans('main.bonuses') }}</h3>
    </div>
    <div class="browse">

          <div class="walletWrap">
             <div class="walletHeader">
                 <div class="walletSumm">
                     <h4>Comm {{ trans('main.count_bonuses') }}</h4>
                     <strong>{{ $userBonus['bonus_amount'] }}</strong>
                 </div>
                 <div class="walletScore">
                     <h4>{{ trans('main.bonus_rating') }}</h4>
                     <strong>{{ $userBonus['pos'] }}</strong>
                 </div>
             </div>

            <div class="walletRef">
                {{ trans('main.earn_bonuses') }}:
                <div>
                    <span id="reflink">http://commfi.net/r/{{ Auth::user()->alias }}</span>
                    <a href="javascript:;" class="greybut" data-clipboard-action="copy" data-clipboard-target="#reflink" id="cp">
                        {{ trans('main.copy') }}
                    </a>
                </div>
            </div>

             <div class="walletComm">
                <h4>{{ trans('main.for_what_bonuses') }}</h4>
                    <div class="bbWrap">    
                    <div class="bb">
                        <div class="bbName">Задай вопрос</div>
                        <div class="bbPrice"><span>+ 10 <img src="images/logo.png"></span></div>
                        <div class="bbText">за 1 вопрос</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">Добавь ответ</div>
                        <div class="bbPrice"><span>+ 10 <img src="images/logo.png"></span></div>
                        <div class="bbText">за 1 ответ</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">Правильный ответ</div>
                        <div class="bbPrice"><span>+ 15 <img src="images/logo.png"></span></div>
                        <div class="bbText">за верный ответ</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">Интересный вопрос</div>
                        <div class="bbPrice"><span>+ 55 <img src="images/logo.png"></span></div> 
                        <div class="bbText">за 100 000 просмотров</div> 
                    </div>
                    <div class="bb">
                        <div class="bbName">Поставить лайк</div> 
                        <div class="bbPrice"><span>+ 23 <img src="images/logo.png"></span></div> 
                        <div class="bbText">за 1000 лайков</div>
                    </div>
                    <div class="bb">
                        <div class="bbName">Хит вопрос</div> 
                        <div class="bbPrice"><span>+ 1000 <img src="images/logo.png"></span></div>
                        <div class="bbText">за 1 000 000 лайков</div> 
                    </div>
                    <div class="bb">
                        <div class="bbName">Добавь комментарий</div> 
                        <div class="bbPrice"><span>+ 4 <img src="images/logo.png"></span></div>
                        <div class="bbText">за 1 комментарий</div> 
                    </div>
                    <div class="bb">
                        <div class="bbName">Пригласи друга</div> 
                        <div class="bbPrice"><span>+ 5 <img src="images/logo.png"></span></div> 
                        <div class="bbText">за 1 регистрацию</div> 
                    </div>
                    <div class="bb last">
                        <div class="bbName">Подпишись на автора/канал</div> 
                        <div class="bbPrice"><span>+ 1 <img src="images/logo.png"></span></div> 
                        <div class="bbText">за 1 подписку</div> 
                    </div>
                </div>
            </div>


          </div>


    </div>
        <div class="clear"></div>
</div>
@endsection
