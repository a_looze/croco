@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
      <h3>{{ trans('main.notices') }}</h3>
    </div>
    <div class="browse">
        @if (count($alerts) > 0)    
        <div class="notifyWrap">
            @foreach ($alerts as $alert)
            <div class="notifyItem">
                <div class="notifyItemAva">
                    <div class="grey">!</div>
                </div>
                <div class="notifyItemWrap">
                    {!! trans($alert->comment, $alert->data) !!}                    
                   <div class="notifyItemDate">{{ $alert->created_at }}</div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="notifyWrap">Пока нет уведомлений</div>
        @endif
    </div>
        <div class="clear"></div>
</div>
@endsection
