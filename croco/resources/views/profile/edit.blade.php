@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
    <h3>Редактировать профиль:</h3>
    </div>
    <div class="wellcome">

        @if(Session::has('message'))
            <p>{{ trans(Session::get('message')) }}</p>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ trans($error) }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

         <form class="loginForm regForm" role="form" method="POST" action="{{ action('UserController@save') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div>
                  <input type="text" placeholder="Ваше имя или псевдоним" name="name" value="{{ Auth::user()->name  }}">
                  @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                  <input type="email" placeholder="Ваш e-mail" name="email" value="{{ Auth::user()->email  }}">
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif

                  <select name="country_id">
                  @foreach($countries as $id => $country)
                    @if ($id == Auth::user()->country_id)
                        <option value="{{ $id  }}" selected>{{ $country }}</option>
                    @else
                        <option value="{{ $id  }}">{{ $country }}</option>
                    @endif
                  @endforeach
                  </select>

              </div>

              <!--<img src="{{ Auth::user()->avatar ? Auth::user()->avatar : config('filesystems.default_path.user_avatar') }}" title="{{ Auth::user()->name  }}" />-->

              <button type="submit" class="greenbut">{{ trans('main.save') }}</button>
          </form>

    </div>
</div>
@endsection
