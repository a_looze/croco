<div id="contentHeader"
    style="background: url({{ !empty(Auth::user()->banner)
        ? Auth::user()->banner : config('filesystems.default_path.banner') }});">
    <div id="contentHeaderMask"></div>
    @if (Auth::user())
    <a class="headerEdit mediauser" data-name="banner"> <span></span> </a>
    <form id="up2" class="dropzone" style="height: 100px; width: 500px; display:none;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    </form>
    @endif

    <div id="contentHeaderAction">
        <div class="avatar">
            <img class="userAvatar" src="{{ Auth::user()->avatar ? Auth::user()->avatar : config('filesystems.default_path.user_avatar')  }}">
            <span class="ttp" data-title="{{ $langCountry[Auth::user()->country_id] }}">
                <img class="country" src="{{ Img::resize('images/flag/' . Auth::user()->country_id . '.jpg', 16, 11) }}">
            </span>
            
            @if (Auth::user())
                <a class="avatarEdit mediauser" data-name="avatar">
                    <span ></span>
                </a>
            @endif
        </div>
        <h2>{{ Auth::user()->name }}</h2>
        <div id="score">{{ $subsCount }}<p>{{ trans_choice('main.subscribers', $subsCount) }}</p></div>
        <a class="redbut" href="javascript:void(0)" id="submit-banner" style="display: none;">{{ trans('main.save') }}</a>
    </div>
</div>