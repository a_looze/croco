@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">

    <div class="head">
        <h3>{{ trans('main.edit_file_library')}}</h3>
    </div>



    <div class="browse">
        <div class="contWrap">

             <!-- Success msg -->
             @if(Session::has('message'))
             <div id="success-answer">
                <p>
                   {{ trans(Session::get('message')) }}
                </p>
             </div>
             @endif

            <div class="editFile">
                <h4>{{ trans('main.edit_file_library') }}</h4>
                <div class="editImg">
                    <a href="javascript:void(0)" class="input-button" data-target="preview-from-input">
                        <img id="preview-image-file" src="{{ $file->previewPath }}">
                        <span>{{ trans('main.change_preview') }}</span>
                    </a>
                </div>

                <form style="display:none;"
                    action="{{ action('FileController@update', ['id' => $file->id]) }}"
                    method="post"
                    id="lib"
                    name="preview"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put"/>
                    <input type="hidden" name="title" value="{{ $file->title }}" id="title" />
                    <input id="preview-from-input"
                           data-draw="preview-image-file"
                           data-error="error-file"
                           class="preview-from-input redbut" type="file" name="file" value="{{ $file->previewPath }}"/>
                    <textarea id="description" style="display: none;" name="description">{{ $file->description }}</textarea>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

                <h4>{{ trans('main.file_name') }}</h4>
                <div class="editImg"><input class="has-target" type="text" placeholder="{{ trans('main.file_name') }}" data-target="title" value="{{ $file->title }}"></div>

                <h4>{{ trans('main.file_description') }}</h4>
                <div class="editImg"><textarea class="has-target" data-target="description" placeholder="{{ trans('main.file_description') }}">{{ $file->description }}</textarea></div>

                <!-- ERROR -->
                <p id="error-file" class="error-msg-block"></p>

                <!-- SUBMIT FORM -->
                <a href="javascript:void(0);" class="greenbut margin-20" onclick="document.getElementById('lib').submit()">{{ trans('main.save')}}</a>


            </div>
        </div>
    </div>

    <div class="clear"></div>

</div>
@endsection
