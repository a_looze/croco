@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">

    <div class="head">
        <a href="{{ action('FileController@index') }}" class="yellbut">{{ trans('main.library') }}</a>
        <h3>{{ trans('main.add_file_to_library') }}</h3>
    </div>

    <!-- ADD FILE -->
    <div class="contWrap lib-block">

        <h4>{{ trans('main.add_file_to_library')}}</h4>

        <form id="lib" action="{{ action('FileController@store') }}" class="dropzone"  enctype="multipart/form-data">

            <div class="add-lib">
                <span>{{ trans('main.drop_file_here')}}</span>
                <a id="add-file-to-lib" href="javascript:void(0);" class=" greenbut">{{ trans('main.add_file_to_library')}}</a>
            </div>

            <input type="hidden" name="_method" value="post"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="preview_from_lib" id="preview-from-lib" value=""/>

        </form>

        <!-- ERROR -->
        <p id="error-file" class="error-msg-block"></p>

        <!-- SUBMIT FORM -->
        <div class="margin-20"><a id="submit-lib" href="javascript:void(0);" class="greenbut ">{{ trans('main.add_file_to_library')}}</a></div>

        <a style="display: none" class="redirect" id="redirect-to-file" href=""></a>

    </div>

    <div class="clear"></div>

</div>
@endsection
