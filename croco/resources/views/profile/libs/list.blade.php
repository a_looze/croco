@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
    <div class="head">
        <h3>{{ trans('main.library') }}</h3>

        <div id="sort">
          <!--<a href="javascript:void(0);" class="sortIco">{{ trans('main.sortby') }}</a>-->
          <div id="sortParam">
              <a href="javascript:void(0);" data-sort="created_at" class="file-sort">{{ trans('main.by_date') }}</a>
              <a href="javascript:void(0);" data-sort="type_id" class="file-sort">{{ trans('m ain.by_type') }}</a>
          </div>
        </div>
    </div>

    <div class="addbtn">
        <a href="{{ action('FileController@create') }}" class="yellbut">{{ trans('main.add_file_to_library') }}</a>
    </div>

    <div id="files-outer" class="browse rsmallultra filelist">    
        @foreach($files as $file)
        <div class="item">
            <div class="img">
                <a href="{{ action('FileController@edit', ['id' => $file['id']]) }}">
                    <img class="imgitem" src="{{ $file['preview'] }}">
                </a>
                <div class="stats">
                    <div class="icons">
                        <img class="ico" src="images/icon/{{ $file['type_id'] }}.png">
                    </div>
                </div>
            </div>
            <div class="userinfo">
                <div class="user">
                    <div>
                        <p class="pubDate">{{ $file['title'] }}</p>
                        <p class="pubDate">{{ $file['created_at'] }}</p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

    </div>
    <div class="clear"></div>

</div>
@endsection
