@if (request()->is("profile", "profile/*", "v/*"))
<!-- all files -->
<div class="modal fade docs-cropped lib-modal" role="dialog" tabindex="-1" data-link="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="getCroppedCanvasTitle">{{ trans('main.library') }}</h4>
      </div>
      <div class="modal-body">

        <div class="lib-container browse filelist rsmallultra">
            @foreach($files as $file)
            <div class="item">
                <div class="img">
                    <!-- file item -->
                    <a href="javascript:void(0)" class="lib-item"
                        data-id="{{ $file['id'] }}"
                        data-type="{{ $file['type_id'] }}">
                            <img class="imgitem" src="{{ $file['preview'] }}">
                    </a>
                    <!-- /file item -->
                    <div class="stats">
                        <div class="icons">
                            <img class="ico" src="images/icon/{{ $file['type_id'] }}.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <div>
                            <p class="pubDate">{{ $file['title'] }}</p>
                            <p class="pubDate">{{ $file['created_at'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

      </div>
    </div>
  </div>
</div>

<!-- images -->
<div class="modal fade docs-cropped lib-preview-modal" role="dialog" tabindex="-1" data-link="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="getCroppedCanvasTitle">{{ trans('main.library') }}</h4>
      </div>
      <div class="modal-body">

        <div class="lib-container browse filelist rsmallultra">
            @foreach($files as $file)
            @if($file['type_id'] == 3)
            <div class="item">
                <div class="img">
                    <!-- file item -->
                    <a href="javascript:void(0)" class="lib-item"
                        data-id="{{ $file['id'] }}"
                        data-type="{{ $file['type_id'] }}">
                            <img class="imgitem" src="{{ $file['preview'] }}">
                    </a>
                    <!-- /file item -->
                    <div class="stats">
                        <div class="icons">
                            <img class="ico" src="images/icon/{{ $file['type_id'] }}.png">
                        </div>
                    </div>

                </div>
                <div class="userinfo">
                    <div class="user">
                        <div>
                            <p class="pubDate">{{ $file['title'] }}</p>
                            <p class="pubDate">{{ $file['created_at'] }}</p>
                        </div>
                    </div>
                </div>

            </div>
            @endif
            @endforeach
        </div>

      </div>
    </div>
  </div>
</div>
@else
<!-- no module -->
@endif