@extends('layouts.app')

@section('content')

@include('profile.banner')

<div class="row">
   <div class="wellcome">

      @if(Session::has('message'))
          <p>{{ trans(Session::get('message')) }}</p>
      @endif

      <form class="loginForm regForm" role="form" method="POST" action="{{ url('/profile/changepass') }}">
          {{ csrf_field() }}
          <div>

              <input type="password" name="old_password" class="{{ $errors->has('old_password') ? ' has-error' : '' }}" placeholder="Введите текущий пароль">
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif

              <input type="password" name="password" class="{{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Придумайте пароль">
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
              <input type="password" name="password_confirmation" class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" placeholder="Повторте пароль">
               @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
          </div>
          <button type="submit" class="greenbut">{{ trans('main.save') }}</button>
      </form>

   </div>
</div>
@endsection