{{-- автору темы показываем тему с уже свернутым вопросом --}}
@if ($file->path)
    <img src="{{ Img::resize($file->path, 850, 260) }}">
@else
    <img src="images/demo/cow.jpg">
@endif
