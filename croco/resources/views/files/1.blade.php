{{-- Video file --}}
<div class="questBox">


    <video id="test_video" class="video-js vjs-default-skin" controls autoplay="autoplay"
    @if ($file->previewPath)
        poster="{{ Img::resize($file->previewPath, 700, 240) }}"
    @else
        poster="images/demo/cow.jpg"
    @endif
        data-setup='{ "controls": true, "autoplay": true}' width="850px" height="260px">
            <source src="{{ $file->path }}" type='video/mp4'>
    </video>
</div>

<link href="http://vjs.zencdn.net/5.11.6/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.11.6/video.js"></script>

<script>
    var homePlayer=_V_("test_video");
</script>