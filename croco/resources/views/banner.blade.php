<div id="contentHeader" style="background: url({{ $cat->bgimg }});">
    <div id="contentHeaderMask"></div>
    <div id="contentHeaderAction">
    <div class="avatar">
        <img src="images/{{ $cat->letter }}Ava.jpg">
    </div>
    <h2>{{ trans($cat->langkey) }}</h2>
    @if ($showSubs)
    <a href="javascript:void(0);" data-subs="category" class="trpbut">
        @if ($userSubs)
        {{ trans('main.unsubscribe') }} -
        @else
        {{ trans('main.subscribe') }} +
        @endif
    </a>
    <div id="score">{{ $subsCount }}<p>{{ trans_choice('main.subscribers', $subsCount) }}</p></div>
    @endif
    </div>
</div>