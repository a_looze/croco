<ul>
@foreach( $list as $logObj)
<li>{!! trans($logObj->comment) !!} {{ $logObj->created_at->format('d.m.Y H:i') }}</li>
@endforeach
</ul>
{{ $list->render() }}

<script>
$('.body a').each(function(i,e) {
    var hr = $(e).attr('href');
    // console.log(hr);
    hr = hr.replace('new.', '');
    // console.log(hr);
    // console.log('--------');
    $(e).attr('href', hr);
});
</script>