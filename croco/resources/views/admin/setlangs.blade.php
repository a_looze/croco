<table class="table-primary table table-striped">
    <thead>
        <tr>
            <th class="row-header">Строки\Языки</th>
            @foreach ($langs as $lang)
            <th class="row-header">{{ $lang->lkey }} ({{ $lang->cname }})</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($keys as $i => $key)
        <tr>
        <form id="f{{ $i }}">
            <td class="row-text">{{ $key }}</td>
            @foreach ($langs as $lang)
            @if (isset($lSet[$lang->lkey][$key]))
            <td class="row-text">
                <a href="javascript:;" class="openform" data-i="{{ $i }}" data-lkey="{{ $lang->lkey }}">{{ $lSet[$lang->lkey][$key] }}</a>
            </td>
            @else
            <td class="row-text">
                <a href="javascript:;" class="openform" data-i="{{ $i }}" data-lkey="{{ $lang->lkey }}">!{{ $key }}!</a>
            </td>
            @endif
            @endforeach        
        </tr>
        <tr id="tr{{ $i }}" style="display: none;">
            <td class="row-text" colspan="{{ $count+1 }}">
                <input type="hidden" id="str{{ $i }}" name="str" value="{{ $key }}" />
                <input type="hidden" id="lkey{{ $i }}" name="lkey" value="" />
                <p>Изменение строки <b><span id="sstr{{ $i }}">{{ $key }}</span></b> для языка <span id="sval{{ $i }}"></span></p>
                <p><textarea name="val" id="val{{ $i }}" cols="150" rows="3"></textarea></p>
                <p><button class="btn btn-primary saveform" data-i="{{ $i }}">Сохранить</button></p>
            </td>
        </form>
        </tr>
        @endforeach
    </tbody>
</table>

<!-- <script src="/js/jquery.min.js"></script> -->
<script>
$(function() {
    $('a.openform').on('click', function() {
        var lk = $(this).data('lkey');
        var text = $(this).text();
        var i = $(this).data('i');
        
        if ($(this).hasClass('activeedit')) {
            $('#tr' + i).hide();

            $('#lkey' + i).val('');
            $('#sval' + i).html('');
            $('#val' + i).val('');            
        } else {
            $('#lkey' + i).val(lk);
            $('#sval' + i).html(lk);
            $('#val' + i).val(text);

            $('#tr' + i).show();
        }

        $('a.activeedit').toggleClass('activeedit');
        $(this).toggleClass('activeedit');
        
        // $('a.openform .activeedit').trigger('click'); //todo
    });

    $('.saveform').on('click', function() {
        var i = $(this).data('i');
        var fd = $('#f' + i).serialize();
        // alert(fd);
        $.getJSON('savelangstr', fd, function(d) {
            if (typeof undefined == typeof d) {
                alert ('Ошибка сети');
            } else {
                if (d.status == 'OK') {
                    // $('a.activeedit').text($('#val' + i).val());
                    $('a.activeedit').text(d.mess);
                    $('a.activeedit').trigger('click');
                } else {
                    alert(d.mess);
                }
            }
        });
        return false;
    });
});
</script>