<ul>
@foreach( $list as $user)
<li>{{ $user->name }} {{ $user->blocked_at->format('d.m.Y H:i') }}</li>
@endforeach
</ul>
{{ $list->render() }}