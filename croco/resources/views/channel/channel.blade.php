@extends('layouts.app')

@section('content')

<div id="contentHeader" style="background: url({{ $channelOwner->banner }});">
    <div id="contentHeaderMask"></div>

    <div id="contentHeaderAction">
        <div class="avatar">
            <img src="{{ $channelOwner->avatar ?: config('filesystems.default_path.user_avatar') }}">
            <span class="ttp" data-title="{{ $langCountry[$channelOwner->country_id] }}">
                <img class="country"  src="images/flag/{{ $channelOwner->country_id }}.jpg">
            </span>
        </div>
        <h2>{{ $channelOwner->name }}</h2>
        @if ($showSubs)
        <a href="javascript:void(0);" data-subs="channel" class="trpbut">
            @if ($userSubs)
            {{ trans('main.unsubscribe') }} -
            @else
            {{ trans('main.subscribe') }} +
            @endif
        </a>
        <div id="score">{{ $subsCount }}<p>{{ trans_choice('main.subscribers', $subsCount) }}</p></div>
        @endif        
    </div>
</div>

<div class="row">
    @if( $subsCount > 0 )
    <div class="head">
        <h3>{{ trans('main.subscribes') }}</h3>
        <a href="javascript:;" class="more">{{ trans('main.show_down') }}</a>
    </div>

    <div class="subscribe">
        @foreach ($subscribers as $subs)
        <div class="subItem">
            <a href="{{ action('UserPagesController@index', ['alias' => $subs->owner->alias]) }}">
                <img src="{{ $subs->owner->avatar ? : config('filesystems.default_path.user_avatar') }}">
                {{ $subs->owner->name }}
            </a>
        </div>
        @endforeach
    </div>
    @endif
</div>

<div class="row chanel">
    <div class="head">
        <h3>{{ trans('main.categoryh1') }}</h3>

        @if (count($articles))
        <div id="sort">
            <a href="javascript:;" class="sortIco"></a>
            <div id="sortParam">
                <p>{{ trans('main.sortby') }}</p>
                <a href="javascript:;" data-sort="d" class="sP active">{{ trans('main.by_date') }}</a>
                <a href="javascript:;" data-sort="p" class="sP">{{ trans('main.by_popularity') }}</a>
                <a href="javascript:;" data-sort="t" class="sP">{{ trans('main.by_type') }}</a>
                <a href="javascript:;" data-sort="a" class="sP">{{ trans('main.by_question') }}</a>
            </div>
        </div>
        @endif
    </div>

    <div class="browse rsmall" id="autoposition">
        @foreach ($articles as $article)
            <div class="item">
                <div class="img">
                    <a href="{{ action('ShowArticlesController@index', ['alias' => $article->alias]) }}">
                        @if ($article->preview)
                        <img class="imgitem" src="{{ $article->preview->resize(300, 180)}}">
                        @elseif ($article->attach->type_id == 3)
                        <img class="imgitem" src="{{ $article->attach->resize(300, 180)}}">
                        @else
                        <img class="avatar" src="{{ config('filesystems.default_path.article_preview') }}">
                        @endif
                    </a>
                    <div class="stats">
                        <div class="soc">
                            <span class="like" data-target="false" data-iid="{{ $article->id}}">{{ $article->rating }}</span>
                            <span class="answer">{{ $article->answers->count() }}</span>
                        </div>
                        <div class="icons">
                            <span class="ttp" data-title="{{ $langCountry[$channelOwner->country_id] }}">
                                <img class="country" src="images/flag/{{ $channelOwner->country_id}}.jpg">
                            </span>                            
                            <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">                    
                        <a href="javascript:;">
                            <img class="avatar" src="{{ $channelOwner->avatar ? : config('filesystems.default_path.user_avatar') }}">
                        </a>
                        <div>
                            <a class="userName" href="javascript:;">
                                {{ $channelOwner->name }}
                            </a>
                            <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                        </div>
                    </div>
                    <div class="views">
                        <p>{{ trans('main.views') }}</p>
                        <p>{{ $article->views }}</p>
                    </div>
                </div>
            </div>
        @endforeach

        @if ($hasMorePages)
        <div class="item empty" id="div4button">
            <a href="javascript:void(0);" class="greybut center pushable" id="morebutton">{{ trans('main.more') }}</a>
        </div>
        @elseif (count($articles) < 1)
        <div class="item empty">{{ trans('main.emptycat') }}</div>
        @endif
    </div>

    <div class="clear"></div>
</div>
@endsection