@extends('layouts.app')

@section('content')
        <div id="contentHeader" style="background: url(images/demo/bgHands.jpg);">
          <div id="contentHeaderMask"></div>
          <div id="contentHeaderAction">
              <div class="avatar">

              </div>
              <h2>{{ trans('pages.' . $page->title) }}</h2>
          </div>
       </div>
       
        <div class="row"> 
            <div class="txtLow">
                {!! trans('pages.' . $page->content) !!}
            </div>  
        </div>
@endsection