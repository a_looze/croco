@extends('layouts.app')

@section('content')
<div class="sub2"> 
    <div id="questionWrap">
       
        <div id="qW">
            <div class="head">
                {{-- блок над вопросом --}}
                <h2>
                {{ trans('main.articleh2') }}
                <a href="{{ url($catUrl) }}">
                    <img class="mbi" src="images/{{ $cat->letter }}Ava.jpg">
                </a> 
                </h2>
                
                <div class="artViews">{!! $article->views !!}</div>  
                
                <div class="questIcon">
                @if (Auth::user() && $user->isManager())
                    <div class="godMode">
                        <div>
                            <a href="javascript:;" class="onlyrm" data-target="article" data-iid="{{ $article->id }}">{{ trans('main.banpost') }}</a>
                            <a href="javascript:;" class="rmban" data-target="article" data-iid="{{ $article->id }}">{{ trans('main.banpostuser') }}</a>
                        </div>
                    </div>
                @endif
                
                    <a href="javascript:;" class="expand"></a>
                    <a href="javascript:;" class="hash"></a>
                </div>
                <div class="htags" style="display: none;">
                    <ul>
                    @if ($forUser)
                        <li><a href="{{ url('/u/' . $forUser->alias) }}"> {{ '@' . $forUser->name }}</a></li>
                    @endif
                    @foreach ($tags as $tag)
                        <li><a href="{{ action('SearchController@tagSearch', ['tag' => $tag->tag->title]) }}">#{{ $tag->tag->title }}</a></li>
                    @endforeach
                    </ul>
                    <div class="mobSoc">
                        <p>{{ trans('main.views') }}: {!! $article->views !!}</p>
                        <p>{{ $article->created_at->format('d.m.Y') }}</p>
                    </div>
                </div>
                
            </div>
            <div class="question">
                {{-- Блок с вопросом --}}
                @if ($article->attach->type_id == 1)
                @include('articles.1')
                @elseif ($article->attach->type_id == 2)
                @include('articles.2')
                @else
                @include('articles.3')
                @endif

                <div class="infoBox">
                    {{-- Блок ниже вопроса --}}
                        <a href="{{ action('UserPagesController@index', ['alias' => $owner->alias]) }}" title="{{ $owner->name }}">
                            @if ($owner->avatar)
                                <img class="avatar" src="{{ $owner->avatar }}"> 
                            @else
                                <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                            @endif                        
                        </a>     
                    <div class="user">
                        <div>
                            <span class="ttp" data-title="{{ $langCountry[$owner->country_id] }}">
                                <img class="country"  src="images/flag/{{ $owner->country_id }}.jpg"> 
                            </span> 
                            <a href="{{ action('UserPagesController@index', ['alias' => $owner->alias]) }}" title="{{ $owner->name }}" class="userName">
                                {{ $owner->name }}
                            </a>
                            <div class="subscribe">
                                @if (!$iAmOwner)
                                <a href="javascript:void(0);" class="trpbut" data-subs="channel">
                                @if ($userSubs)
                                {{ trans('main.unsubscribe') }} -
                                @else
                                {{ trans('main.subscribe') }} +
                                @endif
                                </a>
                                @endif
                                <span id="score">{{ $subsCount }} {{ trans_choice('main.subscribers', $subsCount) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="soc">
                        <span class="like" data-target="article" data-iid="{{ $article->id }}">{{ $article->rating }}</span>
                        <span class="dislike" data-target="article" data-iid="{{ $article->id }}">{{ $article->rating }}</span>
                        <span class="answer">{{ count($answers) }}</span>
                        <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                        <!-- <p>{{ trans('main.views') }}: {!! $article->views !!}</p>
                        <p>{{ $article->created_at->format('d.m.Y') }}</p> -->
                    </div>
                    @if (Auth::user() && $article->author->id != Auth::user()->id)
                    <div class="claim" data-target="article" data-iid="{{ $article->id }}">
                        <a href="javascript:;">{{ trans('main.claim') }}</a>
                    </div>
                    @endif
                </div>
            </div>
            <!-- <div class="head2">
                {{-- Блок заголовка над ответами --}}
                <h3>{{ trans('main.answers') }} ({{ count($answers) }})</h3>
                @if (Auth::user() && !$iAmOwner)
                <a href="javascript:;" id="btn1" class="yellbut show-by-target" data-action="answer" data-target="add-answer-front">{{ trans('main.addanswer') }}</a>
                @elseif(!$iAmOwner)
                <a href="{{ url('/login') }}" id="btn3" class="yellbut">{{ trans('main.addanswer') }}</a>
                @endif

                @if (!$iAmOwner && $authorAnswer)
                <a href="javascript:;" class="greybut showauthoranswer aarescomm" >{{ trans('main.seeauthoranswer') }}</a>
                @endif
            </div>   -->
            
        </div>
    </div>
    
   <div id="addAnswer">
        @if (Auth::user() && !$iAmOwner)
                <a href="javascript:;" id="btn1" data-action="answer" data-target="add-answer-front">{{ trans('main.addanswer') }}</a>
                @elseif(!$iAmOwner)
                <a href="{{ url('/login') }}" id="btn3" >{{ trans('main.addanswer') }}</a>
                @endif
        
       <div id="timer">
           <h3>Ответ автора появится через:</h3> 
           <div class="time">01:20:13:52</div>
           <p>Торопись, добавь свой ответ и получи в 2 раза больше бонусов!</p>
       </div>
   </div>
   
    
    <div id="questSide">
        <div class="answerWrap">

            <div class="row">
                <div class="browse">
                    @if (!$iAmOwner && $authorAnswer)
                    <a href="javascript:;" class="mobAuthorAnswer aarescomm">{{ trans('main.seeauthoranswer') }}</a>
                
                    <div class="item authorblock aaconfirm" style="display:none;">
                        <h3>{{ trans('main.areyousure') }}</h3>
                        <a class="yellbut aaconfirmbut" data-a="1" href="javascript:;">{{ trans('main.article.yes') }}</a>
                        <a class="redbut aaconfirmbut" data-a="0" href="javascript:;">{{ trans('main.article.no') }}</a>
                    </div>

                    <div class="item authorblock aacontent" style="display:none;">
                        <div class="img">
                            {{-- Притворяемся массивом для унификации названия переменной $comment в шаблонах --}}
                            @foreach ([$authorAnswer] as $comment)
                            

                            {{-- @if ($comment->attach->type_id == 1)
                                @include('articles.c1')
                                @elseif ($comment->attach->type_id == 2)
                                @include('articles.c2')
                                @else
                                @include('articles.c3')
                                @endif --}}
                            <!-- <a href="javascript:;">
                                <img class="imgitem" src="{{ $comment->attach->resize(350, 282) }}">
                            </a> -->
                            @if ($comment->preview)
                            <a href="javascript:;">
                                <img class="imgitem imgitemansw" data-iid="{{ $comment->id }}" src="{{ $comment->preview->resize(645,430) }}">
                            </a>
                            @else
                            <a href="javascript:;">
                                <img class="imgitem imgitemansw" data-iid="{{ $comment->id }}" src="{{ $comment->attach->resize(645,430) }}">
                            </a>                            
                            @endif

                            @endforeach

                            @if ($authorAnswer->attach)
                            <div class="stats">
                                <div class="icons">
                                    <img class="ico" src="images/icon/{{ $authorAnswer->attach->type_id }}.png">
                                </div>
                            </div>
                            @endif
                        </div>
                        <h3>{{ trans('main.authoranswer') }}</h3>
                    </div>
                    @endif

                    @if (Auth::user() && !$iAmOwner)
                        @include('forms.answer')
                    @endif

                    {{-- Список ответов --}}
                    @if (count($answers))

                    @foreach ($answers as $comment)                
                    <div class="item">
                        @if ($user && $user->isManager())
                        <div class="godMode">
                            <div>
                                <a href="javascript:;" class="onlyrm" data-target="answer" data-iid="{{ $comment->id }}">{{ trans('main.bananswer') }}</a>
                                <a href="javascript:;" class="rmban" data-target="answer" data-iid="{{ $comment->id }}">{{ trans('main.banansweruser') }}</a>
                            </div>
                        </div>
                        @endif
                         <div class="head">
                                {{-- блок над ответом --}}
                                <h2>Ответ</h2>

                                <div class="artViews">{!! $article->views !!}</div>  

                                <div class="questIcon">
                                @if (Auth::user() && $user->isManager())
                                    <div class="godMode">
                                        <div>
                                            <a href="javascript:;" class="onlyrm" data-target="article" data-iid="{{ $article->id }}">{{ trans('main.banpost') }}</a>
                                            <a href="javascript:;" class="rmban" data-target="article" data-iid="{{ $article->id }}">{{ trans('main.banpostuser') }}</a>
                                        </div>
                                    </div>
                                @endif

                                    <a href="javascript:;" class="expand"></a>
                                    <a href="javascript:;" class="hash"></a>
                                </div>
                            </div>
                        
                        <div class="img">
                            <div class="authorSay" id="as{{ $comment->id }}">
                                @if ($comment->author_say == 1)
                                <span class="ttp" data-title="{{ trans('main.correctansw') }}">
                                    <img src="images/icon/win.png"> 
                                </span>
                                @elseif ($comment->author_say == 2)
                                <span class="ttp" data-title="{{ trans('main.wrongansw') }}">
                                    <img src="images/icon/los.png">
                                </span> 
                                @else
                                {{-- trans('main.qansw') --}}
                                @endif
                            </div>
                            {{-- @if ($comment->attach->type_id == 1)
                                @include('articles.c1')
                                @elseif ($comment->attach->type_id == 2)
                                @include('articles.c2')
                                @else
                                @include('articles.c3')
                                @endif --}}
                            @if ($comment->preview)
                            <a href="javascript:;">
                                <img class="imgitem imgitemansw" data-iid="{{ $comment->id }}" src="{{ $comment->preview->resize(645,430) }}">
                            </a>
                            @else
                            <a href="javascript:;">
                                <img class="imgitem imgitemansw" data-iid="{{ $comment->id }}" src="{{ $comment->attach->resize(645,430) }}">
                            </a>
                            @endif
 
                        </div>
                        <div class="userinfo">
                            <div class="user">
                                <a href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}" title="{{ $comment->author->name }}">
                                    @if ($comment->author->avatar)
                                        <img class="avatar" src="{{ $comment->author->avatar }}"> 
                                    @else
                                        <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                                    @endif
                                </a>
                                <div>
                                    <span class="ttp" data-title="{{ $langCountry[$comment->author->country_id] }}">
                                        <img class="country"  src="images/flag/{{ $comment->author->country_id }}.jpg">
                                    </span>
                                    <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}">
                                        {{ $comment->author->name }}
                                    </a>
                                    <p class="pubDate">{{ $comment->created_at->format('d.m.Y') }}</p>
                                </div>
                            </div>
                            <div class="soc">
                                <span class="like" data-target="answer" data-iid="{{ $comment->id }}">{{ $comment->rating }}</span>
                                 <span class="dislike" data-target="article" data-iid="{{ $article->id }}">{{ $article->rating }}</span>
                            </div>
                            @if (Auth::user() && $comment->author->id != Auth::user()->id)
                            <div class="claim" data-target="answer" data-iid="{{ $comment->id }}">
                                <a href="javascript:;">{{ trans('main.claim') }}</a>
                            </div>
                            @endif
                        </div>

                        @if ($iAmOwner)
                        {{-- Кнопки реакции автора на ответ --}}
                        <div class="authorAction">
                            <a href="javascript:;" class="greenbut authorsay" data-what="1" data-target="{{ $comment->id }}">{{ trans('main.correctansw') }}</a>
                            <a href="javascript:;" class="redbut authorsay" data-what="0" data-target="{{ $comment->id }}">{{ trans('main.wrongansw') }}</a>
                        </div>
                        @endif

                    </div>
                    @endforeach

                    @endif
                </div>
            </div>
        </div>
        <div class="commentWrap">
            <div class="row">
                <div class="head">
                    {{-- Блок заголовка над комментариями --}}
                    <h3>{{ trans('main.comments') }} ({{ count($comments) }})</h3>

                    @if (Auth::user())
                    <div id="commentBut">
                        <a href="javascript:void(0)" class="addFile show-by-target"
                            data-action="comment"
                            data-id="{{$article->id}}"
                            data-comment="post"
                            data-target="comment"><img src="images/icon/addFile.png"></a>
                        <a href="javascript:void(0)" class="yellbut"
                            id="submit-comment">{{trans('main.addcomment')}}</a>
                    </div>
                    @else
                    <div id="commentBut">
                        <a href="{{ url('/login') }}" class="yellbut">{{trans('main.addcomment')}}</a>
                    </div>
                    @endif

                    @include('forms.comment')

                </div>
                <div class="browse">
                    {{-- Список комментариев --}}
                    @if (count($comments))

                    @foreach ($comments as $comment)                
                    <div class="item">
                        @if ($user && $user->isManager())
                        <div class="godMode">
                            <div>
                                <a href="javascript:;" class="onlyrm" data-target="comment" data-iid="{{ $comment->id }}">{{ trans('main.bancomment') }}</a>
                                <a href="javascript:;" class="rmban" data-target="comment" data-iid="{{ $comment->id }}">{{ trans('main.bancommentuser') }}</a>
                            </div>
                        </div>
                        @endif
                        <div class="comment">{{ $comment->content }}</div>
                        @if ($comment->attach)
                        <div class="img">
                            @if ($comment->attach->type_id == 1)
                            @include('articles.c1')
                            @elseif ($comment->attach->type_id == 2)
                            @include('articles.c2')
                            @else
                            @include('articles.c3')
                            @endif
                            <div class="stats">
                                <div class="soc">
                                    <span class="like" data-target="comment" data-iid="{{ $comment->id}}">{{ $comment->rating }}</span>
                                </div>
                                <div class="icons"> 
                                    <span class="ttp" data-title="{{ $langCountry[$comment->author->country_id] }}">
                                        <img class="country"  src="images/flag/{{ $comment->author->country_id }}.jpg">
                                    </span>
                                    <img class="ico" src="images/icon/{{ $comment->attach->type_id }}.png">
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="userinfo">
                            <div class="user">
                                <a href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}" title="{{ $comment->author->name }}">
                                    @if ($comment->author->avatar)
                                        <img class="avatar" src="{{ $comment->author->avatar }}"> 
                                    @else
                                        <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                                    @endif
                                </a>
                                <div>
                                    <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}">
                                        {{ $comment->author->name }}
                                    </a>
                                    <p class="pubDate">{{ $comment->created_at->format('d.m.Y') }}</p>
                                </div>
                            </div>
                            @if (Auth::user() && $comment->author->id != Auth::user()->id)
                            <div class="claim" data-target="comment" data-iid="{{ $comment->id }}">
                                <a href="javascript:;">{{ trans('main.claim') }}</a>
                            </div>
                            @endif
                        </div>
                    </div>
                    @endforeach

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div id="otherQuest">
    <div class="row">
        <div class="head">
            <div class="fixoq">
                <h3>            
                {{ trans('main.otherarticles')}}
                <a href="{{ url($catUrl) }}">
                    <img class="mbi" src="images/{{ $cat->letter }}Ava.jpg">
                </a>
                </h3>
                <a href="javascript:;" class="vis"></a>
            </div>
        </div>
        <div class="browse{{ $opacity }}" id="oqBws">
            <div class="baron2 baron__root baron__clipper macosx scrollbar">
            <div class="baron__scroller">
            {{-- Прочие статьи из этой категории --}}
            @foreach ($otherArticles as $siblingArticle)
            <div class="item">
                <div class="img">
                    <a href="{{ url('/v/' . $siblingArticle->alias) }}">
                        <img class="imgitem" src="{{ $siblingArticle->preview->resize(300, 180)}}">
                        <div class="mask"></div>
                    </a>
                    <div class="artViews" style="POSITION: absolute; z-index: 9; top: 0px; right: 2px;">{!! $article->views !!}</div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        
                        <a href="{{ action('UserPagesController@index', ['alias' => $siblingArticle->author->alias]) }}" title="{{ $siblingArticle->author->name }}">
                            @if ($siblingArticle->author->avatar)
                                <img class="avatar" src="{{ $siblingArticle->author->avatar }}"> 
                            @else
                                <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                            @endif
                        </a>
                        <div>
                            <span class="ttp" data-title="{{ $langCountry[$siblingArticle->author->country_id] }}">
                                <img class="country" src="images/flag/{{ $siblingArticle->author->country_id }}.jpg">
                            </span>
                            <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $siblingArticle->author->alias]) }}">
                                {{ $siblingArticle->author->name }}
                            </a>
                            <p class="pubDate">{{ $siblingArticle->created_at->format('d.m.Y') }}</p>
                        </div>
                    </div>
                  <!--  <div class="soc">
                            <span class="like" data-target="false" data-iid="{{ $siblingArticle->id}}">{{ $siblingArticle->rating }}</span>
                            <span class="answer">{{ $siblingArticle->answers->count() }}</span>
                        </div> 
                    <div class="views">
                        <p>{{ trans('main.views') }}</p>
                        <p>{{ $siblingArticle->views }}</p>
                    </div> -->
                </div>
            </div>
            @endforeach

            <div class="item">
                <a href="{{ url($catUrl) }}" class="anyclass">{{ trans('main.seemore') }}</a>
            </div>
        </div>
        </div>
        </div>
        </div>

</div>

@if (count($answers))

@foreach ($answers as $comment)                
<div class="item">
    <!-- modal {{ $comment->id }} -->
    <div class="modalBg" id="md{{ $comment->id }}" data-iid="{{ $comment->id }}" style="display:none;">
            <div class="modalContent"> 
                <!-- <a href="javascript:;" data-iid="{{ $comment->id }}" class="closeModal">{{ trans('main.cancel') }}</a> -->
                
                <div class="mC">
                    <a class="srav closeModal" data-iid="{{ $comment->id }}" href="javascript:;"><img src="images/icon/repeat.png"></a>
                    <div class="modalItem modalItemp">
                        <h2>{{ trans('main.question') }}</h2>
                        <div class="qItem">
                            <div class="img">
                                {{-- Блок с вопросом --}}
                                @if ($article->attach->type_id == 1)
                                @include('articles.1')
                                @elseif ($article->attach->type_id == 2)
                                @include('articles.2')
                                @else
                                @include('articles.3')
                                @endif                                                       
                                <div class="stats">
                                    <div class="soc">
                                        <span class="like" data-target="article" data-iid="{{ $article->id }}">{{ $article->rating }}</span>
                                        <span class="answer">{{ count($answers) }}</span>
                                    </div>
                                    
                                    <div class="icons">
                                        <span class="ttp" data-title="{{ $langCountry[$owner->country_id] }}">
                                            <img class="country"  src="images/flag/{{ $owner->country_id }}.jpg">
                                        </span>

                                        <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                                    </div>
                                </div>
                            </div>
                            <div class="userinfo">
                                <div class="user">
                                    <a href="{{ action('UserPagesController@index', ['alias' => $owner->alias]) }}" title="{{ $owner->name }}">
                                        @if ($owner->avatar)
                                            <img class="avatar" src="{{ $owner->avatar }}"> 
                                        @else
                                            <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                                        @endif                        
                                    </a>
                                    <div>
                                        <a href="{{ action('UserPagesController@index', ['alias' => $owner->alias]) }}" title="{{ $owner->name }}" class="userName">
                                            {{ $owner->name }}
                                        </a>
                                        <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modalItem modalItema">
                        <a class="mobsrav closeModal" data-iid="{{ $comment->id }}" href="javascript:;"><img src="images/icon/repeat.png"></a>
                        <h2>{{ trans('main.answer') }}</h2>
                        <div class="aItem">
                            <div class="img">
                                <div class="authorSay" id="as{{ $comment->id }}">
                                    @if ($comment->author_say == 1)
                                    <span class="ttp" data-title="{{ trans('main.correctansw') }}">
                                        <img src="images/icon/win.png"> 
                                    </span>
                                    @elseif ($comment->author_say == 2)
                                    <span class="ttp" data-title="{{ trans('main.wrongansw') }}">
                                        <img src="images/icon/los.png">
                                    </span> 
                                    @else
                                    {{-- trans('main.qansw') --}}
                                    @endif
                                </div>
                                @if ($comment->attach->type_id == 1)
                                @include('articles.c1')
                                @elseif ($comment->attach->type_id == 2)
                                @include('articles.c2')
                                @else
                                @include('articles.c3')
                                @endif
                                <div class="stats">
                                    <div class="soc">
                                        <span class="like" data-target="answer" data-iid="{{ $comment->id }}">{{ $comment->rating }}</span>
                                    </div>
                                    <div class="icons">
                                        <span class="ttp" data-title="{{ $langCountry[$comment->author->country_id] }}">
                                            <img class="country"  src="images/flag/{{ $comment->author->country_id }}.jpg">
                                        </span>

                                        <img class="ico" src="images/icon/{{ $comment->attach->type_id }}.png">
                                    </div>
                                </div>
                            </div>
                             <div class="userinfo">
                                <div class="user">
                                    <a href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}" title="{{ $comment->author->name }}">
                                        @if ($comment->author->avatar)
                                            <img class="avatar" src="{{ $comment->author->avatar }}"> 
                                        @else
                                            <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                                        @endif
                                    </a>
                                    <div>
                                        <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}">
                                            {{ $comment->author->name }}
                                        </a>
                                        <p class="pubDate">{{ $comment->created_at->format('d.m.Y') }}</p>
                                    </div>                                  
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                    <a class="srav closeModal" data-iid="{{ $comment->id }}" href="javascript:;"><img src="images/icon/repeat.png"></a>
                
                </div>
            </div>
    </div>
    <!-- END modal {{ $comment->id }} -->

    @if ($iAmOwner)
    {{-- Кнопки реакции автора на ответ --}}
    <div class="authorAction">
        <a href="javascript:;" class="greenbut authorsay" data-what="1" data-target="{{ $comment->id }}">{{ trans('main.correctansw') }}</a>
        <a href="javascript:;" class="redbut authorsay" data-what="0" data-target="{{ $comment->id }}">{{ trans('main.wrongansw') }}</a>
    </div>
    @endif

</div>
@endforeach

@endif

@if (!$iAmOwner && $authorAnswer)
{{-- Притворяемся массивом для унификации названия переменной $comment в шаблонах --}}
@foreach ([$authorAnswer] as $comment)
<div class="item">
    <!-- modal {{ $comment->id }} -->
    <div class="modalBg" id="md{{ $comment->id }}" data-iid="{{ $comment->id }}" style="display:none;">
            <div class="modalContent"> 
                <!-- <a href="javascript:;" data-iid="{{ $comment->id }}" class="closeModal">{{ trans('main.cancel') }}</a> -->
                
                <div class="mC">
                    <a class="srav closeModal" data-iid="{{ $comment->id }}" href="javascript:;"><img src="images/icon/repeat.png"></a>
                    <div class="modalItem modalItemp">
                        <h2>{{ trans('main.question') }}</h2>
                        <div class="qItem">
                            <div class="img">
                                {{-- Блок с вопросом --}}
                                @if ($article->attach->type_id == 1)
                                @include('articles.1')
                                @elseif ($article->attach->type_id == 2)
                                @include('articles.2')
                                @else
                                @include('articles.3')
                                @endif                                                       
                                <div class="stats">
                                    <div class="soc">
                                        <span class="like" data-target="article" data-iid="{{ $article->id }}">{{ $article->rating }}</span>
                                        <span class="answer">{{ count($answers) }}</span>
                                    </div>
                                    <div class="icons">
                                        <span class="ttp" data-title="{{ $langCountry[$owner->country_id] }}">
                                            <img class="country"  src="images/flag/{{ $owner->country_id }}.jpg">
                                        </span>

                                        <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                                    </div>
                                </div>
                            </div>
                            <div class="userinfo">
                                <div class="user">
                                    <a href="{{ action('UserPagesController@index', ['alias' => $owner->alias]) }}" title="{{ $owner->name }}">
                                        @if ($owner->avatar)
                                            <img class="avatar" src="{{ $owner->avatar }}"> 
                                        @else
                                            <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                                        @endif                        
                                    </a>
                                    <div>
                                        <a href="{{ action('UserPagesController@index', ['alias' => $owner->alias]) }}" title="{{ $owner->name }}" class="userName">
                                            {{ $owner->name }}
                                        </a>
                                        <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modalItem modalItema">
                        <a class="mobsrav closeModal" data-iid="{{ $comment->id }}" href="javascript:;"><img src="images/icon/repeat.png"></a>
                        <h2>{{ trans('main.answer') }}</h2>
                        <div class="aItem">
                            <div class="img">
                                <div class="authorSay" id="as{{ $comment->id }}">
                                    @if ($comment->author_say == 1)
                                    <span class="ttp" data-title="{{ trans('main.correctansw') }}">
                                        <img src="images/icon/win.png"> 
                                    </span>
                                    @elseif ($comment->author_say == 2)
                                    <span class="ttp" data-title="{{ trans('main.wrongansw') }}">
                                        <img src="images/icon/los.png">
                                    </span> 
                                    @else
                                    {{-- trans('main.qansw') --}}
                                    @endif
                                </div>
                                @if ($comment->attach->type_id == 1)
                                @include('articles.c1')
                                @elseif ($comment->attach->type_id == 2)
                                @include('articles.c2')
                                @else
                                @include('articles.c3')
                                @endif
                                <div class="stats">
                                    <div class="soc">
                                        <span class="like" data-target="answer" data-iid="{{ $comment->id }}">{{ $comment->rating }}</span>
                                    </div>
                                    <div class="icons">
                                        <span class="ttp" data-title="{{ $langCountry[$comment->author->country_id] }}">
                                            <img class="country"  src="images/flag/{{ $comment->author->country_id }}.jpg">
                                        </span>

                                        <img class="ico" src="images/icon/{{ $comment->attach->type_id }}.png">
                                    </div>
                                </div>
                            </div>
                             <div class="userinfo">
                                <div class="user">
                                    <a href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}" title="{{ $comment->author->name }}">
                                        @if ($comment->author->avatar)
                                            <img class="avatar" src="{{ $comment->author->avatar }}"> 
                                        @else
                                            <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                                        @endif
                                    </a>
                                    <div>
                                        <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $comment->author->alias]) }}">
                                            {{ $comment->author->name }}
                                        </a>
                                        <p class="pubDate">{{ $comment->created_at->format('d.m.Y') }}</p>
                                    </div>                                  
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                    <a class="srav closeModal" data-iid="{{ $comment->id }}" href="javascript:;"><img src="images/icon/repeat.png"></a>
                
                </div>
            </div>
    </div>
    <!-- END modal {{ $comment->id }} -->
</div>

@endforeach
@endif
@endsection
 