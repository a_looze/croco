@extends('layouts.app')

@section('content')
<div id="contentHeader" style="background: url(images/demo/bgWellcome.jpg);">
  <div id="contentHeaderMask"></div>
  <div id="contentHeaderAction">
      <div class="avatar">
        <img src="images/enterAva.jpg">
      </div>
      <h2>{{ trans('main.welcome') }}</h2>
  </div>
</div>

  <div class="row">
   <div class="wellcome">
      <ul class="login">
          <li class="active"><a href="{{ url('/login') }}">{{ trans('main.enter') }}</a> </li>
          <li><a href="{{ url('/register') }}">{{ trans('main.registration') }}</a></li>
      </ul>

      <form class="loginForm" role="form" method="POST" action="{{ url('/login') }}">
          {{ csrf_field() }}
          <div>
              <input type="text" name="email" value="{{ old('email') }}" placeholder="E-mail" {{ $errors->has('email') ? ' has-error' : '' }}>
              @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              <input type="password" name="password" placeholder="Password">

              <a href="{{ url('/password/reset') }}">{{ trans('main.forgot_password') }}</a>
          </div>
          <button type="submit" class="greenbut">{{ trans('main.enter') }}</button>
      </form>


   </div>
</div>
@endsection