@extends('layouts.app')

@section('content')
<div id="contentHeader" style="background: url(images/demo/bgWellcome.jpg);">
  <div id="contentHeaderMask"></div>
  <div id="contentHeaderAction">
      <div class="avatar">
        <img src="images/regAva.jpg">
      </div>
      <h2>{{ trans('main.welcome') }}</h2>
  </div>
</div>

  <div class="row">
   <div class="wellcome">
      <ul class="login">
          <li><a href="{{ url('/login') }}">{{ trans('main.enter') }}</a> </li>
          <li class="active"><a href="{{ url('/register') }}">{{ trans('main.registration') }}</a></li>
      </ul>

      <form class="loginForm regForm" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
          <div>
              <input type="text" placeholder="{{ trans('main.your_name') }}" name="name" value="{{ old('name') }}">
              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
              <input type="email" placeholder="{{ trans('main.your_email') }}" name="email" value="{{ old('email') }}">
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
              <select name="country_id">
                 @foreach($countries as $id => $country)
                        <option value="{{ $id }}">{{ $country }}</option>
                  @endforeach
              </select>
              <input type="password" name="password" class="{{ $errors->has('password') ? ' has-error' : '' }}" placeholder="{{ trans('main.create_password') }}">
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
              <input type="password" name="password_confirmation" class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" placeholder="{{ trans('main.repeat_password') }}">
               @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
          </div>
          <button type="submit" class="greenbut">{{ trans('main.registration') }}</button>
      </form>

   </div>
</div>
@endsection