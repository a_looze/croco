@extends('layouts.app')

@section('content')
<div id="contentHeader" style="background: url(images/demo/bgWellcome.jpg);">
  <div id="contentHeaderMask"></div>
  <div id="contentHeaderAction">
      <div class="avatar">

      </div>
      <h2>Восстановление пароля</h2>
  </div>
</div>

  <div class="row">
   <div class="wellcome">
      <ul class="login">
          <li class="active"><a href="{{ url('/login') }}">Вход</a> </li>
          <li><a href="{{ url('/register') }}">Регистрация</a></li>
      </ul>

      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif

      <form class="loginForm" role="form" method="POST" action="{{ url('/password/email') }}">
          {{ csrf_field() }}

          <div class="">
                  <input id="email" type="email" class="{{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}">

                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
          </div>
          <button type="submit" class="greenbut">Отправить ссылку на сброс пароля</button>
      </form>


   </div>
</div>
@endsection