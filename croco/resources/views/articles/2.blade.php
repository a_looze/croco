{{-- Audio file --}}
            {{-- автору темы показываем тему с уже свернутым вопросом --}}
            @if ($iAmOwner)
            <div class="questBox" nostyle="display:none;">
            @else
            <div class="questBox">
            @endif
            
            <video id="video" class="video-js vjs-fluid placeholder" controls 
                @if ($article->preview)
                    poster="{{ $article->preview->resize(960, 640) }}"
                @else
                    poster="{{ config('filesystems.default_path.audio') }}" 
                @endif
                    data-setup='{ "controls": true, "autoplay": false}' width="960" height="640">

                        <source src="{{ 'assets/media/users/' . $owner->id . '/2/' . $file->alias . '.mp3' }}" type='audio/mpeg'>
                        <source src="{{ 'assets/media/users/' . $owner->id . '/2/' . $file->alias . '.oga' }}" type='audio/ogg'>
                </video>           
            </div>