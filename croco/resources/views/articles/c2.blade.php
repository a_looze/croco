{{-- Audio file --}}
                        @if ($comment->attach)
                        <a href="javascript:;">

                            <video class="video-js vjs-fluid placeholder" controls 
                            @if ($comment->preview)
                                poster="{{ $comment->preview->resize(960, 640) }}"
                            @else
                                poster="{{ config('filesystems.default_path.audio') }}" 
                            @endif
                                data-setup='{ "controls": true, "autoplay": false}' width="960" height="640">

                                    <source src="{{ 'assets/media/users/' . $comment->author->id . '/2/' . $comment->attach->alias . '.mp3' }}" type='audio/mpeg'>
                                    <source src="{{ 'assets/media/users/' . $comment->author->id . '/2/' . $comment->attach->alias . '.oga' }}" type='audio/ogg'>
                            </video> 
                        </a>
                        @endif