{{-- автору темы показываем тему с уже свернутым вопросом --}}
            @if ($iAmOwner)
            <div class="questBox" nostyle="display:none;">
            @else
            <div class="questBox">
            @endif
                @if ($article->attach)
                    <img class="imgitem" src="{{ $article->attach->resize(960, 640) }}">
                @else
                    <img class="imgitem" src="{{ config('filesystems.default_path.article_preview') }}"> 
                @endif                   
            </div>