{{-- Video file --}}
            {{-- автору темы показываем тему с уже свернутым вопросом --}}
            @if ($iAmOwner)
            <div class="questBox" nostyle="display:none;">
            @else
            <div class="questBox">
            @endif

                <video id="video" class="video-js vjs-fluid placeholder" controls 
                @if ($article->preview)
                    poster="{{ $article->preview->resize(850, 540) }}"
                @else
                    poster="{{ config('filesystems.default_path.video') }}" 
                @endif
                    data-setup='{ "controls": true, "autoplay": false}' width="850px" height="540px">

                        <source src="{{ 'assets/media/users/' . $owner->id . '/1/' . $file->alias . '.mp4' }}" type='video/mp4'>
                        <source src="{{ 'assets/media/users/' . $owner->id . '/1/' . $file->alias . '.webm' }}" type='video/webm'>
                        <source src="{{ 'assets/media/users/' . $owner->id . '/1/' . $file->alias . '.ogv' }}" type='video/ogg'>
                </video>           
            </div>