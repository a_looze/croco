{{-- Video file --}}
                        @if ($comment->attach)
                        <a href="javascript:;">
                        
                            <video class="video-js vjs-fluid placeholder" controls 
                            @if ($comment->preview)
                                poster="{{ $comment->preview->resize(645, 430) }}"
                            @else
                                poster="{{ config('filesystems.default_path.video') }}" 
                            @endif
                                data-setup='{ "controls": true, "autoplay": false}' width="645" height="430">
                                    <source src="{{ 'assets/media/users/' . $comment->author->id . '/1/' . $comment->attach->alias . '.mp4' }}" type='video/mp4'>
                                    <source src="{{ 'assets/media/users/' . $comment->author->id . '/1/' . $comment->attach->alias . '.webm' }}" type='video/webm'>
                                    <source src="{{ 'assets/media/users/' . $comment->author->id . '/1/' . $comment->attach->alias . '.ogv' }}" type='video/ogg'>
                            </video> 
                        </a>
                        @endif