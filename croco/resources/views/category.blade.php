@extends('layouts.app')

@section('content')

@include('banner')

<div class="row">
    <div class="head autofix">
        <div class="fixHead">
            <h3>
            {{ trans('main.categoryh1') }}
            <img class="mbi" src="images/{{ $cat->letter }}Ava.jpg">
            </h3>
            @if (count($articles))
            <div id="sort">
                <a href="javascript:;" class="sortIco"></a>
                <div id="sortParam">
                    <p>{{ trans('main.sortby') }}</p>
                    <a href="javascript:;" data-sort="d" class="sP active">{{ trans('main.by_date') }}</a>
                    <a href="javascript:;" data-sort="p" class="sP">{{ trans('main.by_popularity') }}</a>
                    <a href="javascript:;" data-sort="t" class="sP">{{ trans('main.by_type') }}</a>
                    <a href="javascript:;" data-sort="a" class="sP">{{ trans('main.by_question') }}</a>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="browse rsmall" id="autoposition">
        @foreach ($articles as $article)
            <div class="item">
                <div class="img">
                    <a href="{{ url('/v/' . $article->alias) }}">
                        <!-- <img class="imgitem" src="{{-- $article->preview ? $article->preview->resize(300, 180) : ''--}}"> -->
                        @if ($article->preview)
                        <img class="imgitem" src="{{ $article->preview->resize(300, 200)}}">
                        @elseif ($article->attach->type_id == 3)
                        <img class="imgitem" src="{{ $article->attach->resize(300, 200)}}">
                        @else
                        <img class="avatar" src="{{ config('filesystems.default_path.article_preview') }}">
                        @endif
                    <div class="mask"></div>
                    </a>
                    
                    <div class="artViews" style="POSITION: absolute; z-index: 9; top: 0px; right: 2px;">{!! $article->views !!}</div>
                    <div class="stats" style="display: none;">
                        <div class="soc">
                            <span class="like" data-target="false" data-iid="{{ $article->id}}">{{ $article->rating }}</span>
                            <span class="answer">{{ $article->answers()->where('author_say', '<', '3')->count() }}</span>
                        </div>
                        <div class="icons">
                            
                            <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                        </div>
                    </div>
                </div>
                                
                <div class="userinfo">
                    <div class="user">                    
                        <a href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}" title="{{ $article->author->name }}">
                            @if ($article->author->avatar)
                                <img class="avatar" src="{{ $article->author->avatar }}"> 
                            @else
                                <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                            @endif
                        </a>
                        <div>
                                <span class="ttp" data-title="{{ $langCountry[$article->author->country_id] }}">
                                <img class="country" src="images/flag/{{ $article->author->country_id}}.jpg">
                            </span>
                            <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}">
                                {{ $article->author->name }} 
                            </a>
                            <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                        </div>
                    </div>
                     
                </div>
            </div>
        @endforeach

        @if ($hasMorePages)
        <div class="item empty" id="div4button">
            <a href="javascript:void(0);" class="greybut center pushable" id="morebutton">{{ trans('main.more') }}</a>
        </div>
        @elseif (count($articles) < 1)
        <div class="item empty">{{ trans('main.emptycat') }}</div>
        @endif
    </div>

    <div class="clear"></div>

</div>
@include('footer')
@endsection