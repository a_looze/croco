@extends('layouts.app')

@section('content')
        <div class="row"> 
            <div class="head"> 
            <h3>{{ trans('pages.' . $page->title) }}</h3> 
            </div>
            <div class="txtLow">
                {!! trans('pages.' . $page->content) !!}
            </div>  
        </div>
@endsection