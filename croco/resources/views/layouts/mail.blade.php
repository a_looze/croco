<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
    
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->
    
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

<style type="text/css">
  .header {
    background: #8a8a8a;
  }
  .header .columns {
    padding-bottom: 0;
  }
  .header p {
    color: #fff;
    padding-top: 15px;
  }
  .header .wrapper-inner {
    padding: 20px;
  }
  .header .container {
    background: transparent;
  }
  table.button.facebook table td {
    background: #3B5998 !important;
    border-color: #3B5998;
  }
  table.button.twitter table td {
    background: #1daced !important;
    border-color: #1daced;
  }
  table.button.google table td {
    background: #DB4A39 !important;
    border-color: #DB4A39;
  }
  .wrapper.secondary {
    background: #f3f3f3;
  }
</style>

</head>
<body>

<wrapper class="header">
  <container>
    <row class="collapse">
      <columns small="6">
        <img src="{{ $message->embed(public_path('images/logo.png')) }}">
      </columns>
      <columns small="6">
        <!-- <p class="text-right">BASIC</p> -->
      </columns>
    </row>
  </container>
</wrapper>

<container>

  <spacer size="16"></spacer>

  <row>
    <columns small="12">
      <h1>{{ $h1 }}</h1>

      <p class="lead">{!! $content !!}</p>

      <callout class="primary">
        <p>{{ $bonusTxt }}</p>
      </callout>
    </columns>
  </row>
  <wrapper class="secondary">

    <spacer size="16"></spacer>

    <row>
    <columns large="6">
      <!-- <h5>Connect With Us:</h5>
      <button class="facebook expand" href="http://zurb.com">Facebook</button>
      <button class="twitter expand" href="http://zurb.com">Twitter</button>
      <button class="google expand" href="http://zurb.com">Google+</button> -->
    </columns>
    <columns large="6">
      <h5>Contact Info:</h5>
      <p>Email: <a href="mailto:master@commfi.net">master@commfi.net</a></p>
      <div id="copyright">Protected by the Copyright <br>(Copyright number: 284670239, 284693752)</div>
    </columns>
    </row>
  </wrapper>
</container>
</body>
</html>