<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <base href="{{ url('/') }}" />

    <title>{{ config('app.name') }} :: {{ config('settings.site_name') }}</title>

    <link rel="icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />


    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link rel="stylesheet" href="css/animate.css" type='text/css'>
    <link rel="stylesheet" href="owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="css/dropzone.css">
    <link rel="stylesheet" href="css/styles.css">

    <link  href="css/cropper.min.css" rel="stylesheet">
    <link href="css/320up.css" rel="stylesheet" type="text/css"> 
    <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="css/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/baron.css">
</head>
<body>
<div id="loader-wrapper">
    <div id="preloader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div id="header">
   <div id="hd">
    <a href="javascript:void(0);" id="menu-ico"></a>
    <div id="logo">
        <a href="/"><img alt="Commfi" title="Commfi" src="images/logo.png"></a>
        <h1>{{ config('settings.site_name') }}</h1>
    </div>
    
    
    
    <div id="rsection">
        <a href="javascript:;" id="searchIcon"><img src="images/icon/2018/search1.png"></a>
        <div id="logout">
        @if (Auth::user())
            <a href="{{ url('/profile/notifies') }}">
                <div id="notice">
                @if ($alertCount > 0)
                    <div id="noticeBall" class="test4">{{ $alertCount }}</div>
                @endif
                </div>
            </a>
            <a href="{{ url('/profile/bonuses') }}">
                <div id="wallet">
                @if ($bonusCount > 0)
                    <div id="walletBall">C</div>
                @endif
                </div>
            </a>
        @endif
        
        @if (Auth::guest())
        <div id="login">
            <a href="{{ url('/login') }}" class="greybut">{{ trans('main.enter') }}</a>
            <a href="{{ url('/register') }}" class="redbut">{{ trans('main.registration') }}</a>
        </div>
        @else
        <!-- <div id="add"><a href="{{ action('ArticlesController@create') }}" class="redbut">{{ trans('main.add_question') }}</a></div> -->
        <div id="add"><a href="{{ action('ArticlesController@create') }}" class="redbut">+</a></div> 
           <!-- <div id="avatar">
                <a href="{{ url('/profile') }}">
                    <img src="{{ Auth::user()->avatar ? Auth::user()->avatar : config('filesystems.default_path.user_avatar')  }}">
                </a>
            </div> -->
            @if (Auth::user() && Auth::user()->isManager())
                <a href="{{ url('/admin') }}" class="bluebut">{{ trans('main.admin') }}</a>
            @endif
        </div>
        @endif
    </div>
    
  </div>
</div>
    </div> <!--- /hz -->
    <div id="search">
        <form name="search" id="searchForm" method="post" action="{{ url('/s/search') }}">
            <input type="search" name="search" placeholder="{{ trans('main.search') }}" value="{{$srchVal or ''}}" autofocus>
            <input type="submit" value="{{ trans('main.search') }}">
        </form>        
    </div>
<!---->
<div id="mainBox">
    <div id="navbar" class="{{-- request()->is('about') ? 'navbarMini' : '' --}}">
        <div id="nb">
        <div class="baron baron__root baron__clipper macosx scrollbar"><div class="baron__scroller">
        
        @if (Auth::guest())
        <div id="mobLogin">
            <div id="login">
                <a href="{{ url('/login') }}" class="greybut">{{ trans('main.enter') }}</a>
                <a href="{{ url('/register') }}" class="redbut">{{ trans('main.registration') }}</a>
            </div>
        </div>
        @else
        <div id="mobLogin">
            <div id="avatar"><a href="{{ url('/profile') }}"><img src="assets/media/default/default_user.jpg"></a></div>
            <div id="add"><a href="{{ url('/profile/articles/create') }}" class="redbut">{{ trans('main.add_question') }}</a></div>
            <div id="logout">
                <a href="{{ url('/profile/notifies') }}">
                    <div id="notice">
                    @if ($alertCount > 0)
                        <div id="noticeBall" class="test4">{{ $alertCount }}</div>
                    @endif
                    </div>
                </a>
                <a href="{{ url('/profile/bonuses') }}">
                    <div id="wallet">
                    @if ($bonusCount > 0)
                        <div id="walletBall">C</div>
                    @endif
                    </div>
                </a>
            </div>
        </div>
        @endif

        <ul id="fastnav">
          <li><a href="{{ url('/')  }}" class="main">{{ trans('main.mstart') }}</a></li>
          <li><a href="{{ action('ArticlesController@index') }}" class="profile">{{ trans('main.mprofile') }}</a>
                {{-- @if (request()->is("profile", "profile/*")) --}}
                @if (Auth::user())
                <ul>
                    <li><a href="{{ action('ArticlesController@index') }}">Мои вопросы</a></li>
                    <li><a href="{{ action('FileController@index') }}">Мои файлы</a></li>
                    <li><a href="{{ action('UserController@showNotifies') }}">Мои Уведомления</a></li>
                    <li><a href="{{ action('UserController@showSigns') }}">Мои подписки</a></li>
                    <li><a href="{{ action('UserController@showBonuses') }}">Мои Бонусы ({{ $bonusCount }})</a></li>
                    <li><a href="{{ action('UserController@showSettings') }}">Настройки</a></li>
                </ul>
                @endif
          </li>

          <li><a href="{{ url('new') }}" class="new">{{ trans('main.mnew') }}</a></li>
          <li><a href="{{ url('popul') }}" class="pop">{{ trans('main.mpopul') }}</a></li>
          <li><a href="{{ url('rev') }}" class="last">{{ trans('main.mrev') }}</a></li>
          <li><a href="{{ url('like') }}" class="like" data-target="false">{{ trans('main.mlike') }}</a></li>
        </ul>
        
        <ul id="catnav">
            {{-- $cats определены в App\Providers\GlobalViewServiseProvider --}}
            @foreach ($cats as $cat)
            <li><a href="{{url($cat->alias) }}" class="{{ $cat->letter }}">{{trans($cat->langkey)}}</a>
                <div class="aboutCategory">{{ trans($cat->langkey . 'desc') }}</div>
            </li>
            @endforeach
        </ul>
        
        <ul id="othernav">
            @foreach ($pages as $page)
            <li><a href="{{ url($page->alias) }}">{{ trans('main.' . $page->title) }}</a></li>
            @endforeach
            <!-- <li><a href="#">О проекте</a></li>
            <li><a href="#">Пользователям</a></li>
            <li><a href="#">Партнерская программа</a></li> -->
            @if (Auth::user())
                <li><a href="{{ url('/logout') }}">{{ trans('main.logout') }}</a></li>
            @endif

        </ul>
        
        <div id="lang">
            <a href="javascript:;" class="current"><img src="images/lang/{{ $currentLang }}.gif">{{ trans('main.current_lang') }}. 
            <span> {{-- trans('main.langswitcher') --}}</span></a>
            <ul class="otherlang">
                @foreach ($langs as $lang)
                    {{-- $langs и $currentLang определены в middleware SetLocale --}}
                    @if ($currentLang == $lang->lkey)
                        <li class="active"><a href="javascript:void(0);"><img src="images/lang/{{ $lang->lkey }}.gif">{{ $lang->ownname }}</a></li>
                    @else
                        <li><a href="{{ route('setlang', $lang->lkey) }}"><img src="images/lang/{{ $lang->lkey }}.gif">{{ $lang->ownname }}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div id="copyright">Protected by the Copyright <br>(Copyright number: 284670239, 284693752)
        <a href="mailto: commfi.net@gmail.com">commfi.net@gmail.com</a></div>
      </div>
      </div>
      </div>
    </div>
    
    <div id="{{ request()->is('v/*') ? 'content2' : 'content' }}" class="{{-- request()->is('about') ? 'contFull' : '' --}}">
        @yield('content')
    </div> 
</div>

<!-- Show the cropped image in modal -->
@if (request()->is("profile","profile/*"))
<div class="modal fade docs-cropped usermedia-module" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
      </div>
      <div class="modal-body">

        <div class="image-container">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-primary" id="download" href="javascript:void(0);">Download</a>
      </div>
    </div>
  </div>
</div><!-- /.modal -->
@endif

<!-- Lib module -->
@if(Auth::user() && !empty($files))
    @include('profile.libs.lib_module')
@endif
<!-- /.modal -->

<script src="js/jquery.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/JResponsive.js"></script>
<script src="js/baron.js"></script>
<script src="js/scripts.js"></script>
<script src="owl-carousel/owl.carousel.js"></script>
<script src="js/dropzone.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/cropper.min.js"></script>
<script src="js/main.js"></script>
<script src="js/custom.js"></script>

@if (request()->is("profile", "profile/*"))
<script src="js/typeahead.bundle.min.js"></script>
<script src="js/bootstrap-tagsinput.min.js"></script>
<script src="js/app.js"></script>
<script src="js/app_bs3.js"></script>
<script src="js/clipboard.min.js"></script>
<script src="js/profile.js"></script>
@endif

<link href="css/video-js.css" rel="stylesheet">
<script src="js/videojs-ie8.min.js"></script>
<script src="js/video.js"></script>

<script > 
var lastScroll = 0, hundert;
$(window).scroll(function(){
	//NAVIGATION FADEIN/OUT
	var st = $(document).scrollTop();
	if (st > 100 && st > lastScroll){
		$('#header').addClass('hiddenHeader');
        $("#navbar").css('margin-top','-67px');
        //$("#questionWrap").css('margin-top','-63px');
		hundert = st;
	}
	else if(st < (hundert - 33)){
		$('#header').removeClass('hiddenHeader');
        $("#navbar").css('margin-top','0px');
        //$("#questionWrap").css('margin-top','0px');
	}
  lastScroll = st; 	
  });
</script>
    
<noscript><div><img src="https://mc.yandex.ru/watch/45245325" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>