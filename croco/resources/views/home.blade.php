@extends('layouts.app')

@section('content')
<div id="contentHeader" style="background: url(../images/demo/section1.jpg);">
    <div id="contentHeaderMask"></div>
    <div id="contentHeaderAction" style="text-align: center; width: 100%;">
        <div id="mainHeaderBlock">
            <h1>Игра ассоциаций</h1>
            <h3>Общаемся без слов</h3>
            <div id="ABN">
                <a href="#" class="actButNew ABNfaq">Как играть</a>
                <a href="#" class="actButNew ABNprize">Призы</a>
            </div>
        </div>
       
    </div>
</div> 

@foreach ($pseudocat as $key => $cat)
@if (count($cat['articles']) > 0)
<div class="row">
    <div class="head">
        <h3>
        <img class="mbi" src="images/{{ $cat['letter'] }}Ava.jpg">
        {{ $cat['name'] }}
        </h3>
       <!-- <a href="{{ $cat['url'] }}">{{ trans('main.seemore') }}</a> -->
        
        
    </div>
        @if ($key == 'rec')
        <div class="owl browse">
        @else
        <div class="owl-small browse">
        @endif
            @foreach ($cat['articles'] as $article)
            <div class="item">
                <div class="img">
                    <a href="{{ url('/v/' . $article->alias) }}">
                        <!-- <img class="imgitem" src="{{-- $article->preview ? $article->preview->resize(300, 200) : ''--}}"> -->
                        @if ($article->preview)
                        <img class="imgitem" src="{{ $article->preview->resize(300, 200)}}">
                        @elseif ($article->attach->type_id == 3)
                        <img class="imgitem" src="{{ $article->attach->resize(300, 200)}}">
                        @else
                        <img class="avatar" src="{{ config('filesystems.default_path.article_preview') }}">
                        @endif
                        <div class="mask"></div>
                    </a>
                    <div class="artViews" style="POSITION: absolute; z-index: 9; top: 0px; right: 2px;">{!! $article->views !!}</div>
                    <div class="stats" style="display: none;">
                        <div class="soc">
                            <span class="like" data-target="false" data-iid="{{ $article->id}}">{{ $article->rating }}</span>
                            <span class="answer">{{ $article->answers->count() }}</span>
                        </div>
                        <div class="icons">
                            

                            <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    <div class="user">
                        <a href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}" title="{{ $article->author->name }}">
                            @if ($article->author->avatar)
                                <img class="avatar" src="{{ $article->author->avatar }}"> 
                            @else
                                <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                            @endif
                        </a>
                        <div>
                            <span class="ttp" data-title="{{ $langCountry[$article->author->country_id] }}">
                                <img class="country" src="images/flag/{{ $article->author->country_id }}.jpg">
                            </span>
                            <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}">
                                {{ $article->author->name }}
                            </a>
                            <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                        </div>
                    </div>
                    
                </div>
            </div>
            @endforeach

            @if (count($cat['articles']) < 1)
                <div class="item">{{ trans('main.emptycat') }}</div>
            @endif
        </div>

    <div class="clear"></div>
</div>
@endif
@endforeach

@foreach ($realcat as $key => $cat)
@if (count($cat['articles']) > 0)
<div class="row">
    <div class="head">
        <h3>
        <img class="mbi" src="images/menu/{{ $cat['letter'] }}.jpg">
        {{ $cat['name'] }}
        </h3>
        <!-- <a href="{{ $cat['url'] }}">{{ trans('main.seemore') }}</a> -->
        
        
    </div>
        <div class="owl-small browse">
            @foreach ($cat['articles'] as $article)
            <div class="item">
                <div class="img">
                    <a href="{{ url('/v/' . $article->alias) }}">
                        <!-- <img class="imgitem" src="{{-- $article->preview ? $article->preview->resize(300, 180) : ''--}}"> -->
                        @if ($article->preview)
                        <img class="imgitem" src="{{ $article->preview->resize(300, 200)}}">
                        @elseif ($article->attach->type_id == 3)
                        <img class="imgitem" src="{{ $article->attach->resize(300, 200)}}">
                        @else
                        <img class="avatar" src="{{ config('filesystems.default_path.article_preview') }}">
                        @endif
                        <div class="mask"></div>
                    </a>
                     <div class="artViews" style="POSITION: absolute; z-index: 9; top: 0px; right: 2px;">{!! $article->views !!}</div>
                    <div class="stats" style="display:none;">
                        <div class="soc">
                            <span class="like" data-target="false" data-iid="{{ $article->id}}">{{ $article->rating }}</span>
                            <span class="answer">{{ $article->answers->count() }}</span>
                        </div>
                        <div class="icons">
                            
                            
                            <img class="ico" src="images/icon/{{ $article->attach->type_id }}.png">
                        </div>
                    </div>
                </div>
                <div class="userinfo">
                    
                    <div class="user">
                        <a href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}" title="{{ $article->author->name }}">
                            @if ($article->author->avatar)
                                <img class="avatar" src="{{ $article->author->avatar }}"> 
                            @else
                                <img class="avatar" src="{{ config('filesystems.default_path.user_avatar') }}"> 
                            @endif
                        </a>
                        <div>
                            <span class="ttp" data-title="{{ $langCountry[$article->author->country_id] }}">
                                <img class="country" src="images/flag/{{ $article->author->country_id }}.jpg">
                            </span>
                            <a class="userName" href="{{ action('UserPagesController@index', ['alias' => $article->author->alias]) }}">
                                {{ $article->author->name }}
                            </a>
                            <p class="pubDate">{{ $article->created_at->format('d.m.Y') }}</p>
                        </div>
                    </div>
                     
                </div>
            </div>
            @endforeach

            @if (count($cat['articles']) < 1)
                <div class="item">{{ trans('main.emptycat') }}</div>
            @endif
        </div>

    <div class="clear"></div>
</div>
@endif
@endforeach

@endsection
