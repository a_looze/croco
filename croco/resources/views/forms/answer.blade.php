<!-- ADD ANSWER -->
<div id="add-answer-front">
    <form id="answer" action="{{ action('AnswerController@store') }}" class="dropzone"  enctype="multipart/form-data">

        <div class="add-answer">
            <span>{{ trans('main.drop_file_here')}}</span>
            <a id="add-answer" href="javascript:void(0);" class="greenbut dz-clickable">{{ trans('main.add_answer')}}</a>
            <div>
                <span>{{ trans('main.you_can_choose_file_from')}}
                    <a id="file-from-lib-link-3" class="show-file-lib" href="javascript:void(0);"
                        data-modal="lib-modal"
                        data-target="file-from-lib-3"
                        data-remove=""
                        data-action="file">{{ trans('main.from_your_library')}}
                    </a>
                </span>
            </div>
        </div>

        <input type="hidden" name="article_id" value="{{ $article->id }}"/>
        <input type="hidden" name="_method" value="post"/>
        <input type="hidden" name="file_from_lib" id="file-from-lib-3" value=""/>
        <input type="hidden" name="preview_from_lib" id="preview-from-lib-3" value=""/>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

    </form>

    <!-- msg block -->
    <p class="msg-block"> {{ trans('main.file_will_be_added_from_library')  }} <a class="greybut cansel-file" data-remove="file-from-lib-3" data-action="file" href="javascript:void(0)">{{ trans('main.cancel') }}</a></p>
    <!-- /msg block -->

    <!-- ERROR -->
    <p id="error-answer" class="error-msg-block"></p>

    <!-- submit -->
    <a id="submit-answer" href="javascript:void(0);" class="submit-answer-front greenbut margin-20">{{ trans('main.save')}}</a>
</div>
<div id="success-answer" style="display: none">
    <p>{{ trans('main.success_answer') }}</p>
</div>
<!-- /ADD ANSWER -->