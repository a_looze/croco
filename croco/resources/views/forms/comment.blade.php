<!-- ADD COMMENT -->

<!-- SUCCESS -->
<div id="success-comment" style="display: none">
    <p>{{ trans('main.success_comment') }}</p>
</div>

<!-- ERROR -->
<p id="error-comment" class="error-msg-block"></p>

<form id="comment" action="{{ action('CommentController@store') }}" class="dropzone" enctype="multipart/form-data" style="display:none;">

    <div class="add-comment">
        <p>{{ trans('main.drop_file_here')}}</p>
        <a id="add-comment" href="javascript:void(0);" class="greenbut dz-clickable">{{ trans('main.add_comment') }}</a>
        <div>
            <span>{{ trans('main.you_can_choose_file_from')}}
                <a id="file-from-lib-link-2" class="show-file-lib" href="javascript:void(0);"
                    data-modal="lib-modal"
                    data-target="file-from-lib-2"
                    data-remove=""
                    data-action="file">{{ trans('main.from_your_library') }}
                </a>
            </span>
        </div>
    </div>

    <input type="hidden" name="parent_id" id="target-id" value="{{ $article->id }}"/>
    <input type="hidden" name="target" id="target-comment" value="post"/>
    <input type="hidden" name="_method" value="post"/>
    <input type="hidden" name="file_from_lib" id="file-from-lib-2" value=""/>
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    <textarea id="comment-description" style="display: none;" name="content"></textarea>

</form>

<!-- msg block -->
 <p class="msg-block"> {{ trans('main.file_will_be_added_from_library')  }} <a class="greybut cansel-file" data-remove="file-from-lib-2" data-action="file" href="javascript:void(0)">{{ trans('main.cancel') }}</a></p>
 <!-- /msg block -->

<div id="commentNow">
    <textarea class="has-target" name="description" placeholder="{{ trans('main.comment') }}"
        data-target="comment-description"
        id="msg"
        cols="30"
        rows="10"></textarea>
</div>
<!-- /ADD COMMENT -->