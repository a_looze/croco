<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\Convert::class,
        Commands\Import::class,
        Commands\Scan::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // * * * * * /usr/local/bin/php /home/commfi/new_public_html/croco/artisan schedule:run >> /dev/null 2>&1 
        // * * * * * /usr/local/bin/php /home/commfi/new_public_html/croco/artisan schedule:run >> /home/commfi/new_public_html/croco/scan.log 2>&1 
        $schedule->command('scan')
                ->everyFiveMinutes();
    }
}
