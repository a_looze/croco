<?php

namespace App\Console\Commands;

// use Psr\Logger\LoggerInterface as Logger;
use App\User;
use App\File;
use App\Helpers\FileHelper;
use Log;

use Illuminate\Console\Command;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Video\X264;
use FFMpeg\Format\Video\WMV;
use FFMpeg\Format\Video\WebM;
use FFMpeg\Format\Video\Ogg;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Coordinate\Framerate;

use FFMpeg\Format\Audio\Vorbis;
use FFMpeg\Format\Audio\Mp3;

class Convert extends Command
{
  /**
   * Имя и параметры консольной команды.
   *
   * @var string
   */
  protected $signature = 'convert {file}';

  /**
   * Описание консольной команды.
   *
   * @var string
   */
  protected $description = 'Convert video/audio file';

    /**
     * Выполнение консольной команды.
     *
     * @return mixed
     */
    public function handle()
    {
        $ffmpeg = FFMpeg::create([
            'ffmpeg.binaries' => '/usr/local/bin/ffmpeg', //base_path('ffmpeg/ffmpeg'),
            'ffprobe.binaries' => '/usr/local/bin/ffprobe', //base_path('ffmpeg/ffprobe'),
            'timeout'          => 3600, // The timeout for the underlying process
            'ffmpeg.threads'   => 1,   // The number of threads that FFMpeg should use
        ]);

        $file = $this->argument('file');
        $pathName = storage_path('videoin/' . $file);
        if (!file_exists($pathName)) {
            $this->error('File ' . $pathName . ' not exists!');
            return false;
        }

        $fAr = pathinfo($pathName);

        // dd($fAr);
        /*array:4 [                                                                     
          "dirname" => "D:\OpenServer\domains\croco.lc\croco\croco\storage\videoin"   
          "basename" => "1.mp4"                                                       
          "extension" => "mp4"                                                        
          "filename" => "1"                                                           
        ] */

        $ffmpeg = $ffmpeg->open($pathName);

        $ffmpeg->getFFMpegDriver()->listen(new \Alchemy\BinaryDriver\Listeners\DebugListener());
        $ffmpeg->getFFMpegDriver()->on('debug', function ($message) {
            echo $message."\n";
        });

        // получаем id файла и запись из БД
        $fileObj = File::where('id', $fAr['filename'])->first();
        if (!is_object($fileObj)) {
            $this->error('Db object for file' . $pathName . ' not found!');
            return false;
        }

        // $userId = $fileObj->user_id;
        // $user = User::where('id', $userId)->first();
        // if (!is_object($user)) {
        //     $this->error('User object for file' . $pathName . ' not found!');
        //     return false;
        // }
        $fh = new FileHelper();

        $destFileNoExt = $fh->getPathByFileId($fileObj->id);
        $dir = str_replace('/' . $fileObj->alias, '', $destFileNoExt);

        // $this->info($dir);
        if (!file_exists(public_path($dir))) {
            // assets/media/users/111/1
            // $this->info(dirname(public_path($dir)));
            if (!file_exists(dirname(public_path($dir)))) {
                // assets/media/users/111
                
                mkdir(dirname(public_path($dir)), 0777);
                chown(dirname(public_path($dir)), 'commfi'); 
            }
            mkdir(public_path($dir), 0777);
            chown(public_path($dir), 'commfi'); 
        }


        switch ($fAr['extension']) {
            case 'mp3':
            case 'wma':
            case 'oga':
            case 'wma':
                /*find $input_dir/$tmp_name -type f -exec /usr/local/bin/ffmpeg -i {} -vn -ar 44100 -ac 2 -ab 192 -f mp3 $out_dir/$file_name.mp3 >> /home/shcoder/logfile.log \;
                find $input_dir/$tmp_name -type f -exec /usr/local/bin/ffmpeg -i {} -acodec libvorbis -f ogg $out_dir/$file_name.ogg >> /home/shcoder/logfile.log \;
                */
                $mp3Format = new Mp3('libmp3lame');
                $ogaFormat = new Vorbis();
                $ffmpeg
                    ->save($mp3Format, public_path($destFileNoExt . '.mp3'))
                    ->save($ogaFormat, public_path($destFileNoExt . '.oga')) // ogg??
                    ;
            break;


            case 'ogg':
            case 'mp4':
            case 'wmv':
            case 'webm':
            case 'avi':
            case 'mpeg':
                $dimension = new Dimension(800, 600);
                $mode = ResizeFilter::RESIZEMODE_SCALE_WIDTH;
                $useStandards = true;

                $ffmpeg
                    ->filters()
                    ->watermark(public_path('images/logo.png'), array(
                        'position' => 'relative',
                        'bottom' => 10,
                        'right' => 10,
                    ))    
                    ->resize($dimension, $mode, $useStandards)
                    // ->framerate($framerate, $gop)
                    ->synchronize();

                // для видеофайлов также принудительно создаем превью
                if ($fileObj->preview_id) {
                    // $previewFileNoExt = $fh->getPathByFileId($fileObj->preview_id);
                    $previewFileNoExt = 'assets/media/users/' . $fileObj->user_id.'/3/' . $fileObj->alias;

                    $dir = 'assets/media/users/' . $fileObj->user_id.'/3';

                    if (!file_exists(public_path($dir))) {
                        mkdir(public_path($dir), 0777);
                        chown(public_path($dir), 'commfi'); 
                    }

                    $ffmpeg
                        ->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(2))
                        ->save(public_path($previewFileNoExt . '-a.jpg'));

                    $ffmpeg
                        ->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(2))
                        ->save(public_path($previewFileNoExt . '-f.jpg'));
                }

                

                /*
                find $input_dir/$tmp_name -type f -exec /usr/local/bin/ffmpeg -i {} -f mp4 -s 640x480 -b 512k -vcodec libx264 -preset medium -ar 44100 -ab 96k -threads 1 $out_dir/$file_name.mp4 >> /home/shcoder/logfile.log \;
                find $input_dir/$tmp_name -type f -exec /usr/local/bin/ffmpeg -i {} -vcodec libtheora -acodec libvorbis $out_dir/$file_name.ogv >> /home/shcoder/logfile.log \;
                find $input_dir/$tmp_name -type f -exec /usr/local/bin/ffmpeg -i {} -vcodec libvpx -f webm $out_dir/$file_name.webm >> /home/shcoder/logfile.log \;
                find $input_dir/$tmp_name -type f -exec /usr/local/bin/ffmpeg -i {} -r 1 -ss 00:00:02 -t 00:00:05 $out_dir/$file_name%02d.jpg >> /home/shcoder/logfile.log \;
                */

                $mp4Format = new X264('libmp3lame');
                $ogvFormat = new Ogg();
                $webmFormat = new WebM();

                $mp4Format
                    -> setKiloBitrate(512)
                    -> setAudioChannels(2)
                    -> setAudioKiloBitrate(96);

                $ffmpeg
                    ->save($mp4Format, public_path($destFileNoExt . '.mp4'))
                    ->save($ogvFormat, public_path($destFileNoExt . '.ogv'))
                    ->save($webmFormat, public_path($destFileNoExt . '.webm'))
                    ;
            break;
            
            default:
                $this->error('File ' . $pathName . ' has bad extension!');
                return false;
            break;
        }

        unlink($pathName);

        $this->info('Saved with ' . $destFileNoExt . '.EXT');
        return true;
    }
}
