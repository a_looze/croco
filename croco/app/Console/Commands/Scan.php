<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Scan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan directory _videoin_ and run converter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ext = '{mp3,mp4,wma,wmv,webm,avi,ogg,oga}';
        $dir = storage_path('videoin/') . '*.' . $ext;
        $filesAr = glob($dir, GLOB_NOESCAPE|GLOB_BRACE);
        
        foreach ($filesAr as $file) {
            $fAr = pathinfo($file);
            $this->info('Call convert ' . $fAr['basename']);
            $this->call('convert', ['file' => $fAr['basename']]);
        }
    }
}
