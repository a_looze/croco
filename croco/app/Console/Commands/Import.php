<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Image;
use App\User;
use App\Article;
use App\File;
use App\Answer;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import {modxpath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import users and files from MODX';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modxBaseDir = $this->argument('modxpath');
        $modxBaseDir = trim(rtrim($modxBaseDir, '/'), '\\') . '/';
        // dd($modxBaseDir . 'index-ajax.php');
        if (!$modxBaseDir || !is_dir($modxBaseDir) || !file_exists($modxBaseDir . 'index-ajax.php')) {
            $this->error('No valid MODX_BASE_PATH!' . PHP_EOL);
            return;
        }

        $this->comment(PHP_EOL . 'Start' . PHP_EOL);

        $modx = DB::connection('mysqlprev');
        $lara = DB::connection('mysql');

        $q = "SELECT u.*, wua.*, uf.*, uf.id as fileid 
            FROM modx_file_path uf 
            LEFT JOIN modx_web_users u
            ON u.id=uf.userid
            LEFT JOIN modx_web_user_attributes wua
            ON wua.internalKey=u.id 
            ";
        $mRes = $modx->select($q);
        $this->comment('Found ' . count($mRes) . ' files in MODX' . PHP_EOL);

        foreach ($mRes as $ufObj) {
            $uAr[$ufObj->internalKey][] = $ufObj;
        }
        $this->comment('Found ' . count($uAr) . ' users with files in MODX' . PHP_EOL);

        // print_r($uAr);
        // die();

        // создадим список категорий
        /*1 => 'play', M
            2 => 'dance', D
            3 => 'draw', P
            4 => 'move', G
            5 => 'voice', V
        */
        $catIds = [
            71 => 1,
            72 => 1,
            255 => 1,
            66 => 1,
            74 => 2,
            75 => 2,
            76 => 2,
            67 => 2,
            77 => 3,
            78 => 3,
            142 => 3,
            68 => 3,
            80 => 4,
            81 => 4,
            256 => 4,
            257 => 4,
            69 => 4,
            83 => 5,
            84 => 5,
            70 => 5,
        ];

        // создаем юзеров и копируем для них файлы
        foreach ($uAr as $modxUid => $ufObjAr) {
            // данные юзера
            $uObjData = $ufObjAr[0];
            $user = User::where('email', $uObjData->email)->first();
            // dd($uObjData->email);
            $this->comment('=======================================');
            if (!$user) {
                $user = User::create();
                $user->name = $uObjData->fullname;
                $user->email = $uObjData->email;
                $user->alias = $user->uniqueAlias();
                $user->password = '$2y$10$8q/58wnBsXn4apX1.1OhveBPx7vxvYDzVHzqE/dJLkjdzEp/2qs/W'; // всем ставим 123456
                $user->role = '0';
                $user->country_id = $uObjData->country;
                $user->banner = '';
                $user->notice_article_like = '1';
                $user->notice_answer_like = '1';
                $user->notice_comment_like = '1';
                $user->notice_referral_sign = '1';
                $user->save();
                $this->comment('User  ' . $user->name . ' was created with ID=' . $user->id);
            } else {
                $this->comment('User  ' . $user->name . ' was found with ID=' . $user->id);
            }

            // работаем с файлами
            $oldAva = $modxBaseDir . $uObjData->mobilephone;
            if (!file_exists(public_path('/assets/media/users/' . $user->id))) {
                mkdir(public_path('/assets/media/users/' . $user->id));
            }
            if ($uObjData->mobilephone != '' && file_exists($oldAva)) {
                $this->comment('User avatar found: ' . $oldAva);                
                $newAva = '/assets/media/users/' . $user->id . '/avatar.jpg'; // public/assets/media/users/3/avatar.jpg
                
                $img = Image::make($oldAva);
                $img->save(public_path() . $newAva);
                $img->destroy();
                $user->avatar = $newAva;
                $this->comment('User avatar was created: ' . $newAva);
            } else {
                $this->error('User avatar not found: ' . $oldAva);
            }

            $user->save();
            
            foreach ($ufObjAr as $uObj) {
                // библиотека файлов
                $srcName = $uObj->filename;
                $srcType = $uObj->type;

                switch ($srcType) {
                    case 'image':
                        $tvId = 24;
                    break;
                    
                    case 'audio':
                        $tvId = 23;
                    break;
                    
                    case 'video':
                        $tvId = 22;
                    break;

                    default:
                        $tvId = false;
                    break;
                }

                if (!$tvId) {
                    // не переносим файлы, неизвестный тип
                    $this->error('Unknown file type: ' . $srcType);
                    continue;
                }

                // ищем упоминания файла в вопросах
                $q = "SELECT * FROM modx_site_tmplvar_contentvalues
                    WHERE value LIKE '%" . $srcName . "%'
                    AND tmplvarid=" . $tvId;
                $tvAr = $modx->select($q);
                if (count($tvAr) > 1) {
                    $this->error('Error in record count for filename ' . $srcName . ' and modx user ' . $uObj->internalKey);
                    // continue;
                } else {
                    // $this->comment('Files count: ' . count($tvAr));
                    if (count($tvAr) == 1) {
                        $filesStr = $tvAr[0]->value;
                        $cId = $tvAr[0]->contentid;

                        // получаем данные для темы
                        $q = "SELECT c.*, tv1.value views, tv2.value previews, tv3.value catid 
                                FROM modx_site_content c 
                                LEFT JOIN modx_site_tmplvar_contentvalues tv1
                                ON tv1.tmplvarid=29 AND tv1.contentid=c.id 
                                LEFT JOIN modx_site_tmplvar_contentvalues tv2
                                ON tv2.tmplvarid=25 AND tv2.contentid=c.id 
                                LEFT JOIN modx_site_tmplvar_contentvalues tv3
                                ON tv3.tmplvarid=27 AND tv3.contentid=c.id 
                                WHERE c.id=" . $cId . "
                                AND c.published=1 AND c.deleted=0
                            ";
                        $docs = $modx->select($q);

                        if (!$docs || count($docs) < 1) {
                            $this->error($cId . ': MODX doc not found');
                        } else {
                            // создаем тему
                            $docAr = $docs[0];

                            $article = Article::create();
                            $article->alias = $uObj->filename;
                            $article->user_id = $user->id;
                            $article->title = $docAr->pagetitle;
                            $article->views = $docAr->views;
                            $article->rating = rand(5,15);
                            $article->category_id = $catIds[$docAr->catid];
                            $article->save();

                            $this->comment('Article ' . $article->title . ' was created with ID=' . $article->id);
                            $artAr[$cId] = $article->id; // сохраняем соответствие старого и нового id

                            // работаем непосредственно с файлами
                            switch ($srcType) {
                                case 'image':
                                    if (file_exists($modxBaseDir . $filesStr)) {
                                        $file = File::create();
                                        $preview = File::create();

                                        $file->user_id = $user->id;
                                        $file->alias = $uObj->filename;
                                        $file->type_id = 3;
                                        $file->title = $uObj->filename;
                                        $file->preview_id = $preview->id;

                                        $preview->user_id = $user->id;
                                        $preview->alias = $uObj->filename . '-a';
                                        $preview->type_id = 3;
                                        $preview->title = $uObj->filename;
                                        $preview->preview_id = 0;

                                        $file->save();
                                        $preview->save();

                                        // копируем файлы с новым расширением в папку юзера
                                        $this->comment('Create dir  ' . public_path('assets/media/users/') . $user->id . '/3/');
                                        if (!file_exists(public_path('assets/media/users/') . $user->id . '/3/')) {
                                            mkdir(public_path('assets/media/users/') . $user->id . '/3/');
                                        }
                                        $userDir = public_path('assets/media/users/') . $user->id . '/3/';

                                        $img = Image::make($modxBaseDir . $filesStr);
                                        $img->save($userDir . $uObj->filename . '.jpg');
                                        $img->save($userDir . $uObj->filename . '-a.jpg');
                                        $img->destroy();
                                    }
                                break;
                                
                                case 'audio':
                                    $filesAr = explode('||', $filesStr);
                                    // mp3==assets/media/userfiles/6/9/e/1/0/6/a/51aa7569e106a.mp3||oga==assets/media/userfiles/6/9/e/1/0/6/a/51aa7569e106a.ogg
                                    foreach ($filesAr as $str) {
                                        list($ext, $path) = explode('==', $str);
                                        $fAr[$ext] = $path;
                                    }
                                    
                                    if (file_exists($modxBaseDir . $fAr['mp3'])) {
                                        $file = File::create();
                                        // $preview = File::create();

                                        $file->user_id = $user->id;
                                        $file->alias = $uObj->filename;
                                        $file->type_id = 2;
                                        $file->title = $uObj->filename;
                                        $file->preview_id = $preview->id;

                                        $file->save();

                                        // переносим файлы
                                        $dest = public_path('assets/media/users/') . $user->id . '/2/';
                                        $this->comment('Create dir  ' . $dest);

                                        if (!file_exists($dest)) {
                                            mkdir($dest);
                                        }

                                        foreach ($fAr as $ext => $path) {
                                            $src = $modxBaseDir . $path;
                                            if (file_exists($src)) {
                                                copy($src, $dest . $uObj->filename . '.' . $ext);
                                            }
                                        }
                                    }
                                break;
                                
                                case 'video':
                                    $filesAr = explode('||', $filesStr);
                                    // mp4==assets/media/userfiles/2/4/9/7/3/9/9/51abc82497399.mp4||ogv==assets/media/userfiles/2/4/9/7/3/9/9/51abc82497399.ogv||webm==assets/media/userfiles/2/4/9/7/3/9/9/51abc82497399.webm
                                    foreach ($filesAr as $str) {
                                        list($ext, $path) = explode('==', $str);
                                        $fAr[$ext] = $path;
                                    }

                                    $prevAr = explode('||', $docAr->previews);
                                    // assets/media/userfiles/2/4/9/7/3/9/9/51abc8249739901.jpg||assets/media/userfiles/2/4/9/7/3/9/9/51abc8249739902.jpg||assets/media/userfiles/2/4/9/7/3/9/9/51abc8249739903.jpg||assets/media/userfiles/2/4/9/7/3/9/9/51abc8249739904.jpg||assets/media/userfiles/2/4/9/7/3/9/9/51abc8249739905.jpg
                                    $prevPathName = $prevAr[0];
                                    
                                    if (file_exists($modxBaseDir . $fAr['mp4'])) {
                                        $file = File::create();
                                        $preview = File::create();

                                        $file->user_id = $user->id;
                                        $file->alias = $uObj->filename;
                                        $file->type_id = 1;
                                        $file->title = $uObj->filename;
                                        $file->preview_id = $preview->id;

                                        $preview->user_id = $user->id;
                                        $preview->alias = $uObj->filename . '-a';
                                        $preview->type_id = 3;
                                        $preview->title = $uObj->filename;
                                        $preview->preview_id = 0;

                                        $file->save();
                                        $preview->save();

                                        // копируем файлы с новым расширением в папку юзера
                                        $this->comment('Create dir  ' . public_path('assets/media/users/') . $user->id . '/3/');
                                        if (!file_exists(public_path('assets/media/users/') . $user->id . '/3/')) {
                                            mkdir(public_path('assets/media/users/') . $user->id . '/3/');
                                        }
                                        $userDir = public_path('assets/media/users/') . $user->id . '/3/';

                                        if (file_exists($modxBaseDir . $prevPathName )) {
                                            $img = Image::make($modxBaseDir . $prevPathName);
                                            $img->save($userDir . $uObj->filename . '-f.jpg');
                                            $img->save($userDir . $uObj->filename . '-a.jpg');
                                            $img->destroy();
                                        } else {
                                            $this->error('File ' . $modxBaseDir . $prevPathName);
                                            die();
                                        }

                                        

                                        // переносим видеофайлы
                                        $dest = public_path('assets/media/users/') . $user->id . '/1/';
                                        $this->comment('Create dir  ' . $dest);

                                        if (!file_exists($dest)) {
                                            mkdir($dest);
                                        }

                                        foreach ($fAr as $ext => $path) {
                                            $src = $modxBaseDir . $path;
                                            if (file_exists($src)) {
                                                copy($src, $dest . $uObj->filename . '.' . $ext);
                                            }
                                        }
                                    }
                                break;
                            }


                            $article->file_id = $file->id;
                            $article->preview_file_id = $preview->id;
                            $article->save();
                        }

                        
                    } /*else {
                        print_r($tvAr);
                        $this->comment('Ask db: ' . $q);
                    }*/
                }

                // упоминания файла в комментариях (ответах)
                $mCom = $modx->select("SELECT * FROM modx_user_comments WHERE fileid=" . $uObj->fileid);
                if (!$mCom || count($mCom) < 1) {
                    $this->error('Comment with this file not found: fileid=' . $uObj->fileid);
                    continue;
                }

                $this->comment('Found answer with fileid=' . $uObj->fileid);

                $mAnsw = $mCom[0];
                $answer = Answer::create();

                $answer->article_id = $mAnsw->contentid * 100; // чтобы потом перезаписать новыми id
                $answer->user_id = $user->id;
                $answer->author_say = $mAnsw->authorsay;
                $answer->rating = rand(1,10);

                // работаем непосредственно с файлами
                $pathAr = str_split(substr($uObj->filename, -7));
                $fileDir = 'assets/media/userfiles/' . implode('/', $pathAr);
                // assets/media/userfiles/[+path+]/[+filename+]01.jpg

                switch ($srcType) {
                    case 'image':
                        if (file_exists($modxBaseDir . $fileDir . '/' . $uObj->filename . '.png')) {
                            $file = File::create();
                            $preview = File::create();

                            $file->user_id = $user->id;
                            $file->alias = $uObj->filename;
                            $file->type_id = 3;
                            $file->title = $uObj->filename;
                            $file->preview_id = $preview->id;

                            $preview->user_id = $user->id;
                            $preview->alias = $uObj->filename . '-a';
                            $preview->type_id = 3;
                            $preview->title = $uObj->filename;
                            $preview->preview_id = 0;

                            $file->save();
                            $preview->save();

                            // копируем файлы с новым расширением в папку юзера
                            $this->comment('Create dir  ' . public_path('assets/media/users/') . $user->id . '/3/');
                            if (!file_exists(public_path('assets/media/users/') . $user->id . '/3/')) {
                                mkdir(public_path('assets/media/users/') . $user->id . '/3/');
                            }
                            $userDir = public_path('assets/media/users/') . $user->id . '/3/';

                            $img = Image::make($modxBaseDir . $fileDir . '/' . $uObj->filename . '.png');
                            $img->save($userDir . $uObj->filename . '.jpg');
                            $img->save($userDir . $uObj->filename . '-a.jpg');
                            $img->destroy();
                        }
                    break;
                    
                    case 'audio':
                        $fAr = [
                                'mp3' => $fileDir . '/' . $uObj->filename . '.mp3',
                                'ogg' => $fileDir . '/' . $uObj->filename . '.ogg',
                            ];

                        
                        if (file_exists($modxBaseDir . $fAr['mp3'])) {
                            $file = File::create();

                            $file->user_id = $user->id;
                            $file->alias = $uObj->filename;
                            $file->type_id = 2;
                            $file->title = $uObj->filename;
                            $file->preview_id = $preview->id;

                            $file->save();

                            // переносим файлы
                            $dest = public_path('assets/media/users/') . $user->id . '/2/';
                            $this->comment('Create dir  ' . $dest);

                            if (!file_exists($dest)) {
                                mkdir($dest);
                            }

                            foreach ($fAr as $ext => $path) {
                                $src = $modxBaseDir . $path;
                                if (file_exists($src)) {
                                    copy($src, $dest . $uObj->filename . '.' . $ext);
                                }
                            }
                        }
                    break;
                    
                    case 'video':
                        $fAr = [
                                'mp4' => $fileDir . '/' . $uObj->filename . '.mp4',
                                'ogv' => $fileDir . '/' . $uObj->filename . '.ogv',
                                'webm' => $fileDir . '/' . $uObj->filename . '.webm',
                            ];


                        $prevPathName = $fileDir . '/' . $uObj->filename . '01.jpg';
                        
                        if (file_exists($modxBaseDir . $fAr['mp4'])) {
                            $file = File::create();
                            $preview = File::create();

                            $file->user_id = $user->id;
                            $file->alias = $uObj->filename;
                            $file->type_id = 1;
                            $file->title = $uObj->filename;
                            $file->preview_id = $preview->id;

                            $preview->user_id = $user->id;
                            $preview->alias = $uObj->filename . '-a';
                            $preview->type_id = 3;
                            $preview->title = $uObj->filename;
                            $preview->preview_id = 0;

                            $file->save();
                            $preview->save();

                            // копируем файлы с новым расширением в папку юзера
                            $this->comment('Create dir  ' . public_path('assets/media/users/') . $user->id . '/3/');
                            if (!file_exists(public_path('assets/media/users/') . $user->id . '/3/')) {
                                mkdir(public_path('assets/media/users/') . $user->id . '/3/');
                            }
                            $userDir = public_path('assets/media/users/') . $user->id . '/3/';

                            if (file_exists($modxBaseDir . $prevPathName )) {
                                $img = Image::make($modxBaseDir . $prevPathName);
                                $img->save($userDir . $uObj->filename . '-f.jpg');
                                $img->save($userDir . $uObj->filename . '-a.jpg');
                                $img->destroy();
                            } else {
                                $this->error('File ' . $modxBaseDir . $prevPathName);
                                die();
                            }

                            // переносим видеофайлы
                            $dest = public_path('assets/media/users/') . $user->id . '/1/';
                            $this->comment('Create dir  ' . $dest);

                            if (!file_exists($dest)) {
                                mkdir($dest);
                            }

                            foreach ($fAr as $ext => $path) {
                                $src = $modxBaseDir . $path;
                                if (file_exists($src)) {
                                    copy($src, $dest . $uObj->filename . '.' . $ext);
                                }
                            }
                        }
                    break;
                }

                $answer->file_id = $file->id;
                $answer->preview_file_id = $preview->id;
                $answer->save();
            }
        }

        // перезаписываем ID статей в ответах
        $this->comment(PHP_EOL . 'Article IDs:' . PHP_EOL);

        $answerCollection = Answer::get();
        foreach ($answerCollection as $answ) {
            $oldArticleId = intval($answ->article_id / 100);
            if (isset($artAr[$oldArticleId])) {
                $this->comment($answ->article_id . ' => ' . $artAr[$oldArticleId]);
                $answ->article_id = $artAr[$oldArticleId];
                $answ->save();
            } else {
                $answ->delete();
            }
        }

        $this->comment(PHP_EOL . 'DONE' . PHP_EOL);
        // print_r($mRes[0]);
        /*stdClass Object                                                  
            (                                                                
                [id] => 82                                                   
                [username] => 51a8c0cd76235                                  
                [password] => b9c93fbdfd2a30504e05d3b0b32307da               
                [cachepwd] =>                                                
                [internalKey] => 36                                          
                [fullname] => ╨Ь╨░╨║╨░╤А                                     
                [role] => 0                                                  
                [email] => unit10@lab39.ru                                   
                [phone] =>                                                   
                [mobilephone] => assets/media/avatars/559939faac18e.jpg      
                [blocked] => 0                                               
                [blockeduntil] => 0                                          
                [blockedafter] => 0                                          
                [logincount] => 4                                            
                [lastlogin] => 1437092359                                    
                [thislogin] => 1437392548                                    
                [failedlogincount] => 0                                      
                [sessionid] => b3ci24f0qtsmc72na3rj8qo0f7                    
                [dob] => 0                                                   
                [gender] => 0                                                
                [country] => 220                                             
                [state] =>                                                   
                [zip] =>                                                     
                [fax] =>                                                     
                [photo] => assets/images/avatars/flags/220.jpg               
                [comment] =>                                                 
                [userid] => 36                                               
                [contentid] => 0                                             
                [filename] => 51abdd6688ac4                                  
                [available] => 1                                             
                [type] => image                                              
                [description] => default                                     
                [title] => ╨Ъ╤А╨╛╤В                                          
            )                                                                */
       

    }
}
