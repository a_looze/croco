<?php

namespace App\Listeners;

use App\Eventlogs;
use App\Events\LogUserActivity;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogUserActivityListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogUserActivity  $event
     * @return void
     */
    public function handle(LogUserActivity $event)
    {
        $eventlog = new Eventlogs();

        $data = $event->getData();
        if ($data['renew'] === true) {
            $eventlog->where('user_id', $data['user_id'])
                    ->where('target_id', $data['target_id'])
                    ->where('event_id', $data['event_id'])
                    ->delete();
        }
        $eventlog->create($data);
    }
}
