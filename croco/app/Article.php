<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // use DateFormatTrait; // это был тест

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'type_id', 'category_id', 'img_preview', 'visible', 'commented', 'alias', 'user_id',
        'preview_file_id', 'file_id', 'for_user_id',
    ];

    protected $guarded = [];

    public function preview()
    {
        return $this->hasOne(File::class, 'id', 'preview_file_id');
    }

    public function attach()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * тестовый метод для унификации всех типов likeable (статьи, комменты, ответы)
     */
    public function article()
    {
        return $this->hasOne(Article::class, 'id', 'id');
    }
}
