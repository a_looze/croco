<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Session;
use App\Lang as Lang;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // язык по умолчанию для всего сайта указан в конфиге
        $currentLocale = Config::get('app.locale');

        // Если пользователь уже был на нашем сайте, то в сессии будет значение выбранного им языка.
        $sessionLocale = Session::get('locale'); 

        if (isset($sessionLocale)) {
            // Проверяем, что у пользователя в сессии установлен доступный язык
            $langConfirm = Lang::where('lkey', $sessionLocale)
                        ->where('active', 1)
                        ->get(); 
            if ($langConfirm) {
                $currentLocale = $sessionLocale;
            }
        } else {
            // пытаемся получить из запроса предпочитаемый язык
            $browserLangStr = $request->server->get('HTTP_ACCEPT_LANGUAGE');

            // код ниже взят напрокат из MODX YAMS
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i'
                            , $browserLangStr
                            , $parsedAcceptLanguageHeader
                        );
            $langTags = array();

            if ( count( $parsedAcceptLanguageHeader[1] ) > 0 ) {
                // create a list like 'en' => 0.8
                $langTags = array_combine(
                    $parsedAcceptLanguageHeader[1]
                    , $parsedAcceptLanguageHeader[4]
                );

                // set default to 1 for any without q factor
                // For any with 1 or without a q factor (effective 1)
                // count down from a very large number to
                // ensure the correct sort order...
                $oneSort = 20000000;
                foreach ( $langTags as $langTag => $val ) {
                    if ( $val == 1 || $val === '' ) {
                        $oneSort -= 1;
                        $langTags[ $langTag ] = $oneSort;
                    }

                    // sort list based on value
                    arsort( $langTags, SORT_NUMERIC );
                }
            }

            // dd($langTags);
            
            $langsCollection = Lang::get();
            foreach ( $langTags as $langTag => $val ) {
                foreach ( $langsCollection as $langObj ) {
                    if ($langObj->lkey == $langTag) {
                        $currentLocale = $langObj->lkey;
                        break(2);
                    }
                }
            }
        }

        Session::put('locale', $currentLocale);
        App::setLocale($currentLocale); 

        // добавляем переключение языка во все шаблоны
        $langs = Lang::where('active', 1)->orderby('priority', 'ASC')->get();
        view()->share('langs', $langs);
        view()->share('currentLang', $currentLocale); 

        $hlp = new App\Helpers\Country();
        $langCountry = $hlp->getList();
        // dd($langCountry);
        view()->share('langCountry', $langCountry);

        return $next($request);                                   
    }
}
