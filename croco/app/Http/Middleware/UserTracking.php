<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

/**
 * Данный код решает проблемы сохранения различной информации для пользователей
 *
 * ++ 1) Если человек впервые попал на сайт ему должна открываться именно страница презентации, а не главная страница
 * 2) Если пользователь воспользовался иконкой то при дальнейших переходах по сайту его выбор должен учитываться и сохраняться
 * 3) При просмотре ответа автора, и последующей публикации своего ответа, предлагаю не начислять баллы
 * ++ 4) Для привлечения друзей у каждого пользователя есть уникальная реферальная ссылка
 */

// cookies
/*'ut_rla', // для хранения реферальных данных
'ut_sv', // для хранения прозрачности боковой ленты
'ut_aav', // для хранения списка "подсмотренных" авторских ответов
'ut_fv', // первое посещение устанавливает эту куку, если ее нет - редирект на презентацию
'ut_vql', // список просмотренных страниц
*/

class UserTracking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // если юзер уже авторизован, он в этом месте нам неинтересен
        $userObj = auth()->user();

        if (is_object($userObj)) {
            return $next($request);
        }

        /******************
            перенесено в ReferralController
            *********************************/
        // если получена реферальная ссылка
        // $referalLink = $request->input('rla');
        // $isRefCookieAlreadySet = $request->cookie('ut_rla') !== null ? true : false;

        // // пока проверяем только наличие реф.ссылки, чтобы можно было тестировать
        // // после тестирования не даем перезаписывать реф. куку (может быть придется добавить проверку на наличие алиаса юзера)
        // // if (!$isRefCookieAlreadySet && $referalLink !== null) {
        // if ($referalLink !== null) {
        //     // реферальная кука еще не установлена и пользователь пришел по реферальной ссылке
        //     $do = Cookie::queue('ut_rla', $referalLink, 10000);
        //     return redirect($request->url()); 
        // }
        
        // проверяем, первый ли это визит и запрошена ли главная страница
        // если да и да - редирект на презентацию
        $isFirstVisit = $request->cookie('ut_fv') !== null ? false : true;
        if ($isFirstVisit && $request->route()->uri() == '/') {
            $do = Cookie::queue('ut_fv', true, 10000);
            return redirect(url('about'));
        }
        
        return $next($request);
    }
}
