<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

class EncryptCookies extends BaseEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        // 'ut_rla', // для хранения реферальных данных
        // 'ut_sv', // для хранения прозрачности боковой ленты
        // 'ut_aav', // для хранения списка "подсмотренных" авторских ответов
        // 'ut_fv', // первое посещение устанавливает эту куку, если ее нет - редирект на презентацию
    ];
}
