<?php

namespace App\Http\Middleware;

use Closure;
use App\User as User;

class AdminPanelRequestEdit
{
    /**
     * Перезаписываем поля с паролями при редактировании/добавлении юзера.
     * Проверка на длину пароля производится в 
     * app/Http/Sections/Users.php
     *
     * Работаем с объектом из vendor/symfony/http-foundation/Request.php
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next, $guard = null)
    {
        // проверяем, что мы имеем дело с редактированием юзеров
        if ($request->route()->parameter('adminModel')) {
            $alias = $request->route()->parameter('adminModel')->getAlias();
        } else {
            return $next($request);
        }

        // убедимся, что получены данные из формы
        $paramAr = $request->request->all();
        // dd(count($paramAr));
        /*array:8 [▼
              "_redirectBack" => "http://croco.lc/admin/users"
              "_token" => "sn0BxLlykTe3Z7uibEd3Tv3ckCon9kKiKlLNgzL4"
              "name" => "user"
              "email" => "a.l.o.o.z.e@gmail.com"
              "password" => "$2y$10$yCmoxP2LBAulM4t0D9absum3ZtWGC57lRGkeRNG1MKB/xzz9CZqG6"
              "password_confirmation" => ""
              "role" => "0"
              "next_action" => "save_and_continue"
            ]*/

        // вариант:
        // $request->request->get('_token') и проверка на null

        if (count($paramAr) > 0 && $alias == 'users') {
            // если из формы не поступила роль, принудительно ставим 0
            // вариант: $paramAr['role'] = intval($paramAr['role'])
            if (!isset($paramAr['role']) || trim($paramAr['role']) == '') {
                $paramAr['role'] = 0;
            }

            // id юзера из request
            $userId = $request->route('adminModelId');

            // запрошенная операция
            $uriStr = $request->route()->uri();
            $uriAr = explode('/', $uriStr);
            $opKey = array_pop($uriAr); // create OR edit

            switch($opKey)
            {
                case 'create':
                    // если у нас добавление юзера, то вписываем хеши от пароля и его подтверждения
                    // проверяем, что у нас в полях паролей не остались хеши от предыдущих неудачных попыток 
                    if (strlen($paramAr['password']) > 50) {
                        // вряд ли это такой длинный пароль, скорее всего в поле уже хеш
                        // ничего не делаем
                    } else {
                        if (trim($paramAr['password']) != '' && strlen($paramAr['password']) > 5 && trim($paramAr['password_confirmation']) == $paramAr['password']) {
                            $paramAr['password'] = bcrypt($paramAr['password']);
                            $paramAr['password_confirmation'] = $paramAr['password'];
                        } else {
                            // пусть админка отдает ошибку
                        }
                    }

                    // и перезаписываем их в запросе
                    $request->request->replace($paramAr);
                break;

                case 'edit':
                    // если у нас редактирование юзера, то действуем в зависимости от заполнения подтверждения пароля
                    if (trim($paramAr['password_confirmation']) == '') {
                        // пароль не хотят перезаписывать
                        $user = User::where('id', $userId)->get();
                        // dd($user->name);
                        if (!$user) {
                            // мало ли что...
                            return response('Not found.', 404);
                        } else {
                            $user = $user[0];
                        }
                        $paramAr['password_confirmation'] = $paramAr['password'] = $user->password;
                    } else {
                        // пароль пытаются обновить
                        if (strlen($paramAr['password']) > 50) {
                            // вряд ли это такой длинный пароль, скорее всего в поле уже хеш
                            // ничего не делаем
                        } else {
                            if (trim($paramAr['password']) != '' && strlen($paramAr['password']) > 5 && trim($paramAr['password_confirmation']) == $paramAr['password']) {
                                $paramAr['password'] = bcrypt($paramAr['password']);
                                $paramAr['password_confirmation'] = $paramAr['password'];
                            } else {
                                // пусть админка отдает ошибку
                            }
                        }                        
                    }

                    // перезаписываем пароли в запросе
                    $request->request->replace($paramAr);
                break;
            }
        }

        return $next($request);
    }
}
