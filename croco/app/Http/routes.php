<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::auth();

// в RouteServiceProvider заданы фильтры для алиасов
Route::get('/{cat}', 'CategoryController@index');
Route::get('/{pseudocat}', 'CategoryController@filter'); //rec|new|popul|rev|like
Route::get('/{page}', 'PageController@index');

// профиль юзера
Route::get('/u/{alias}', 'UserPagesController@index');

// страница темы
Route::get('/v/{alias}', 'ShowArticlesController@index');

// поиск
Route::get('/s/{tag}', 'SearchController@tagSearch');
Route::post('/s/search', 'SearchController@strSearch');

// переключение языков
Route::get('setlang/{lkey}', ['as' => 'setlang', function ($lkey) { 
    $langConfirm = \App\Lang::where('lkey', $lkey)
                        ->where('active', 1)
                        ->get(); 
    if ($langConfirm) {
        Session::put('locale', $lkey);
    }
    return redirect()->back();
}]);

// страница реферрала
Route::get('/r/{alias}', 'ReferralController@saveCookie');

// страницы кабинета пользователя
Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function () {
    Route::get('/', 'UserController@index');

    // редактирование профиля
    Route::get('edit', [
          'as' => 'profile.edit', 'uses' => 'UserController@edit'
        ]);
    Route::post('save', 'UserController@update');

    Route::get('signs', 'UserController@showSigns');

    Route::get('notifies', 'UserController@showNotifies');

    Route::get('settings', 'UserController@showSettings');

    Route::get('bonuses', 'UserController@showBonuses');

    // изменение пароля авторизованным пользователем
    Route::get('changepass', [
        'as' => 'profile.edit', 'uses' => 'UserController@showChangePassForm'
    ]);
    Route::post('changepass', 'UserController@updatePassword');

    Route::post('usermedia', 'UserController@editUsermedia');

    // ответы
    Route::resource('answer', 'AnswerController');

    // добавление комментария
    Route::post('comment', 'CommentController@store');

    // темы пользователя
    Route::resource('articles','ArticlesController');

    // темы пользователя
    Route::resource('lib','FileController');

});

// AJAX
Route::group(['prefix' => 'ajax'], function () {
    Route::get('base','AjaxController@base'); // заглушка, для проверки отклика сервера
    Route::get('status','AjaxController@getStatus');
    Route::get('subscribe','AjaxController@subscribe');
    Route::get('catposts','AjaxController@catPosts');
    Route::get('filesort','AjaxController@fileSort');
    // Route::get('acomments','AjaxController@articleComments');
    Route::get('rating','AjaxController@setRating');
    Route::get('authorsay','AjaxController@authorSay');
    Route::get('setcook','AjaxController@setCookie');
    Route::get('setting','AjaxController@setSetting');
    Route::get('tags','AjaxController@getTags');
    Route::get('whom','AjaxController@whom');
    Route::get('banrm','AjaxController@rmItAndBanUser');
    Route::get('rm','AjaxController@rmIt');
    Route::get('claim','AjaxController@claimIt');
    // tests
    // Route::get('test4','AjaxController@test4');

});

// test
// Route::get('mail', function() {
//     return Mail::send('layouts.mail', ['h1' => 'test mail', 'content' => 'Bla bla bla'], function ($message) {
//         $message->from('robot@commfi.lc', 'Commfi');
//         $message->to('user@example.com');
//         $message->subject('Test with no attach');
//         // $message->attach(public_path('images/logo.jpg'));
//     });
// });
