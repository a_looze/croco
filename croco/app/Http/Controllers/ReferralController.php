<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;

class ReferralController extends Controller
{
    public function saveCookie($alias)
    {

        // если юзер уже авторизован, он в этом месте нам неинтересен
        $userObj = auth()->user();

        if (is_object($userObj)) {
            return redirect('/');
        }

        // если получена реферальная ссылка
        $isRefCookieAlreadySet = $request->cookie('ut_rla') !== null ? true : false;

        // не даем перезаписывать реф. куку 
        if (!$isRefCookieAlreadySet && $alias !== null) {
            // реферальная кука еще не установлена и пользователь пришел по реферальной ссылке
            Cookie::queue('ut_rla', $alias, 60000);
        }
        return redirect('/');
    }

}
