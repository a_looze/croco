<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Subscribe;
use App\Article;
use App\Http\Requests;

class UserPagesController extends Controller
{
    private $onPage = 50;

    public function index($alias)
    {
        $channelOwner = User::whereAlias($alias)
                        ->where('isblocked', 0)
                        ->first();
        // dd($alias);      

        if (!is_object($channelOwner)) {
            return response()->view('errors.404', [], 404);
        }

        $visitor = auth()->user();

        // состояние подписки юзера
        $userSubs = false;
        $showSubs = true;
        if (is_object($visitor)) {
            $subsObjAr = Subscribe::where('channel_id', $channelOwner->id)
                                ->where('user_id', $visitor->id)
                                ->where('type', 'channel')
                                ->get();
            if (count($subsObjAr) == 1) {            
                $userSubs = true;
            }

            if ($visitor->id === $channelOwner->id) {
                $showSubs = false;
            } else {
                $showSubs = true;
            }
        }
        

        // общее кол-во подписок
        $subscribers = Subscribe::with(['owner'])
                                ->where('channel_id', $channelOwner->id)
                                ->where('type', 'channel')
                                ->get();
        $subsCount = count($subscribers);

        // вопросы юзера
        $articles = Article::with(['preview', 'attach',
                                    'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                     ])
                            ->where('user_id', $channelOwner->id)
                            ->where('visible', 1)
                            ->orderby('created_at', 'desc')
                            ->simplePaginate($this->onPage);
        $hasMorePages = $articles->hasMorePages();

        return view('channel.channel', ['channelOwner' => $channelOwner,
                                        'userSubs' => $userSubs,
                                        'showSubs' => $showSubs,
                                        'subsCount' => $subsCount,
                                        'subscribers' => $subscribers,
                                        'articles' => $articles,
                                        'hasMorePages' => $hasMorePages,
                                        ]);
    }


    public function profile(Request $request, $alias)
    {
        //$userObj = User::where('alias', $alias)->SubscribedOnUser()->where('type', 'channel')->get();
        //$userObj = User::find(4)->SubscribedOnUser()->where('type', 'channel')->get();
        $userObj = User::with(
            [
                'SubscribedOnUser' => function($q) {
                    $q->where(['type' => 'channel']);
                },
            ]
        )->where(['alias' => $alias])->first();

        $subs = count($userObj->SubscribedOnUser);

        return view('channel.user_channel', [
            'user' => $userObj,
            'subs' => $subs,
        ]);
    }

}
