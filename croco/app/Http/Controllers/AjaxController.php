<?php

namespace App\Http\Controllers;


use App\User;
use App\Category;
use App\Setting;
use App\Subscribe;
use App\Tag;
use App\Article;
use App\Answer;
use App\Comment;
use App\UserLike;
use App\Events\LogUserActivity;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helpers\Scenario;
use App\Helpers\FileHelper as FileService;
use Cookie;
use Session;
use View;
use DB;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class AjaxController extends Controller
{
    private $user;

    private $param;

    private $onPage = 50;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $script = request()->server->get('SCRIPT_FILENAME');
        $isCli = $script === 'artisan' || $script === './artisan';
        // if (app()->runningInConsole() === false) {
        // if (!$request->ajax() && !$isCli = $request->server->get('SCRIPT_FILENAME') === 'artisan') {

        
        if (!$request->ajax() && !$isCli) {
            // убрать после тестов
            // return response()->view('errors.missing', [], 404);
            // return response()->json(['error' => 'Not Found'], 404);
            abort(404, 'The AJAX resource you are looking for could not be found');
        } 

        $this->param = [
            'id' => false,
            'name' => '',
            'mess' => [
                'success' => '',
                'warn' => '',
                'error' => '',
                ],
            'status' => 'OK',
            'body' => '',
            'redirect' => '',
            ];
        $userOb = auth()->user();
        if (is_object($userOb)) {
            $this->user = $userOb;
            $this->param['id'] = $this->user->id;
            $this->param['name'] = $this->user->name;
            $this->param['mess']['success'] = 'User#' . $this->user->id;
        } else {
            $this->param['mess']['warn'] = 'No user';
            $this->user = false;     
        }
    }

    /**
     * Получаем текущий статус юзера, его сообщения и пр.
     */
    public function getStatus(Request $request)
    {
        return response()->json($this->param);
    }

    /**
     * Ping
     */
    public function base(Request $request)
    {
        return response()->json($this->param);
    }

    /**
     * Подписка на канал/отписка
     */
    public function subscribe(Scenario $scenario)
    {
        // если не авторизован, то редирект на логин
        if (!$this->param['id']) {
            $this->param['redirect'] = url('login');

            return response()->json($this->param);

        } else {
            $subsType = request()->input('type');
            switch ($subsType) {
                case 'channel':
                case 'category':
                    $subsData = $scenario->makeSubscribe($subsType);
                break;

                default:
                    $subsData = false;
                break;
            }
        }

        if ($subsData === false) {
            $this->param['mess']['error'] = 'No category';
            $this->param['status'] = 'Bad';
        } elseif (in_array($subsData['event_id'], [3,5])) {
            $this->param['mess']['success'] = 'Subscribe OK';
            $this->param['body']['button'] = trans('main.unsubscribe') . ' -';
        } elseif (in_array($subsData['event_id'], [4,6])) {
            $this->param['mess']['success'] = 'Unsubscribe OK';
            $this->param['body']['button'] = trans('main.subscribe') . ' +';
        }
        // обновляем общее кол-во подписок
        $subsCount = $scenario->getSubsCount($subsType);
        $this->param['body']['text'] = $subsCount . '<p>' . trans_choice('main.subscribers', $subsCount) . '</p>';

        return response()->json($this->param);
    }

    /**
     * Бесконечная подгрузка постов в категории + сортировка
     */
    public function catPosts(Request $request)
    {
        // запрос приходит с параметрами  page: 2, sort: "d", asc: false, from: "/draw"

        $catAlias = trim($request->input('from'), '/');
        $reqPage = intval($request->input('page'));
        $reqSort = $request->input('sort');
        $reqAsc = $request->input('asc');

        // уточняем корректность параметров
        if ($reqPage < 1) {
            // запрошена некорректная страница, редирект на главную
            $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
            $this->param['mess']['success'] = '';
            $this->param['status'] = 'Bad';
            $this->param['redirect'] = '/';
            return response()->json($this->param);
        }

        // сортировка
        switch ($reqSort) {
            case 'p': $orderby = 'rating'; break; // по лайкам
            // case 't': $orderby = 'created_at'; break; // по типу
            // case 'a': $orderby = 'created_at'; break; // по наличию ответов
            default: $orderby = 'created_at'; break; // по дате
        }

        if ($reqAsc === 'false') {
            $dir = 'desc';
        } else {
            $dir = 'asc';
        }

        // получаем категорию, из которой надо подгрузить страницу
        $cat = Category::where('active', 1)
                    ->where('visible', 1)
                    ->where('alias', $catAlias)
                    ->first();
        if ($cat !== null) {
            /**************************************************************
                Реальные категории
            ***************************************************************/

            // для сортировки по наличию ответов делаем уточняющие запросы
            if ($reqSort == 'a') {
                $q = "SELECT a.id, count(answ.id) as acnt 
                        FROM articles a 
                        LEFT JOIN answers answ 
                        ON answ.article_id=a.id AND answ.author_say < 3
                        WHERE a.visible=1 AND a.category_id=" . $cat->id . "
                        GROUP BY a.id
                        ORDER BY acnt " . $dir . ", a.created_at desc";

                $res = DB::select($q);
                // dd($res);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad cat page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                // $pages = new Paginator($tmpAr, count($tmpAr), $this->onPage, $reqPage);
                // $this->param['body']['next'] = $pages->hasMorePages();

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else if ($reqSort == 't') {
                $q = "SELECT a.id
                        FROM articles a 
                        LEFT JOIN files f 
                        ON f.id=a.file_id 
                        WHERE a.visible=1 AND a.category_id=" . $cat->id . "
                        ORDER BY f.type_id " . $dir . ", a.created_at desc";

                $res = DB::select($q);
                // dd($res);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad cat page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                // $pages = new Paginator($tmpAr, count($tmpAr), $this->onPage, $reqPage);
                // $this->param['body']['next'] = $pages->hasMorePages();

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else {
                // выборка вопросов из запрошенной категории
                $articles = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->where('category_id', $cat->id)
                                    ->where('visible', 1)
                                    ->orderby($orderby, $dir)
                                    ->simplePaginate($this->onPage);
            }

            if (count($articles) < 1) {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => $articles->hasMorePages(),
                                            ])
                                            ->render();
            $this->param['body']['next'] = $articles->hasMorePages();

            return response()->json($this->param);
        } else if (in_array($catAlias, ['rec', 'new', 'popul', 'rev', 'like'])) {
            /**************************************************************
                Псевдо категории
            ***************************************************************/
                
            // выборка вопросов из запрошенной псевдокатегории
            // это реально жесть по размерам кода, но деваться некуда

            // вначале получаем список вопросов в нужной псевдокатегории
            switch ($catAlias) {
                case 'like':
                    // вопросы, которые юзер лайкал
                    if (!is_object($userObj)) {
                        $articlesRaw = [];
                    } else {
                        $ulCollection = UserLike::where('user_id', $userObj->id)
                                                ->where('target', 'article')
                                                ->get();

                        foreach ($ulCollection as $data) {
                            $tmpAr[$data->item_id] = $data->item_id;
                        }

                        if (count($tmpAr) < 1) {
                            $articlesRaw = [];
                        } else {
                            $articlesRaw = Article::whereIn('id', $tmpAr)
                                                ->get();
                        }
                    }
                break;

                case 'rev':
                    // недавно просмотренные
                    $vList = request()->cookie('ut_vql');
                    if (!$vList || trim($vList) == '') {
                        $articlesRaw = [];
                    } else {
                        $vAr = explode('|', $vList);
                        $articlesRaw = Article::wherein('id', $vAr)->get();
                    }
                break;

                case 'popul':
                    // набирающие популярность
                    // пока тупо делаем выборку из самых популярных - нет алгоритма
                    $articlesRaw = Article::where('rating', '>', 10)->get();
                break;

                case 'new':
                    // устанавливаем новинками темы за последний месяц
                    $dateLast = Carbon::now()->subMonth()->toDateTimeString();
                    $articlesRaw = Article::where('created_at', '>', $dateLast)->get();
                break;

                case 'rec':
                    // вообще непонятно что выдавать в рекомендациях
                    // $articlesRaw = [];
                    
                    // список статей
                    $articlesRaw = Article::where('rating', '>', 10) // пусть хоть так
                                ->get();
                break;
            }

            if (count($articlesRaw) < 1) {
                $this->param['mess']['error'] = 'Bad pseudocat page sort';
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            } 

            $articlesIdsAr = $articlesRaw->map(function($item, $key) {
                return $item->id;
            });

            $idsStr = implode(',', $articlesIdsAr->toArray());

            // для сортировки по наличию ответов делаем уточняющие запросы
            if ($reqSort == 'a') {
                $q = "SELECT a.id, count(answ.id) as acnt 
                        FROM articles a 
                        LEFT JOIN answers answ 
                        ON answ.article_id=a.id AND answ.author_say < 3
                        WHERE a.visible=1 AND a.id IN (" . $idsStr . ")
                        GROUP BY a.id
                        ORDER BY acnt " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad pseudocat answers page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else if ($reqSort == 't') {
                $q = "SELECT a.id
                        FROM articles a 
                        LEFT JOIN files f 
                        ON f.id=a.file_id 
                        WHERE a.visible=1 AND a.id IN (" . $idsStr . ")
                        ORDER BY f.type_id " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad pseudocat types page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else {
                // выборка вопросов из запрошенной категории
                $articles = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $articlesIdsAr->toArray())
                                    ->where('visible', 1)
                                    ->orderby($orderby, $dir)
                                    ->simplePaginate($this->onPage);
            }

            if (count($articles) < 1) {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No page exists: ' . $reqPage;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => $articles->hasMorePages(),
                                            ])
                                            ->render();
            $this->param['body']['next'] = $articles->hasMorePages();

            return response()->json($this->param);
        } else if (preg_match('~u/(.*)?~', $catAlias, $match)) {
            /**************************************************************
                Страница одного канала
            ***************************************************************/
                
            // выборка вопросов в одном канале (один автор)

            if (!isset($match[1]) || $match[1] == '') {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No channel page exists: ' . $catAlias;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $channelOwner = User::whereAlias($match[1])
                                ->where('isblocked', 0)
                                ->first();
            if (!$channelOwner) {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No user channel exists: ' . $catAlias;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            // для сортировки по наличию ответов делаем уточняющие запросы
            if ($reqSort == 'a') {
                $q = "SELECT a.id, count(answ.id) as acnt 
                        FROM articles a 
                        LEFT JOIN answers answ 
                        ON answ.article_id=a.id AND answ.author_say < 3
                        WHERE a.visible=1 AND a.user_id=" . $channelOwner->id . "
                        GROUP BY a.id
                        ORDER BY acnt " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad channel answers page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No channel answers page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else if ($reqSort == 't') {
                $q = "SELECT a.id
                        FROM articles a 
                        LEFT JOIN files f 
                        ON f.id=a.file_id 
                        WHERE a.visible=1 AND a.user_id=" . $channelOwner->id . "
                        ORDER BY f.type_id " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad channel answers page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No channel answers page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else {
                // выборка вопросов из запрошенной категории
                $articles = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->where('user_id', $channelOwner->id)
                                    ->where('visible', 1)
                                    ->orderby($orderby, $dir)
                                    ->simplePaginate($this->onPage);
            }

            if (count($articles) < 1) {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No channel answers page exists: ' . $reqPage;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => $articles->hasMorePages(),
                                            ])
                                            ->render();
            $this->param['body']['next'] = $articles->hasMorePages();

            return response()->json($this->param);
        } else if (preg_match('~s/search/(.*)?~', $catAlias, $match)) {
            /**************************************************************
                Страница с результатами поиска по строке
            ***************************************************************/

            // выборка вопросов
            if (!isset($match[1]) || $match[1] == '') {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No search page exists: ' . $catAlias;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            // получаем список вопросов по данному условию
            $articlesRaw = Article::where('title', 'like', '%' . $match[1] . '%')
                                    ->where('visible', 1)
                                    ->get();

            if (count($articlesRaw) < 1) {
                $this->param['mess']['error'] = 'Bad search page sort';
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $articlesIdsAr = $articlesRaw->map(function($item, $key) {
                return $item->id;
            });

            $idsStr = implode(',', $articlesIdsAr->toArray());

            // для сортировки по наличию ответов делаем уточняющие запросы
            if ($reqSort == 'a') {
                $q = "SELECT a.id, count(answ.id) as acnt 
                        FROM articles a 
                        LEFT JOIN answers answ 
                        ON answ.article_id=a.id AND answ.author_say < 3
                        WHERE a.visible=1 AND a.id IN (" . $idsStr . ")
                        GROUP BY a.id
                        ORDER BY acnt " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad search page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No search page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else if ($reqSort == 't') {
                $q = "SELECT a.id
                        FROM articles a 
                        LEFT JOIN files f 
                        ON f.id=a.file_id 
                        WHERE a.visible=1 AND a.id IN (" . $idsStr . ")
                        ORDER BY f.type_id " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad search page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No search page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else {
                // выборка вопросов из запрошенной категории
                $articles = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $articlesIdsAr->toArray())
                                    ->where('visible', 1)
                                    ->orderby($orderby, $dir)
                                    ->simplePaginate($this->onPage);
            }

            if (count($articles) < 1) {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No search sort page exists: ' . $reqPage;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => $articles->hasMorePages(),
                                            ])
                                            ->render();
            $this->param['body']['next'] = $articles->hasMorePages();

            return response()->json($this->param);
        } else if (preg_match('~s/(.*)?~', $catAlias, $match)) {
            /**************************************************************
                Страница с результатами поиска по тегу
            ***************************************************************/
                
            // выборка вопросов

            if (!isset($match[1]) || $match[1] == '') {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No tag page exists: ' . $catAlias;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            // получаем список вопросов по данному тегу
            $tagObj = Tag::whereTitle($match[1])->first();
            $articlesRaw = $tagObj->articles()->get();

            if (count($articlesRaw) < 1) {
                $this->param['mess']['error'] = 'Bad tag page sort';
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $articlesIdsAr = $articlesRaw->map(function($item, $key) {
                return $item->id;
            });

            $idsStr = implode(',', $articlesIdsAr->toArray());

            // для сортировки по наличию ответов делаем уточняющие запросы
            if ($reqSort == 'a') {
                $q = "SELECT a.id, count(answ.id) as acnt 
                        FROM articles a 
                        LEFT JOIN answers answ 
                        ON answ.article_id=a.id AND answ.author_say < 3
                        WHERE a.visible=1 AND a.id IN (" . $idsStr . ")
                        GROUP BY a.id
                        ORDER BY acnt " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad tag page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No tag page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else if ($reqSort == 't') {
                $q = "SELECT a.id
                        FROM articles a 
                        LEFT JOIN files f 
                        ON f.id=a.file_id 
                        WHERE a.visible=1 AND a.id IN (" . $idsStr . ")
                        ORDER BY f.type_id " . $dir . ", a.created_at desc";

                $res = DB::select($q);

                if (count($res) < 1) {
                    // что-то пошло не так
                    $this->param['mess']['error'] = 'Bad tag page sort';
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }                

                foreach ($res as $key => $data) {
                    // задаем порядок вывода...
                    $tmpAr[$data->id] = $data->id;
                }

                $articlesPre = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $tmpAr)
                                    ->get();

                // ...и размещаем объекты в массив по заданному порядку
                foreach ($articlesPre as $articleObj) {
                    if (isset($tmpAr[$articleObj->id])) {
                        $tmpAr[$articleObj->id] = $articleObj;
                    }
                }

                // имитируем пагинацию
                $chunks = collect($tmpAr)->chunk($this->onPage);
                $articles = $chunks[$reqPage-1];

                if (count($articles) < 1) {
                    // запрошена некорректная страница, редирект на главную
                    $this->param['mess']['error'] = 'No tag page exists: ' . $reqPage;
                    $this->param['mess']['success'] = '';
                    $this->param['status'] = 'Bad';
                    $this->param['redirect'] = '/';
                    return response()->json($this->param);
                }

                $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => isset($chunks[$reqPage]),
                                            ])
                                            ->render();
                $this->param['body']['next'] = isset($chunks[$reqPage]);

                return response()->json($this->param);
            } else {
                // выборка вопросов из запрошенной категории
                $articles = Article::with(['preview', 
                                             'answers' => function($q) {
                                                $q->where('author_say', '<', 3);
                                             },
                                             'author'
                                             ])
                                    ->whereIn('id', $articlesIdsAr->toArray())
                                    ->where('visible', 1)
                                    ->orderby($orderby, $dir)
                                    ->simplePaginate($this->onPage);
            }

            if (count($articles) < 1) {
                // запрошена некорректная страница, редирект на главную
                $this->param['mess']['error'] = 'No tag sort page exists: ' . $reqPage;
                $this->param['mess']['success'] = '';
                $this->param['status'] = 'Bad';
                $this->param['redirect'] = '/';
                return response()->json($this->param);
            }

            $this->param['body']['code'] = view('ajax.category', 
                                            ['articles' => $articles,
                                            'page' => $reqPage,
                                            'hasMorePages' => $articles->hasMorePages(),
                                            ])
                                            ->render();
            $this->param['body']['next'] = $articles->hasMorePages();

            return response()->json($this->param);
        } else {
            /**************************************************************
                Категория не найдена, выборку делать не из чего
            ***************************************************************/
                
            $this->param['mess']['error'] = 'No category found by alias ' . $catAlias;
            $this->param['mess']['success'] = '';
            $this->param['status'] = 'Bad';
            return response()->json($this->param);
        }
    }

    /**
     * Обработка лайков и дизлайков
     */
    public function setRating(Scenario $scenario)
    {
        if (!$this->param['id']) {
            $this->param['redirect'] = url('login');
            return response()->json($this->param);
        }
        $target = request()->input('target');
        $iid = intval(request()->input('iid'));

        $collection = false;

        switch ($target) {
            case 'article':
                $collection = Article::where('id', $iid)->get();
                break;
            case 'answer':
                $collection = Answer::where('id', $iid)->get();
                break;
            case 'comment':
                $collection = Comment::where('id', $iid)->get();
                break;
        }

        if (!$collection || count($collection) === 0) {
            // $this->param['mess']['success'] = '';
            $this->param['mess']['error'] = 'No rating target';
            $this->param['status'] = 'Bad';
        } else {
            $item = $collection[0];

            // проверяем, отмечал ли текущий юзер эту фигню лайком
            $ulCollection = UserLike::where('user_id', $this->param['id'])
                                    ->where('target', $target)
                                    ->where('item_id', $iid)
                                    ->get();
            if (count($ulCollection) < 1) {
                // нет записей, добавляем лайк
                $userLike = UserLike::create();
                $userLike->user_id = $this->param['id'];
                $userLike->target = $target;
                $userLike->item_id = $iid;
                $userLike->save();

                $item->rating = $item->rating + 1;
                $item->save();
                $act = 'inc';
            } else {
                $userLike = $ulCollection[0];
                $userLike->delete();

                $item->rating = $item->rating - 1;
                $item->save();
                $act = 'dec';
            }

            $this->param['body'] = $item->rating;

            // вызываем событие для записи лайка/дизлайка
            // $scenario->changeRating($this->user, $target, $iid, $act);
            $scenario->changeRating($target, $iid, $act);

        }

        return response()->json($this->param);
    }

    /**
     * Авторская оценка ответа
     */
    public function authorSay(Scenario $scenario)
    {
        $target = request()->input('target');
        $what = request()->input('what');
        $articleAlias = str_replace('/v/', '', request()->input('from'));

        $article = Article::where('alias', $articleAlias)->first();
        if ($article->author->id != $this->user->id) {
            // чужой вопрос
            $this->param['mess']['error'] = 'Alien article';
            $this->param['status'] = 'Bad';
        } else {
            $answer = Answer::where('id', $target)->first();
            if ($answer->article->id != $article->id) {
                // ответ из другой темы
                $this->param['mess']['error'] = 'Bad article';
                $this->param['status'] = 'Bad';
            } else {
                if ($what != '0' && $what != '1') {
                    $this->param['mess']['error'] = 'Bad resolution';
                    $this->param['status'] = 'Bad';
                } else {
                    // вызываем событие для записи оценки автора
                    $scenario->changeAnswerResolution($answer, $what);

                    if ($what == '0') {
                        $answer->author_say = 2;                        
                        $this->param['body'] = '<img src="images/icon/los.png">';
                    } else {
                        $answer->author_say = 1;
                        $this->param['body'] = '<img src="images/icon/win.png">';
                    }
                    $answer->save();
                }
            }
        }

        return response()->json($this->param);
    }

    /**
     * Установка куки для просмотреного ответа или прозрачности
     */
    public function setCookie(Request $request)
    {
        $type = request()->input('type');

        $articleAlias = trim($request->input('from'), '/'); // v/529502ca241ac
        list($v, $articleAlias) = explode('/', $articleAlias);

        switch ($type) {
            case 'aaview':
                // подсмотренный ответ
                $article = Article::where('alias', $articleAlias)->first();
                if (is_object($article)) {
                    $currentVal = request()->cookie('ut_aav'); 
                    if (!$currentVal || trim($currentVal) == '') {
                        $curAr = [ $article->id ];
                    } else {
                        foreach (explode('|', $currentVal) as $val) {
                            $curAr[$val] = $val;
                        }
                        $curAr[$article->id] = $article->id;
                    }
                    $newVal = implode('|', $curAr);
                    Cookie::queue('ut_aav', $newVal);
                }
            break;

            case 'covis':
                // прозрачность правой колонки
                // 1 - div с классом visActive, 0 - без него
                $currentVal = request()->cookie('ut_sv');

                if (!isset($currentVal) || $currentVal == '1') {
                    // response()->withCookie(cookie()->forever('ut_sv', '0'));
                    Cookie::queue('ut_sv', '0');
                    // $this->param['body'] = '0';
                } else {
                    // response()->withCookie(cookie()->forever('ut_sv', '1'));
                    Cookie::queue('ut_sv', '1');
                    // $this->param['body'] = '1';
                }
            break;
        }
        return response()->json($this->param);
    }

    /**
     * Сортировка файлов
     *
     * @param Request $request
     */
    public function fileSort(Request $request, FileService $fileService)
    {
        $this->validate($request, [
            'sort' => 'required|string',
            'asc' => 'required',
        ]);

        $sortBy = $request->get('sort');
        $orderBy = $request->get('asc');

        //dd($orderBy);

        if ($orderBy === 'false') {
            $dir = 'desc';
        } else {
            $dir = 'asc';
        }

        //dd($dir);

        $files = $fileService->getUserLibAjax($sortBy, $dir);

        $this->param['body']['code'] = view('ajax.files',
            [
                'files' => $files,
            ])
            ->render();

        return response()->json($this->param);
    }

    /**
     * Возвращаем имя пользователя для которого задается вопрос
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function whom(Request $request, Scenario $scenario)
    {
        $user = $this->findUserByAlias($request->get('url'));
        if (!$user) {
            $this->param['body']['code'] = trans('main.no_user');
        } else {
            $this->param['status'] = 201;
            $this->param['user_id'] = $user->id;
            $this->param['body']['code'] = '@'.$user->name;
        }
        return response()->json($this->param);
    }

    /**
     * Бан статьи, ответа, комментария
     */
    public function rmItAndBanUser(Scenario $scenario)
    {
        $user = auth()->user();
        if (!$user->isManager()) {
            $this->param['status'] = 'bad';
            return response()->json($this->param);
        }

        $res = $this->rmObj();
        if (!$res) {
            $this->param['status'] = 'bad';
        } else {
            $res->isblocked = 1;
            $res->blocked_at = time();
            $res->save();
            $this->param['body'] = 'Removed&banned';

            // вызываем событие для записи 
            $scenario->rmObj($res);
            $scenario->banUser($res);
        }

        
        return response()->json($this->param);
    }

    /**
     * Удаление статьи, ответа, комментария
     */
    public function rmIt(Scenario $scenario)
    {
        $user = auth()->user();
        if (!$user->isManager()) {
            $this->param['status'] = 'bad';
            $this->param['body'] = 'Bad user';
            return response()->json($this->param);
        }

        $res = $this->rmObj();
        if (!$res) {
            $this->param['status'] = 'bad';
        } else {
            $this->param['body'] = 'Removed';

            // вызываем событие для записи 
            $scenario->rmObj($res);
        }

        return response()->json($this->param);
    }

    /**
     * Удаление статьи, ответа, комментария
     */
    public function rmObj() 
    {
        $target = request()->input('target');
        $iid = request()->input('iid');

        switch ($target) {
            case 'article':
                $someObj = Article::where('id', $iid)->first();
            break;
            
            case 'answer':
                $someObj = Answer::where('id', $iid)->first();
            break;
            
            case 'comment':
                $someObj = Comment::where('id', $iid)->first();
            break;
            
            default:
                return false;
            break;
        }

        
        if (!is_object($someObj)) {
            $this->param['body'] = 'Can\'t rm';
            return false;
        } else {
            $user = $someObj->author()->first();
            if (!$user) {
                $this->param['body'] = 'Bad author';
            }
            $someObj->delete();
            return $user;
        }
    }

    /**
     * Жалоба на статью, ответ, комментарий
     */
    public function claimIt()
    {
        return response()->json($this->param);
    }


    public function index(Request $request)
    {
        return response()->json($this->param);
    }

    /**
     * Тесты
     */
    public function test1(Request $request)
    {
        $this->param['body'] = time();
        return response()->json($this->param);
    }
    public function test4(Request $request)
    {
        $this->param['body'] = time();
        return response()->json($this->param);
    }

    /**
     * Подгружаем теги
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTags() {
        $arr = [];
        $tags = Tag::get();
        foreach($tags as $tag) {
            $arr[] = $tag->title;
        }
        /*$json = file_get_contents(public_path('assets/citynames.json'));
        return $json;*/
        return response()->json($arr);
    }

    public function setSetting()
    {
        $userId = $this->param['id'];
        $data = [
            request('target') => request('value'),
        ];
        User::where('id', $userId)->update($data);

        return response()->json($this->param);
    }

    /**
     * Возвращает имя пользователя по алиасу
     *
     * @param $str
     * @return bool
     */
    public function findUserByAlias($str)
    {
        $strArr = explode('/', $str);
        $alias = $strArr[count($strArr)-1];
        $userObj = User::where(['alias' => $alias])->first();

        if($userObj) {
            return $userObj;
        }

        return false;
    }

}
