<?php

namespace App\Http\Controllers;


use App\User;
use App\Article;
use App\ArticleTag;
use App\Answer;
use App\Category;
use App\File;
use App\Http\Requests;
use App\Tag;
use App\Helpers\Scenario;
use App\Helpers\FileHelper as FileService;
use App\Helpers\FileStore;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;

class ArticlesController extends Controller
{

    public $fileService;

    public $files;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FileService $fileService)
    {
        $this->middleware('auth');

        $this->fileService = $fileService;

        $userId = Auth::user()->id;

        $userObj = User::with(
            [
                'SubscribedOnUser' => function($q) {
                    $q->where(['type' => 'channel']);
                },
            ]
        )->where(['id' => $userId])->first();

        // количество подписок на канал юзера
        $this->subs = count($userObj->SubscribedOnUser);
        view()->share('subsCount', $this->subs);

        // библиотеке юзера
        $this->files = $this->fileService->getUserLib();
        view()->share('files', $this->files);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $articles = Article::with('preview')
                            ->where(['user_id' => $userId])
                            ->orderby('created_at', 'desc')
                            ->get();

        // уточняем наличие "неотвеченных" ответов
        // и несуществующих файлов
        $articles->transform(function($article, $key) {
            // ответы
            $tmp = $article->answers()->where('author_say', 0)->get();
            if (count($tmp) > 0) {
                $article->alert = true;
            } else {
                $article->alert = false;
            }

            // файлы
            $file = File::where('id', $article->file_id)
                                ->first();

            $path = public_path('assets/media/users/') . $article->user_id . '/'. $file->type_id . '/';
            if (!$this->checkIsFileExists($file->alias, $file->type_id, $path)) {
                // ломаем через колено
                $article->link = false;
            } else {
                $article->link = true;
            }
            return $article;
        });

        return view('profile.articles.index',
                        [
                            'articles' => $articles,
                        ]
                    );
    }

    private function checkIsFileExists($alias, $type, $path/*=''*/)
    {
        // if ($path == '') {
        //     // $path = public_path() . '/files/' . $type . '/';
        //     $path = public_path('assets/media/users/' . $user->id . '/' . $type . '/');
        // }

        if (!is_dir($path)) {
            return false;
        }

        switch ($type) {
            case '1':
                // print_r($path . $alias . '.mp4<br>');
                if (!file_exists($path . $alias . '.mp4')
                    || !file_exists($path . $alias . '.ogv')
                    || !file_exists($path . $alias . '.webm')
                    )
                {
                    return false;
                }

            break;

            case '2':
                if (!file_exists($path . $alias . '.mp3')
                    || !file_exists($path . $alias . '.oga')
                    )
                {
                    return false;
                }
            break;

            case '3':
                if (!file_exists($path . $alias . '.jpg')) 
                {
                    return false;
                }
            break;
        }

        return true;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('profile.articles.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Scenario $scenario, FileStore $store)
    {

        $userId = Auth::user()->id;

        $this->validate($request, [
            'file' => 'required_without:file_from_lib|mimes:jpeg,png,jpg,gif,mp3,mpga,mp4,wma,wmv,webm,avi,ogg,oga|max:19456',
            'file_from_lib' => 'integer',
        ]);

        $file_id = '';
        $preview_file_id = '';
        // Если файл из библиотеки
        if (!empty($request->get('file_from_lib'))) {
            $file_id = intval($request->get('file_from_lib'));
            
            // берем привью из библиотеки
            $preview_file_id = File::find($file_id)->preview_id;
        }

        // Если файл был загружен
        if (empty($file_id)) {

            $file = $request->file('file');

            $fileExt = $file->getClientOriginalExtension();

            $typeId = $this->fileService->getTypeByExtension($fileExt);

            // проверкая файлов на сервер
            if($typeId === false) {
                return  response()->json(
                    [
                        'status' => 'error',
                        'error' => trans('main.file_type_error'),
                        'code' => 422
                    ]
                );
            }

            // СОХРАНЯЕМ ФАЙЛ И ПРИВЬЮ ДЛЯ ФРОНТА И БИБЛИОТЕКИ
            $data = [
                'user_id' => $userId,
                'type_id' => $typeId,
            ];

            // создаем записи в бд
            // возвращает массив объектов файлов
            $files = $store->storeFileWithPreview($data);

            // отправляем файл на конвертацию
            if ($typeId == 3) {

                $data = [
                    'real_path' => $file->getRealPath(),
                    'user_id' => $userId,
                    'file_id' => $files['file']->id,
                ];

                $store->moveImage($data);

            } else {
                $file->move(storage_path('videoin'),
                    $files['file']->id.'.'.$file->getClientOriginalExtension());
            }

            $file_id = $files['file']->id;

            $preview_file_id = $files['previewFront']->id;

        }

        // ищем юзера, которому задали вопрос
        $forUser = trim(request()->input('whom'));
        $userForObj = false;
            
        if ($forUser != '') {
            // тут может быть либо ID, либо ссылка на профиль, либо alias, либо вообще юзернейм
            if (intval($forUser) > 0) {
                $userForObj = User::where('id', $forUser)->first();
            } else if (strpos($forUser, '/u/') !== false) {
                // возможно, мы имеем дело с http ссылкой
                list($nn, $uAlias) = explode('/u/', $forUser);
                $uAlias = trim($uAlias, '/');
                $userForObj = User::where('alias', $uAlias)->first();
            } else {
                // у нас либо алиас, либо юзернейм
                $userForObj = User::where('alias', $forUser)->first();
                if (!$userForObj) {
                    $userForObj = User::where('name', $forUser)->first();
                }
            }
        }

        // dd($userForObj);

        if (is_object($userForObj)) {
            $forUserId = $userForObj->id;
        } else {
            $forUserId = 0;
        }

        // СОХРАНЯЕМ ВОПРОС
        $article = new Article();
        $data = [
            'user_id' => $userId,
            'alias' => uniqid(),
            'file_id' => $file_id,
            'preview_file_id' => $preview_file_id,
            'category_id' => $request->get('category_id'),
            'for_user_id' => $forUserId,
        ];
        $obj = $article->create($data);

        // Добавляем бонус и регистрируем событие
        $scenario->addBonusForArticle($obj);

        // // Уведомляем юзера, если ему задали вопрос
        if (is_object($userForObj)) {
            $scenario->userGotPersonalAsk($userForObj, $obj);
        }

        // if ($request->get('whom') != '') {
        //     $ph = [
        //         'ph' => [
        //             'user' => Auth::user()->id,
        //             'article' => $obj->id,
        //         ],
        //         'msg' => 'main.notice.19',
        //     ];

        //     $scenario->noticeUser(intval($request->get('whom')), 19, json_encode($ph));
        // }

        return response()->json(
            [
                'status' => 'Dropzone.ADDED',
                'code' => 201,
                'id' => $obj->id,
                'action' => action('ArticlesController@edit', ['id '=> $obj->id]),
            ]
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $article = Article::find($id);

        $category = Category::find($article->category_id);

        return view('profile.articles.article', [
            'article' => $article,
            'category' => $category,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $userId = Auth::user()->id;
        $categories = Category::all();

        $article = Article::with('preview')->where(['id' => $id])->first();
        $articleFile = File::find($article->file_id);

        $articleFile->path = $this->fileService->getFile($userId, $articleFile->type_id, $articleFile->alias);

        // Превью
        if(!is_null($article->preview)) {
            $image = $this->fileService->getImage($userId, 3, $article->preview->alias);
            if ($image) {
                $article->previewPath = $image;
            }
        }

        if (is_null($article->previewPath)) {
            if ($articleFile->type_id == 3) {
                $article->previewPath = $articleFile->path;
            } else {
                $type = $articleFile->type_id == 1 ? 'video' : 'audio';
                $article->previewPath = config('filesystems.default_path.'.$type);
            }
        }



        $tags = ArticleTag::with('tag')
            ->where('article_id', $article->id)
            ->get()->toArray();

        $tagArr = [];
        foreach($tags as $tagData) {
            $tagArr[] = $tagData['tag']['title'];
        }

        $tagsString = implode(',', $tagArr);

        // Ответ автора
        $answer = Answer::where(['user_id' => $userId, 'article_id' => $article->id])->first();
        if (!is_null($answer)) {
            $answer->action = action('AnswerController@edit', ['id' => $answer->id]);
        }

        return view('profile.articles.edit', [
            'article' => $article,
            'categories' => $categories,
            'tagsString' => $tagsString,
            'answer' => $answer,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $userId = Auth::user()->id;

        $this->validate($request, [
            'file' => 'mimes:jpeg,png,jpg,gif|max:19456',
            //'file_from_lib' => 'integer',
            'title' => 'string',
            'visible' => 'integer',
        ]);

        $file_id = '';

        // Превью из библиотеки
        if (!empty($request->get('file_from_lib'))) {
            $file_id = intval($request->get('file_from_lib'));
        }

        // Добавляем привью
        if ($request->hasFile('file') && empty($file_id)) {

            $preview = $request->file('file');
            $alias = time().'-f';

            $path = public_path('assets/media/users').'/'.$userId.'/3/'.$alias.'.jpg';

            $image = Image::make($preview->getRealPath())->encode('jpg');
            $image->save($path);

            // СОХРАНЯЕМ ФАЙЛ
            $file = new File();
            $data = [
                'user_id' => $userId,
                'alias' => $alias,
                'type_id' => 3,
            ];
            $fileObj = $file->create($data);

            $file_id = $fileObj->id;

        }

        // Обновляеям вопрос
        $article = Article::find($id);
        $data = [
            'preview_file_id' => !empty($file_id) ? $file_id : $article->preview_file_id,
            'title' => !empty($request->get('title')) ? $request->get('title') : $article->title,
            'visible' => $request->get('visible'),
        ];
        $article->update($data);

        // Удаляем теги
        $tagsToRemove = ArticleTag::where(['article_id' => $id])->delete();

        // Добавляем теги
        if (!empty($request->input('tags'))) {
            $tags = explode(',', $request->input('tags'));
            foreach ($tags as $tag) {
                $tagObg = Tag::firstOrCreate(['title' => $tag]);
                ArticleTag::create(['tag_id' => $tagObg->id, 'article_id' => $id]);
            }
        }

        if (!$request->ajax()) {
            //return back()->with('message','main.messages.updated');
            return redirect(action('ArticlesController@index'));
        }

        return response()->json(
            [
                'status' => 'success',
                'code' => 201,
                'action' => '',
            ]
        );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);

        $defaultPreview = 'assets/media/default/default.jpg';
        if ($article->img_preview !== $defaultPreview) {
            Storage::disk('local')->delete($article->img_preview);
        }

        $article->delete();

        return json_encode(['status' => 'success']);
    }

    /**
     * @param Request $request
     * @param string $field
     * @param $userid
     * @return bool|string
     */
    public function uploadImage(Request $request, $field = 'img', $userid)
    {

        $path = false;

        if ($request->hasFile($field) && $request->file($field)->isValid()) {

            $storageFolders = [ 1 => 'audio', 2 => 'video', 3 => 'image'];
            $storId = $request->get('type_id');

            $userFolder = 'u'.$userid;

            $file = $request->file($field);

            $img = Image::make($file->getRealPath())->resize('100');
            $img = $img->stream()->__toString();

            $extension = $file->getClientOriginalExtension();

            $path = $userFolder . '/' .$storageFolders[$storId] . '/'
                .pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME).'_preview.'.$extension;
            Storage::disk('users_store')->put($path, $img);

        }
        return config('filesystems.disks.users_store.url') . $path;

    }

}
