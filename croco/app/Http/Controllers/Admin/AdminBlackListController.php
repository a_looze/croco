<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use AdminSection;

// use App\Http\Sections\AdminColumn;
// use App\Http\Sections\AdminDisplay;
// use App\Http\Sections\AdminForm;
// use App\Http\Sections\AdminFormElement;
// use SleepingOwl\Admin\Contracts\Initializable;
// use SleepingOwl\Admin\Contracts\DisplayInterface;
// use SleepingOwl\Admin\Contracts\FormInterface;
// use SleepingOwl\Admin\Section;
// use SleepingOwl\Admin\Facades\Admin as AdminSection;

class AdminBlackListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blUsers = User::where('isblocked', '1')->paginate(20);
        if (count($blUsers) < 1) {
            $content = 'Пока нет данных для отображения';
        } else {
            $content = view('admin.blacklist',
                            [
                            'list' => $blUsers,
                            ]
                        );
        }
        // $content = 'Define #2 your dashboard here.';
        return AdminSection::view($content, 'Черный список');
    }
}
