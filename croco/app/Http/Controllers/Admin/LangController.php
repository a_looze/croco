<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller as Controller;
use SleepingOwl\Admin\Facades\Admin as AdminSection;
use SleepingOwl\Admin\Facades\Display as AdminDisplay;
use Illuminate\Http\Request;

class LangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = AdminDisplay::table(); //trans('sleeping_owl::lang.links.index_page');
        return AdminSection::view($content, 'Dashboard');
    }
}
