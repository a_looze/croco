<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use AdminSection;

class AdminClaimListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blUsers = User::where('isblocked', '1')->paginate(20);
        if (count($blUsers) < 1) {
            $content = 'Пока нет данных для отображения';
        } else {
            $content = view('admin.blacklist',
                            [
                            'list' => $blUsers,
                            ]
                        );
        }
        
        return AdminSection::view($content, 'Список жалоб');
    }
}
