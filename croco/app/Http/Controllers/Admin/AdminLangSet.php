<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Lang;
use AdminSection;

class AdminLangSet extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dir = base_path('resources/lang/');

        $langs = Lang::get();

        foreach ($langs as $lang) {
            $langFile = $dir . $lang->lkey . '/main.php';

            if (file_exists($langFile)) {
                $lSet[$lang->lkey] = array_dot(include $langFile);
            } else {
                $lSet[$lang->lkey] = [];
            }
        }

        $keys = [];

        foreach ($lSet as $lKey => $lAr) {
            foreach (array_keys($lAr) as $trKey) {
                $keys[$trKey] = $trKey;
            }
        }

        $count = count($lSet);

        $content = view('admin.setlangs',
                            [
                                'langs' => $langs,
                                'keys' => array_keys($keys),
                                'lSet' => $lSet,
                                'count' => $count,
                            ]
                        );
        
        return AdminSection::view($content, 'Настройка языков');
    }

    public function saveLangStr()
    {
        // $script = request()->server->get('SCRIPT_FILENAME');
        // $isCli = $script === 'artisan' || $script === './artisan';
        // if (!$request->ajax() && !$isCli) {
        //     abort(404, 'The AJAX resource you are looking for could not be found');
        // } 

        $lKey = request()->input('lkey');
        $strKey = request()->input('str');
        $strVal = request()->input('val');

        if ($lKey == 'ru') {
            return response()->json(['status' => 'bad', 'mess' => 'Русские строки в этой версии править нельзя']);
        }

        $dir = base_path('resources/lang/');
        $langFile = $dir . $lKey . '/main.php';

        if (file_exists($langFile)) {
            $lSet = array_dot(include $langFile);
        } else {
            return response()->json(['status' => 'bad', 'mess' => $langFile . ' не найден']);
        }

        // $strVal = mb_strtoupper(mb_substr($strVal, 0, 1)) . mb_strtolower(mb_substr($strVal, 1));
        $strVal = trim($strVal);
        $lSet[$strKey] = $strVal;

        $tmpAr = [];
        foreach ($lSet as $key => $value) {
            array_set($tmpAr, $key, $value);
        }

        $fileContent = '<?php 
return ' . var_export($tmpAr, true) . ';';

        file_put_contents($langFile, $fileContent);

        return response()->json(['status' => 'OK', 'mess' => $strVal]);
    }
}
