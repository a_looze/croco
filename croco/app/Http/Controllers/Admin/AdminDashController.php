<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Eventlogs;
use AdminSection;

class AdminDashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logCollection = Eventlogs::orderby('created_at', 'desc')->paginate(20);
        if (count($logCollection) < 1) {
            $content = 'Пока нет данных для отображения';
        } else {
            $content = view('admin.dashboard',
                            [
                            'list' => $logCollection,
                            ]
                        );
        }
        // $content = 'Define #2 your dashboard here.';
        return AdminSection::view($content, 'Табло');
    }
}
