<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Article;
use App\User;
use App\Comment;
use App\Answer;
use App\File;
use App\Tag;
use App\ArticleTag;
use App\Category;
use App\Subscribe;
use App\Helpers\Scenario;
use App\Helpers\FileHelper as FileService;
use Auth;

class ShowArticlesController extends Controller
{

    public $fileService;

    public $files;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
        // библиотеке юзера
        if (Auth::user()) {
            $this->files = $this->fileService->getUserLib();
            view()->share('files', $this->files);
        }

    }

    public function index($alias, Scenario $scenario)
    {
        $articleCollection = Article::where('alias', $alias)
                                    ->where('visible', 1)
                                    ->where('banned', 0)
                                    ->where('deleted', 0)
                                    ->get();
        if (count($articleCollection) !== 1) {
            return response()->view('errors.404', [], 404);
        }

        $article = $articleCollection[0];

        // проверяем наличие файлов для темы
        // with() и прочие улучшения пока не используем
        $fileCollection = File::where('id', $article->file_id)
                                ->get();
        
        if (count($fileCollection) !== 1) {
            return response()->view('errors.404', [], 404);
        }

        $file = $fileCollection[0];

        $userCollection = User::where('id', $article->user_id)
                                ->where('isblocked', 0)
                                ->get();
        if (count($userCollection) !== 1) {
            return response()->view('errors.404', [], 404);
        }


        $owner = $userCollection[0];


        $path = public_path('assets/media/users/') . $article->user_id . '/'. $file->type_id . '/';
        $urlPath = 'assets/media/users/' . $article->user_id . '/'. $file->type_id . '/';
        // dd($path.$file->alias.'.jpg');
        if (!$this->checkIsFileExists($file->alias, $file->type_id, $path)) {
            return response()->view('errors.404', [], 404);
        }

        // получаем файл для превью 
        $pfCollection = $article->preview()->get();
        if (count($pfCollection) !== 1) {
            $preview = false;
        } else {
            $preview = $pfCollection[0];

            if (!$this->checkIsFileExists($preview->alias, $preview->type_id, $path)) {
                // тут можно использовать логику подстановки превью по умолчанию
                // пока эту логику вытаскиваем в шаблон
                $preview = false;
            }
        }

        // получаем комментарии и ответы
        $answerCollection = Answer::with([
                                        'author',
                                        'attach',
                                    ])
                                    ->where('article_id', $article->id)
                                    ->where('author_say', '<', 3)
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        $commentCollection = Comment::with([
                                        'author',
                                        'attach',
                                    ])
                                    ->where('parent_id', $article->id)
                                    ->where('target', 'post')
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        // отбрасываем ответы, для которых файл не существует или автор блокирован
        foreach ($answerCollection as $key => $uAnswer) {
            $aPath = public_path('assets/media/users/') . $uAnswer->user_id . '/'. $uAnswer->attach->type_id . '/';
            if ($uAnswer->attach === NULL
                || !$this->checkIsFileExists($uAnswer->attach->alias, 
                                             $uAnswer->attach->type_id, 
                                             $aPath
                                             )
                || !is_object($uAnswer->author)
                || $uAnswer->author->isblocked == 1
                ) 
            {
                $answerCollection->forget($key);
            }
        }
        
        // отбрасываем комментарии, для которых автор не существует или блокирован
        foreach ($commentCollection as $key => $uAnswer) {
            if (!is_object($uAnswer->author) || $uAnswer->author->isblocked == 1) {
                $commentCollection->forget($key);
            }
        }

        // добавляем ответ автора
        $authorAnswer = false;
        $authorAnswerCollection = Answer::with(['attach', 'preview'])
                                ->where('article_id', $article->id)
                                ->where('author_say', 3)
                                ->get();
        if (count($authorAnswerCollection) === 1) {
            $authorAnswer = $authorAnswerCollection[0];
        }

        // теги темы
        $tags = ArticleTag::with('tag')
                            ->where('article_id', $article->id)
                            ->get();

        // данные текущего юзера
        $user = auth()->user();        

        // состояние подписки юзера
        $userSubs = false;
        // владелец или нет
        $iAmOwner = false;
        if (is_object($user)) {
            $subsObjAr = Subscribe::where('channel_id', $article->id)
                                ->where('user_id', $user->id)
                                ->where('type', 'channel')
                                ->get();
            if (count($subsObjAr) == 1) {            
                $userSubs = true;
            }

            if ($user->id === $article->user_id) {
                $iAmOwner = true;
            } 
        }

        // общее кол-во подписок
        $subsCollection = Subscribe::where('channel_id', $article->id)
                                ->where('type', 'channel')
                                ->get();
        $subsCount = count($subsCollection);

        // получаем список других тем из этой категории
        $otherArticles = false;
        $otherArticlesCollection = Article::with(['preview', 
                                                 'answers' => function($q) {
                                                    $q->where('author_say', '<', 3);
                                                 }
                                                 ])
                                            ->where('category_id', $article->category_id)
                                            ->where('id', '<>', $article->id)
                                            ->where('visible', '1')
                                            ->orderby('created_at')
                                            ->limit('8')
                                            // ->limit('4')
                                            ->get();
        if (count($otherArticlesCollection) > 0) {
            foreach ($otherArticlesCollection as $key => $art) {
                $otherArticles[$key] = $art;
            }
        }

        $cat = Category::where('id', $article->category_id)->first();

        // вызываем событие просмотра темы
        $scenario->viewArticle($article);

        // устанавливаем класс для прозрачности правой колонки
        // 1 - div с классом visActive, 0 - без него
        $currentVal = request()->cookie('ut_sv');
        if (!isset($currentVal) || $currentVal == '1') {
            $opacity = ' visActive';
        } else {
            $opacity = '';
        }

        // адресат вопроса
        if ($article->for_user_id > 0) {
            $forUser = User::whereId($article->for_user_id)->first();
        } else {
            $forUser = false;
        }

        return view('article', [
            'alias' => $alias,
            'article' => $article,
            'file' => $file,
            'owner' => $owner,
            'path' => $urlPath,
            'tags' => $tags,
            'preview' => $preview,
            'comments' => $commentCollection,
            'answers' => $answerCollection,
            'user' => $user,
            'userSubs' => $userSubs,
            'subsCount' => $subsCount,
            'iAmOwner' => $iAmOwner,
            'authorAnswer' => $authorAnswer,
            'otherArticles' => $otherArticles,
            'opacity' => $opacity,
            'forUser' => $forUser,
            'catUrl' => $cat->alias,
            'cat' => $cat,
        ]);
    }

    private function checkIsFileExists($alias, $type, $path/*=''*/)
    {
        // if ($path == '') {
        //     // $path = public_path() . '/files/' . $type . '/';
        //     $path = public_path('assets/media/users/' . $user->id . '/' . $type . '/');
        // }

        if (!is_dir($path)) {
            return false;
        }

        switch ($type) {
            case '1':
                // print_r($path . $alias . '.mp4<br>');
                if (!file_exists($path . $alias . '.mp4')
                    || !file_exists($path . $alias . '.ogv')
                    || !file_exists($path . $alias . '.webm')
                    )
                {
                    return false;
                }

            break;

            case '2':
                if (!file_exists($path . $alias . '.mp3')
                    || !file_exists($path . $alias . '.oga')
                    )
                {
                    return false;
                }
            break;

            case '3':
                if (!file_exists($path . $alias . '.jpg')) 
                {
                    return false;
                }
            break;
        }

        return true;
    }

}
