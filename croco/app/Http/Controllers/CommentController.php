<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Comment;
use App\File;
use App\Helpers\Scenario;
use App\Helpers\FileHelper as FileService;
use App\Helpers\FileStore;

class CommentController extends Controller
{

    public $fileService;

    public $files;

    public function __construct(FileService $fileService)
    {
        $this->middleware('auth');

        $this->fileService = $fileService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Scenario $scenario, FileStore $store)
    {
        $this->validate($request, [
            'file' => 'required_without_all:content,file_from_lib|max:19456',
            'parent_id' => 'required|integer',
            'target' => 'required',
            'content' => 'required_without_all:file,file_from_lib',
            'file_from_lib' => 'integer',
        ]);

        $userId = Auth::user()->id;

        // РАБОТТАЕМ С ФАЙАЛОМ

        $file_id = '';

        // Если файл из библиотеки
        if (!empty($request->get('file_from_lib'))) {
            $file_id = intval($request->get('file_from_lib'));
        }

        // Если файл был загружен
        if ($request->hasFile('file') && empty($file_id)) {

            $file = $request->file('file');

            $fileExt = $file->getClientOriginalExtension();

            $typeId = $this->fileService->getTypeByExtension($fileExt);

            // сохраняем файл, если прошла валидация на тип файла
            if($typeId !== false) {

                // СОХРАНЯЕМ ФАЙЛ И ПРИВЬЮ ДЛЯ ФРОНТА И БИБЛИОТЕКИ
                $data = [
                    'user_id' => $userId,
                    'type_id' => $typeId,
                ];

                // создаем записи в бд
                // возвращает массив объектов файлов
                $files = $store->storeFileWithPreview($data);

                // отправляем файл на конвертацию
                if ($typeId == 3) {

                    $data = [
                        'real_path' => $file->getRealPath(),
                        'user_id' => $userId,
                        'file_id' => $files['file']->id,
                    ];

                    $store->moveImage($data);

                } else {
                    $file->move(storage_path('videoin'),
                        $files['file']->id.'.'.$file->getClientOriginalExtension());
                }

                $file_id = $files['file']->id;

            }

        }

        // СОХРАНЯЕМ КОММЕНТАРИЙ
        $comment = new Comment();
        $data = [
            'user_id' => $userId,
            'file_id' => $file_id,
            'target' => $request->get('target'),
            'content' => !empty($request->get('content')) ? trim($request->get('content')) : '',
            'parent_id' => $request->get('parent_id'),
        ];
        $obj = $comment->create($data);

        $scenario->addBonusForComment($obj);

        return response()->json(
            [
                'status' => 'success',
                'code' => 201,
                'action' => '',
            ]
        );
    }

}
