<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Auth;
use App\Http\Requests;
use App\Events\LogUserActivity;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Category;
use App\Article;
use App\Subscribe;
use App\UserLike;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * На главной выводим набор вопросов по псевдокатегориям
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userObj = auth()->user();

        $ph = [];
        $psCat = ['new', 'popul', 'rev', 'like'];
        // $psCat = ['rec', 'new', 'popul', 'rev', 'like'];

        foreach ($psCat as $psName) {
            $pCat['name'] = trans('main.m' . $psName);
            $pCat['url'] = url($psName);

            // получаем наборы вопросов в каждой псевдокатегории
            switch ($psName) {
                case 'like':
                    $pCat['letter'] = 'like';
                    // вопросы, которые юзер лайкал
                    if (!is_object($userObj)) {
                        $articles = [];
                    } else {
                        $likes = UserLike::with(['article' => function($q) {
                                                    $q->where('visible', 1);
                                                }])
                                            ->where('user_id', $userObj->id)
                                            ->where('target', 'article')
                                            ->orderby('created_at', 'desc')
                                            ->get();
                        
                        $articles = collect();

                        if (count($likes) > 0) {
                            foreach ($likes as $like) {
                                $articles = $articles->merge($like->article()->with(['preview', 
                                         'answers' => function($q) {
                                            $q->where('author_say', '<', 3);
                                         },
                                         'author'
                                         ])->limit(10)->get());
                            }
                        } else {
                            $articles = [];
                        }
                    }
                break;

                case 'rev':
                    $pCat['letter'] = 'late';
                    // недавно просмотренные
                    $vList = request()->cookie('ut_vql');
                    if (!$vList || trim($vList) == '') {
                        $articles = [];
                    } else {
                        $vAr = explode('|', $vList);
                        $articles = Article::with(['preview', 
                                         'answers' => function($q) {
                                            $q->where('author_say', '<', 3);
                                         },
                                         'author'
                                         ])
                                ->wherein('id', $vAr)
                                ->where('visible', 1)
                                ->orderby('created_at', 'desc')
                                ->limit(10)
                                ->get();
                    }
                break;

                case 'popul':
                    $pCat['letter'] = 'hot';
                    // набирающие популярность
                    $articles = Article::with(['preview', 
                                         'answers' => function($q) {
                                            $q->where('author_say', '<', 3);
                                         },
                                         'author'
                                         ])
                                ->where('rating', '>', 0)
                                ->where('visible', 1)
                                ->orderby('rating', 'desc')
                                ->limit(10)
                                ->get(); 
                break;

                case 'new':
                    $pCat['letter'] = 'new';
                    // устанавливаем новинками темы за последний месяц
                    $dateLast = Carbon::now()->subMonth()->toDateTimeString();
                    // dd($dateLast);
                    $articles = Article::with(['preview', 
                                         'answers' => function($q) {
                                            $q->where('author_say', '<', 3);
                                         },
                                         'author'
                                         ])
                                ->where('created_at', '>', $dateLast)
                                ->where('visible', 1)
                                ->orderby('created_at', 'desc')
                                ->limit(10)
                                ->get(); 
                break;

                case 'rec':
                    $pCat['letter'] = 'new';
                    // вообще непонятно что выдавать в рекомендациях
                    $articles = [];
                    
                    // список статей
                    // $articles = Article::with(['preview', 
                    //                      'answers' => function($q) {
                    //                         $q->where('author_say', '<', 3);
                    //                      },
                    //                      'author'
                    //                      ])
                    //             ->where('category_id', $catOb->id)
                    //             ->where('visible', 1)
                    //             ->orderby('created_at', 'desc')
                    //             // ->paginate(18)
                    //             ->get(); // ????? ДОДЕЛАТЬ!!! 
                break;
            }


            $pCat['articles'] = $articles;

            $ph[$psName] = $pCat;
        }

        $catCollection = Category::get();
        foreach ($catCollection as $cat) {
            $rCat['name'] = trans($cat->langkey);
            $rCat['url'] = url($cat->alias);
            $rCat['letter'] = $cat->letter;

            $articles = Article::with(['preview', 
                                         'answers' => function($q) {
                                            $q->where('author_say', '<', 3);
                                         },
                                         'author'
                                         ])
                                ->where('category_id', $cat->id)
                                ->where('visible', 1)
                                ->orderby('created_at', 'desc')
                                ->limit(10)
                                ->get();
            $rCat['articles'] = $articles;
            $phc[$cat->alias] = $rCat;
        }

        // dd($ph);
        return view('home', [
                            'pseudocat' => $ph,
                            'realcat' => $phc,
                            ]
                        );
    }

}
