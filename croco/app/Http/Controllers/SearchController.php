<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tag;
use App\Category;
use App\Article;
use App\Subscribe;
use App\User;

class SearchController extends Controller
{
    private $onPage = 50;

    public function tagSearch($tag)
    {
        $userObj = auth()->user();

        // данные текущей "категории" подделываем, чтобы не усложнять шаблоны
        $catOb = new Category();
        $catOb->id = 0;
        $catOb->active = 1;
        $catOb->visible = 1;
        $catOb->alias = $tag;
        $catOb->langkey = trans('main.mtagsearch', ['tag' => $tag]);

        // временно одинаково для всех
        $catOb->bgimg = 'images/demo.jpg'; 
        $catOb->letter = 'search'; 
        // dd($catOb);

        // получаем список вопросов по данному тегу
        $tagObj = Tag::whereTitle($tag)->first();
        // dd($tagObj);
        if ($tagObj === null) {
            $articles = [];
            $hasMorePages = false;
        } else {
            $articles = $tagObj->articles()->simplePaginate($this->onPage);
            $hasMorePages = $articles->hasMorePages();
        }        

        return view('category', ['cat' => $catOb, 
                                'articles' => $articles,
                                'showSubs' => false,
                                'hasMorePages' => $hasMorePages,
                                ]
                    );
    }

    public function strSearch(Request $request)
    {
        $str = $request->input('search');
        // dd($str);

        // данные текущей "категории" подделываем, чтобы не усложнять шаблоны
        $catOb = new Category();
        $catOb->id = 0;
        $catOb->active = 1;
        $catOb->visible = 1;
        $catOb->alias = $str;
        $catOb->langkey = trans('main.msearch', ['str' => $str]);

        // временно одинаково для всех
        $catOb->bgimg = 'images/demo.jpg'; 
        $catOb->letter = 'search'; 
        // dd($catOb);

        // поиск по юзернейму
        $sStr = str_replace(' ', '%', $str);
        $users = User::where('name', 'like', '%' . $sStr . '%')
                        ->whereOr('alias', 'like', '%' . $sStr . '%')
                        ->where('isblocked', 0)
                        ->get();

        $usersCount = count($users);

        // поиск по статьям
        $articles = Article::with(['preview', 'attach',
                                    'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                    ])
                            ->where('title', 'like', '%' . $sStr . '%')
                            ->where('visible', 1)
                            ->orderby('created_at', 'desc')
                            ->simplePaginate($this->onPage);
        $hasMorePages = $articles->hasMorePages();

        return view('search', ['cat' => $catOb, 
                                'srchVal' => $str,
                                'usersCount' => $usersCount,
                                'users' => $users,
                                'showSubs' => false,
                                'articles' => $articles,
                                'hasMorePages' => $hasMorePages,
                                ]
                    );
    }
}
