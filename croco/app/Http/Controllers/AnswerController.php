<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Answer;
use App\File;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helpers\Scenario;
use App\Helpers\FileHelper as FileService;
use App\Helpers\FileStore;
use Image;

class AnswerController extends Controller
{

    public $fileService;

    public $files;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FileService $fileService)
    {
        $this->middleware('auth');

        $this->fileService = $fileService;

        $userId = Auth::user()->id;

        $userObj = User::with(
            [
                'SubscribedOnUser' => function($q) {
                    $q->where(['type' => 'channel']);
                },
            ]
        )->where(['id' => $userId])->first();

        // количество подписок на канал юзера
        $this->subs = count($userObj->SubscribedOnUser);
        view()->share('subsCount', $this->subs);

        // библиотеке юзера
        $this->files = $this->fileService->getUserLib();
        view()->share('files', $this->files);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Scenario $scenario, FileStore $store)
    {
        $this->validate($request, [
            'file' => 'required_without:file_from_lib|mimes:jpeg,png,jpg,gif,mp3,mpga,mp4,wma,wmv,webm,avi,ogg,oga|max:19456',
            'file_from_lib' => 'required_without:file',
            //'preview' => 'mimes:jpeg,png,jpg,gif',
        ]);

        $userId = Auth::user()->id;

        // РАБОТТАЕМ С ФАЙАЛОМ

        $file_id = '';
        $preview_file_id = '';
        // Если файл из библиотеки
        if (!empty($request->get('file_from_lib'))) {
            $file_id = intval($request->get('file_from_lib'));
        }

        // Если файл был загружен
        if ($request->hasFile('file') && empty($file_id)) {

            $file = $request->file('file');

            $fileExt = $file->getClientOriginalExtension();

            $typeId = $this->fileService->getTypeByExtension($fileExt);

            // проверкая файлов на сервер
            if($typeId === false) {
                return json_encode(['status' => 'error', "error" => "File could not be saved.", 'code' => 401]);
            }

            // СОХРАНЯЕМ ФАЙЛ И ПРИВЬЮ ДЛЯ ФРОНТА И БИБЛИОТЕКИ
            $data = [
                'user_id' => $userId,
                'type_id' => $typeId,
            ];

            // создаем записи в бд
            // возвращает массив объектов файлов
            $files = $store->storeFileWithPreview($data);

            // отправляем файл на конвертацию
            if ($typeId == 3) {

                $data = [
                    'real_path' => $file->getRealPath(),
                    'user_id' => $userId,
                    'file_id' => $files['file']->id,
                ];

                $store->moveImage($data);

            } else {
                $file->move(storage_path('videoin'),
                    $files['file']->id.'.'.$file->getClientOriginalExtension());
            }

            $file_id = $files['file']->id;

            $preview_file_id = $files['previewFront']->id;

        }

        // СОХРАНЯЕМ ОТВЕТ
        $answer = new Answer();
        $data = [
            'user_id' => $userId,
            'file_id' => $file_id,
            'preview_file_id' => $preview_file_id,
            'author_say' => !empty($request->get('author_say')) ? $request->get('author_say') : '',
            'article_id' => $request->get('article_id'),
        ];
        $obj = $answer->create($data);

        $scenario->addBonusForAnswer($obj);

        return response()->json(
            [
                'status' => 'Dropzone.ADDED',
                'code' => 201,
                'action' => action('AnswerController@edit', ['id '=> $obj->id]),
                'type' => !empty($request->get('author_say')) && $request->get('author_say') == 3 ? 'profile' : 'front',
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userId = Auth::user()->id;

        $answer = Answer::with('preview')->where(['id' => $id])->first();
        $answerFile = File::find($answer->file_id);

        $answerFile->path = $this->fileService->getFile($userId, $answerFile->type_id, $answerFile->alias);

        // Превью
        if(!is_null($answer->preview)) {
            $image = $this->fileService->getImage($userId, 3, $answer->preview->alias);
            if ($image) {
                $answer->previewPath = $image;
            }
        }

        if (is_null($answer->previewPath)) {
            // default image
            if ($answerFile->type_id == 3) {
                $answer->previewPath = $answerFile->path;
            } else {
                $type = $answerFile->type_id == 1 ? 'video' : 'audio';
                $answer->previewPath = config('filesystems.default_path.'.$type);
            }
        }

        return view('profile.answers.edit', [
            'answer' => $answer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:19456',
            'file_from_lib' => 'integer',
            'title' => 'string',
            'description' => 'string'
        ]);

        $preview_file_id = '';

        // Превью из библиотеки
        if (!empty($request->get('file_from_lib'))) {
            $preview_file_id = intval($request->get('file_from_lib'));
        }

        // Добавляем привью
        if ($request->hasFile('file') && empty($preview_file_id)) {

            $preview = $request->file('file');
            $alias = time().'-f';

            $path = public_path('assets/media/users').'/'.$userId.'/3/'.$alias.'.jpg';

            $image = Image::make($preview->getRealPath())->encode('jpg');
            $image->save($path);

            // СОХРАНЯЕМ ФАЙЛ
            $file = new File();
            $data = [
                'user_id' => $userId,
                'alias' => $alias,
                'type_id' => 3,
                'title' => $alias,
            ];
            $prevObj = $file->create($data);

            $preview_file_id = $prevObj->id;

        }

        // РАБОТАЕМ С ОТВЕТОМ
        $answer = Answer::with('article')->where(['id' => $id])->first();
        $data = [
            'preview_file_id' => !empty($preview_file_id) ? $preview_file_id : $answer->preview_file_id,
        ];
        $answerObj = $answer->update($data);

        //return back()->with('message','main.messages.updated');
        if ($answer->author_say ==3) {
            return redirect(action('ArticlesController@edit', ['id' => $answer->article->id]));
        }
        return redirect(url('/v/'.$answer->article->alias));
    }

}
