<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;
use App\Subscribe;
use App\UserLike;

use App\Http\Requests;
use DB;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class CategoryController extends Controller
{
    private $onPage = 50;

    /**
     * Show the category page
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $cat)
    {
        $userObj = auth()->user();

        // данные текущей категории
        $catObjAr = Category::where('active', 1)
                        ->where('visible', 1)
                        ->where('alias', $cat)
                        ->get();

        if (!$catObjAr || count($catObjAr) != 1) {
            return response()->view('errors.404', [], 404);
        } else {
            $catOb = $catObjAr[0];
        }

        // список статей
        $articles = Article::with(['preview', 
                                     'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                     'author'
                                     ])
                            ->where('category_id', $catOb->id)
                            ->where('visible', 1)
                            ->orderby('created_at', 'desc')
                            ->simplePaginate($this->onPage);

        $hasMorePages = $articles->hasMorePages();

        // состояние подписки юзера
        $userSubs = false;
        if (is_object($userObj)) {
            $subsObjAr = Subscribe::where('channel_id', $catOb->id)
                                ->where('user_id', $userObj->id)
                                ->where('type', 'category')
                                ->get();
            if (count($subsObjAr) == 1) {            
                $userSubs = true;
            }
        }

        // общее кол-во подписок
        $subsCollection = Subscribe::where('channel_id', $catOb->id)
                                ->where('type', 'category')
                                ->get();
        $subsCount = count($subsCollection);
        

        return view('category', ['cat' => $catOb, 
                                'articles' => $articles,
                                'userSubs' => $userSubs,
                                'subsCount' => $subsCount,
                                'showSubs' => true,
                                'hasMorePages' => $hasMorePages,
                                ]
                    );
    }

    /**
     * Show the pseudocategory page
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request, $pseudocat)
    {
        $userObj = auth()->user();

        // данные текущей псевдокатегории подделываем, чтобы не усложнять шаблоны
        $catOb = new Category();
        $catOb->id = 0;
        $catOb->active = 1;
        $catOb->visible = 1;
        $catOb->alias = $pseudocat;
        $catOb->langkey = trans('main.m' . $pseudocat);        

        // временно одинаково для всех
        $catOb->bgimg = 'images/demo.jpg'; 
        // $catOb->letter = 's'; 
        // dd($catOb);

        // получаем список вопросов для каждой псевдокатегории
        switch ($pseudocat) {
            case 'like':
                // вопросы, которые юзер лайкал
                $catOb->letter = 'like';
                if (!is_object($userObj)) {
                    $articles = [];
                    $hasMorePages = false;
                } else {
                    $ulCollection = UserLike::where('user_id', $userObj->id)
                                            ->where('target', 'article')
                                            ->orderby('created_at', 'desc')
                                            ->get();

                    foreach ($ulCollection as $data) {
                        // задаем порядок вывода...
                        $tmpAr[$data->item_id] = $data->item_id;
                    }

                    if (!isset($tmpAr) || count($tmpAr) < 1) {
                        $articles = [];
                        $hasMorePages = false;
                    } else {
                        $articlesPre = Article::with(['preview', 
                                                     'answers' => function($q) {
                                                        $q->where('author_say', '<', 3);
                                                     },
                                                     'author'
                                                     ])
                                            ->whereIn('id', $tmpAr)
                                            ->get();

                        // ...и размещаем объекты в массив по заданному порядку
                        foreach ($articlesPre as $articleObj) {
                            if (isset($tmpAr[$articleObj->id])) {
                                $tmpAr[$articleObj->id] = $articleObj;
                            }
                        }

                        // имитируем пагинацию
                        $chunks = collect($tmpAr)->chunk($this->onPage);
                        $articles = $chunks[0];

                        $hasMorePages = isset($chunks[1]);
                    }
                }
            break;

            case 'rev':
                // недавно просмотренные
                $catOb->letter = 'late';
                $vList = request()->cookie('ut_vql');
                // dd($vList);
                if (!$vList || trim($vList) == '') {
                    $articles = [];
                    $hasMorePages =false;
                } else {
                    $vAr = explode('|', $vList);
                    $articles = Article::with(['preview', 
                                     'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                     'author'
                                     ])
                            ->wherein('id', $vAr)
                            ->where('visible', 1)
                            ->orderby('created_at', 'desc')
                            ->simplePaginate($this->onPage);
                $hasMorePages = $articles->hasMorePages();
                }
            break;

            case 'popul':
                // набирающие популярность
                $catOb->letter = 'hot';
                // пока тупо делаем выборку из самых популярных - нет алгоритма
                $articles = Article::with(['preview', 
                                     'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                     'author'
                                     ])
                            ->where('rating', '>', 10)
                            ->where('visible', 1)
                            ->orderby('rating', 'desc')
                            ->simplePaginate($this->onPage);
                $hasMorePages = $articles->hasMorePages();
            break;

            case 'new':
                // устанавливаем новинками темы за последний месяц
                $catOb->letter = 'new';
                $dateLast = Carbon::now()->subMonth()->toDateTimeString();
                // dd($dateLast);
                $articles = Article::with(['preview', 
                                     'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                     'author'
                                     ])
                            ->where('created_at', '>', $dateLast)
                            ->where('visible', 1)
                            ->orderby('created_at', 'desc')
                            ->simplePaginate($this->onPage);
                $hasMorePages = $articles->hasMorePages();
            break;

            case 'rec':
                // вообще непонятно что выдавать в рекомендациях
                $catOb->letter = 'hot';
                // $articles = [];
                
                // список статей
                $articles = Article::with(['preview', 
                                     'answers' => function($q) {
                                        $q->where('author_say', '<', 3);
                                     },
                                     'author'
                                     ])
                            ->where('rating', '>', 10) // пусть хоть так
                            ->where('visible', 1)
                            ->orderby('created_at', 'desc')
                            ->simplePaginate($this->onPage);
                $hasMorePages = $articles->hasMorePages();
            break;
        }

               

        return view('category', ['cat' => $catOb, 
                                'articles' => $articles,
                                'showSubs' => false,
                                'hasMorePages' => $hasMorePages,
                                ]
                    );
    }
}
