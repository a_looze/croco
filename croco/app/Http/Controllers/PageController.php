<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Page;

class PageController extends Controller
{
    /**
     * Show the category page
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $page)
    {
        
        $pageCollection = Page::where('alias', $page)
            ->where('active', 1)
            ->get();

        if (count($pageCollection) != 1) {
            return response()->view('errors.404', [], 404);
        }

        $pageObj = $pageCollection[0];

        return view($pageObj->template, 
                            [
                            'page' => $pageObj,
                            ]
                    );
    }
}
