<?php

namespace App\Http\Controllers;

use App\Helpers\FileStore;
use App\Lang;
use App\User as User;
use App\Subscribe;
use App\Http\Requests;
use App\Helpers\Country;
use App\Userbonus;
use App\UserAlert;
use DB;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public $countries;

    public $subs;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Country $country)
    {
        $userId = Auth::user()->id;

        $this->countries = $country->getList();

        $userObj = User::with(
            [
                'SubscribedOnUser' => function($q) {
                    $q->where(['type' => 'channel']);
                },
            ]
        )->where(['id' => $userId])->first();

        $this->subs = count($userObj->SubscribedOnUser);

        view()->share('subsCount', $this->subs);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.cabinet', [
            'countries' => $this->countries,
            //'subsCount' => $this->subs,
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //include_once(storage_path('app/countries/en.inc.php'));
        return view('profile.edit', [
            'countries' => $this->countries,
        ]);
    }

    public function save(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $userid = Auth::user()->id;

        $user = User::find($userid);

        $data = $request->all();
        //$data['avatar'] =  $this->uploadAvatar($request, 'img', $userid) ?: $user->avatar;

        $user->update($data);

        return back()->with('message','main.messages.updated');

    }

    public function update(Request $request)
    {
        $this->validate($request, [
           'name' => 'required|min:3',
            'old_password' => 'required_with:password',
            'password' => 'required_with:old_password|min:6',
            'password_confirmation' => 'same:password',
        ]);

        $userId = Auth::user()->id;

        $password = $request->get('password');
        $old_password = $request->get('old_password');

        // проверяем пароль
        if (!empty($password) &&
            !empty($old_password) &&
            !Hash::check($old_password, Auth::user()->password)) {

            return back()->with('message','Error password');

        }

        $data = $request->all();
        unset($data['password']);

        if (!empty($password)) {
            $data['password'] = bcrypt($password);
        }

        $user = User::find($userId);
        $user->update($data);

        return back()->with('message','main.messages.updated');

    }

    public function showChangePassForm()
    {
        return view('profile.changepass');
    }

    public function showSigns()
    {
        $userObj = auth()->user();

        $subscribes = Subscribe::with('channel')
                            ->where('user_id', $userObj->id)
                            ->get();
                            
        return view('profile.signs', [
            'user' => $userObj,
            'subscribes' => $subscribes,
        ]);
    }

    public function showNotifies()
    {
        $user = auth()->user();
        if ($user) {
            $alerts = UserAlert::where('user_id', $user->id)->orderby('created_at', 'desc')->get();
        }

        // устанавливаем все сообщения как прочитанные, распаковываем данные из JSON
        $alerts->transform(function ($item, $key) {
            $item->isnew = 0;
            $item->save();
            $item->data = json_decode($item->comment_data, true);
            if (!is_array($item->data)) {
                $item->data = [];
            }
            return $item;
        });

        return view('profile.notifies', ['alerts' => $alerts]);
    }

    public function showBonuses()
    {
        $userId = Auth::user()->id;

        //$bonus = Userbonus::find($userId);

        DB::statement(DB::raw('set @given_user:='.$userId));
        DB::statement(DB::raw('set @pos:=0'));

        $q = "SELECT * FROM
        (
            SELECT (@pos:=@pos+1) pos, user_id, bonus_amount
            FROM userbonuses
            ORDER BY bonus_amount DESC
        ) userbonuses
        WHERE user_id = @given_user;";


        $userBonusRes = DB::select($q);

        $userBonus = collect($userBonusRes)->map(function($x){ return (array) $x; })->toArray();

        // если пользователя нет в рейтинге
        if (count($userBonus) < 1) {
            $userBonus[0] = [
                'pos' => trans('main.dont_in_rating'),
                'bonus_amount' => 0,
            ];
        }

        return view('profile.bonuses', ['userBonus' => $userBonus[0]]);
    }

    public function showSettings()
    {
        return view('profile.settings',
            [
                'countries' => $this->countries,
            ]
        );
    }

    /**
     *
     * Upload and resize user avatar
     *
     * @param Request $request
     * @param string $field
     * @return bool|string
     */
    public function uploadAvatar(Request $request, $field = 'avatar')
    {

        $userid = Auth::user()->id;
        $path = false;

        if ($request->hasFile($field) && $request->file($field)->isValid()) {

            $userFolder = 'u'.$userid;

            $avatar = $request->file($field);

            $img = Image::make($avatar->getRealPath())->resize('100', null);
            $img = $img->stream()->__toString();

            $extension = $avatar->getClientOriginalExtension();

            $path = $userFolder . '/avatar/'
                .pathinfo($avatar->getClientOriginalName(), PATHINFO_FILENAME).'.'.$extension;
            Storage::disk('users_store')->put($path, $img);

        }
        return $path ? config('filesystems.disks.users_store.url') . $path : false;

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|',
            'password' => 'required|min:6',
            'password_confirmation' => 'same:password',
        ]);

        $password = $request->get('password');
        $password_confirmation = $request->get('password_confirmation');
        $old_password = $request->get('old_password');

        if (Hash::check($old_password, Auth::user()->password)) {
            $user = User::find(Auth::user()->id);
            $user->update([
                'password' => bcrypt($password)
            ]);
            return back()->with('message','password.reset');
        }

        return back()->with('message','Error entered data');

    }

    /**
     * Загружаем аватарку и баннер юзера
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editUsermedia(Request $request, FileStore $store)
    {

        $userId = Auth::user()->id;

        $this->validate($request, [
            'banner' => 'image|mimes:jpeg,png,jpg,gif,svg|max:19456',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:19456',
        ]);

        if ($request->hasFile('banner')) {

            $file = $request->file('banner');
            $fileName = 'banner';

        } elseif ($request->hasFile('avatar')) {

            $file = $request->file('avatar');
            $fileName = 'avatar';

        } else {

            return response()->json(
                [
                    'status' => 'success',
                    'code' => 401,
                ]
            );

        }

        $fullPath = 'assets/media/users/'.$userId;

        $result = $store->createDir($fullPath);

        $file->move(public_path($fullPath),
            $fileName.'.jpg');

        User::find($userId)->update([$fileName => $fullPath.'/'.$fileName.'.jpg']);

        return response()->json(
            [
                'status' => 'success',
                'code' => 201,
            ]
        );
    }
}
