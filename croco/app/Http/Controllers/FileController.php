<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\User;
use App\File;
use App\Helpers\FileHelper as FileService;
use App\Helpers\FileStore;
use Auth;
use Image;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;


class FileController extends Controller
{

    public $files;

    public $fileService;

    public function __construct(FileService $fileService)
    {
        $this->middleware('auth');

        $this->fileService = $fileService;

        $userId = Auth::user()->id;

        // данные юзера
        $userObj = User::with(
            [
                'SubscribedOnUser' => function($q) {
                    $q->where(['type' => 'channel']);
                },
            ]
        )->where(['id' => $userId])->first();

        // количество подписок на канал юзера
        $this->subs = count($userObj->SubscribedOnUser);
        view()->share('subsCount', $this->subs);

        // библиотеке юзера
        $this->files = $this->fileService->getUserLib();
        view()->share('files', $this->files);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.libs.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profile.libs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, FileStore $store)
    {
        $userId = Auth::user()->id;

        $this->validate($request, [
            'file' => 'mimes:jpeg,png,jpg,gif,mp3,mpga,mp4,wma,wmv,webm,avi,ogg,oga|max:19456',
            //'preview' => 'image|mimes:jpeg,png,jpg,gif|max:1024',
            //'title' => 'string',
            //'description' => 'string',
            //'preview_from_lib' => 'integer',
            //'file_from_lib' => 'integer',
        ]);

        // РАБОТАЕМ С ФАЙЛОМ
        $file = $request->file('file');

        $fileExt = $file->getClientOriginalExtension();

        $typeId = $this->fileService->getTypeByExtension($fileExt);

        // возвращаем ошибку, если тип файла не указан
        if($typeId === false) {
            return response()->json(
                [
                    'status' => 'error',
                    'error' => trans('main.file_type_error'),
                    'code' => 422,
                ]
            );
        }

        // СОХРАНЯЕМ ФАЙЛ И ПРИВЬЮ ДЛЯ ФРОНТА И БИБЛИОТЕКИ
        $data = [
            'user_id' => $userId,
            'type_id' => $typeId,
        ];

        // создаем записи в бд
        // возвращает массив объектов файлов
        $files = $store->storeFileWithPreview($data);

        // отправляем файл на конвертацию
        if ($typeId == 3) {

            $data = [
                'real_path' => $file->getRealPath(),
                'user_id' => $userId,
                'file_id' => $files['file']->id,
            ];

            $store->moveImage($data);

        } else {
            $file->move(storage_path('videoin'),
                $files['file']->id.'.'.$file->getClientOriginalExtension());
        }


        return response()->json(
            [
                'status' => 'success',
                'code' => 201,
                'action' => action('FileController@edit', ['id '=> $files['file']->id]),
            ]
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userId = Auth::user()->id;
        $file = File::with('preview')->where(['id' => $id])->first();

        if(!is_null($file->preview)) {
            $file->previewPath = 'assets/media/users/'.$userId.'/3/'.$file->preview->alias.'.jpg';
        } else {
            $type = $file->type_id == 1 ? 'video' : 'audio';
            $file->previewPath = config('filesystems.default_path.'.$type);
        }
        $file->path = $this->fileService->getFile($userId, $file->type_id, $file->alias);
        return view('profile.libs.edit', ['file' => $file]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:19456',
            'file_from_lib' => 'integer',
            'title' => 'string',
            'description' => 'string'
        ]);

        $preview_file_id = '';

        // Превью из библиотеки
        if (!empty($request->get('file_from_lib'))) {
            $preview_file_id = intval($request->get('file_from_lib'));
        }

        // Добавляем привью
        if ($request->hasFile('file') && empty($preview_file_id)) {

            $preview = $request->file('file');
            //$alias = $id.'-f';
            $alias = time();

            $path = public_path('assets/media/users').'/'.$userId.'/3/'.$alias.'.jpg';

            $image = Image::make($preview->getRealPath())->encode('jpg');
            $image->save($path);

            // СОХРАНЯЕМ ФАЙЛ
            $file = new File();
            $data = [
                'user_id' => $userId,
                'alias' => $alias,
                'type_id' => 3,
                'title' => $alias,
            ];
            $prevObj = $file->create($data);

            $preview_file_id = $prevObj->id;
        }

        // РАБОТАЕМ С ФАЙЛОМ
        $file = File::find($id);
        $data = [
            'preview_id' => !empty($preview_file_id) ? $preview_file_id : $file->preview_id,
            'title' => !empty($request->get('title')) ? $request->get('title') : $file->title,
            'description' => !empty($request->get('description')) ? trim($request->get('description')) : $file->description,
        ];
        $fileObj = $file->update($data);

        return back()->with('message','main.messages.updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
