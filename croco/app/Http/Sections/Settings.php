<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

class Settings extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Настройки сайта';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var \App\Setting
     */
    protected $model;


    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Событие срабатываемое в процессе создания записи (В случае если метод возвращает false, запись не будет создана)
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
        
        // Событие срабатываемое в процессе редактирования записи (В случае если метод возвращает false, запись не будет создана)
        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
           ->setHtmlAttribute('class', 'table-primary')
           ->setColumns(
               AdminColumn::text('id', 'ID')->setWidth('100px'),
               AdminColumn::text('name', 'Ключ')->setWidth('100px'),
               AdminColumn::text('value', 'Значение')->setWidth('100px')
           )->paginate(50);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Ключ')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('value', 'Значение')
                        ->required()
                        ->addValidationRule('max:255')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Ключ')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('value', 'Значение')
                        ->required()
                        ->addValidationRule('max:255')
                        ->addValidationRule('unique:settings')
        ]);
    }
}
