<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

class Langs extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Языки сайта';

    /**
     * @var string
     */
    protected $icon = 'fa fa-language';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var \App\Lang
     */
    protected $model;


    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Событие срабатываемое в процессе создания записи (В случае если метод возвращает false, запись не будет создана)
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
        
        // Событие срабатываемое в процессе редактирования записи (В случае если метод возвращает false, запись не будет создана)
        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
        
        // В этом месте можно проверять наличие/создавать папку и файлы с языками
        $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // dd($model->lkey);  // "en"          
        });
        
        // В этом месте можно проверять наличие/создавать папку и файлы с языками
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // dd($model->lkey);  // "en"          
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
           ->setHtmlAttribute('class', 'table-primary')
           ->setColumns(
               AdminColumn::text('id', 'ID')->setWidth('100px'),
               AdminColumn::text('lkey', 'Ключ')->setWidth('100px'),
               AdminColumn::text('cname', 'Название (рус)')->setWidth('100px'),
               AdminColumn::text('ownname', 'Название (собственное)')->setWidth('100px'),
               AdminColumn::text('priority', 'Порядок вывода')->setWidth('100px'),
               AdminColumn::text('active', 'Включен (1/0)')->setWidth('100px')
           );
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('lkey', 'Ключ (en)')
                        ->required()
                        ->addValidationRule('min:2')
                        ->addValidationRule('max:3')
                        /*->addValidationRule('unique:langs')*/,
            AdminFormElement::text('cname', 'Название (рус)')
                        ->required(),
            AdminFormElement::text('ownname', 'Название (собственное)')
                        ->required(),
            AdminFormElement::text('priority', 'Порядок вывода'),
            AdminFormElement::select('active', 'Включен', [1=>'Да', 0=>'Нет'])
                        ->required()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        // return $this->onEdit(null);
        return AdminForm::panel()->addBody([
            AdminFormElement::text('lkey', 'Ключ (en)')
                        ->required()
                        ->addValidationRule('min:2')
                        ->addValidationRule('max:3')
                        ->addValidationRule('unique:langs'),
            AdminFormElement::text('cname', 'Название (рус)')
                        ->required(),
            AdminFormElement::text('ownname', 'Название (собственное)')
                        ->required(),
            AdminFormElement::text('priority', 'Порядок вывода'),
            AdminFormElement::select('active', 'Включен', [1=>'Да', 0=>'Нет'])
                        ->required()
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
