<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use \Illuminate\Database\Eloquent\Model;

class Pages extends Section
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Статические страницы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
           ->setHtmlAttribute('class', 'table-primary')
           ->setColumns(
               AdminColumn::text('id', 'ID')->setWidth('30px'),
               AdminColumn::text('title', 'Ключ названия')->setWidth('100px'),
               AdminColumn::text('alias', 'Alias')->setWidth('100px'),
               AdminColumn::text('description', 'Ключ description')->setWidth('100px'),
               AdminColumn::text('keywords', 'Ключ keywords')->setWidth('100px'),
               AdminColumn::text('content', 'Ключ content')->setWidth('100px'),
               AdminColumn::text('template', 'Шаблон')->setWidth('100px'),
               AdminColumn::image('bgimg', 'Фоновое изображение')->setWidth('100px'),
               AdminColumn::text('active', 'Опубликована (1/0)')->setWidth('100px'),
               AdminColumn::text('visible', 'Показывать (1/0)')->setWidth('50px'),
               AdminColumn::text('priority', 'Порядок вывода')->setWidth('100px'),
               AdminColumn::custom('Перейти', function(Model $model) {
                    return '<a href="' . url($model->alias) . '" target="_blank">' . trans('main.' . $model->title) . '</a> <i class="fa fa-eye"></i>';
                })->setWidth('150px')
           );
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
        ->setHtmlAttribute('enctype', 'multipart/form-data')
        ->addBody([
            AdminFormElement::text('title', 'Ключ названия**'),
            AdminFormElement::text('alias', 'Alias')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('description', 'Ключ description**')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('keywords', 'Ключ keywords**')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('content', 'Ключ content**')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('template', 'Файл шаблона (без .blade.php)'),
            AdminFormElement::checkbox('visible', 'Показывать'),
            AdminFormElement::checkbox('active', 'Опубликована'),
            AdminFormElement::text('priority', 'Порядок вывода'),
            AdminFormElement::upload('bgimg', 'Фоновое изображение')
                        ->addValidationRule('image')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return AdminForm::panel()
        ->setHtmlAttribute('enctype', 'multipart/form-data')
        ->addHeader([
            AdminFormElement::html('<h3>**В полях ниже укажите языковые ключи, а не сам текст!</h3>'),
        ])
        ->addBody([
            AdminFormElement::text('title', 'Ключ названия**'),
            AdminFormElement::text('alias', 'Alias')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('description', 'Ключ description**')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('keywords', 'Ключ keywords**')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('content', 'Ключ content**')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('template', 'Файл шаблона (без .blade.php)'),
            AdminFormElement::checkbox('visible', 'Показывать'),
            AdminFormElement::checkbox('active', 'Опубликована'),
            AdminFormElement::text('priority', 'Порядок вывода'),
            AdminFormElement::upload('bgimg', 'Фоновое изображение')
                        ->addValidationRule('image')
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
