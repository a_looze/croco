<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

class Categories extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Категории сайта';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var \App\Category
     */
    protected $model;


    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Событие срабатываемое в процессе создания записи (В случае если метод возвращает false, запись не будет создана)
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
        
        // Событие срабатываемое в процессе редактирования записи (В случае если метод возвращает false, запись не будет создана)
        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
           ->setHtmlAttribute('class', 'table-primary')
           ->setColumns(
               AdminColumn::text('id', 'ID')->setWidth('30px'),
               AdminColumn::text('cname', 'Название (для описания)')->setWidth('150px'),
               AdminColumn::text('alias', 'Alias')->setWidth('100px'),
               AdminColumn::text('letter', 'Буквица')->setWidth('100px'),
               AdminColumn::text('langkey', 'Ключ для файлов языков')->setWidth('130px'),
               // AdminColumn::text('parentid', 'Родительская категория (-)')->setWidth('100px'),
               AdminColumn::text('visible', 'Показывать (1/0)')->setWidth('50px'),
               AdminColumn::text('active', 'Включена (1/0)')->setWidth('100px'),
               AdminColumn::text('priority', 'Порядок вывода')->setWidth('100px'),
               AdminColumn::image('bgimg', 'Фоновое изображение')->setWidth('100px')
           );
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('cname', 'Название (для описания)'),
            AdminFormElement::text('letter', 'Буквица')
                        ->required(),
            AdminFormElement::text('alias', 'Alias')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('langkey', 'Ключ для файлов языков')
                        ->required()
                        ->addValidationRule('min:8') // 'main.BOO'
                        ->addValidationRule('max:255'),
            AdminFormElement::checkbox('visible', 'Показывать'),
            AdminFormElement::checkbox('active', 'Включена'),
            AdminFormElement::text('priority', 'Порядок вывода'),
            AdminFormElement::upload('bgimg', 'Фоновое изображение')
                        ->addValidationRule('image')
        ])->setHtmlAttribute('enctype', 'multipart/form-data');
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('cname', 'Название (для описания)'),
            AdminFormElement::text('letter', 'Буквица')
                        ->required()
                        ->addValidationRule('unique:categories'),
            AdminFormElement::text('alias', 'Alias')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255')
                        ->addValidationRule('unique:categories'),
            AdminFormElement::text('langkey', 'Ключ для файлов языков')
                        ->required()
                        ->addValidationRule('min:8') // 'main.BOO'
                        ->addValidationRule('max:255'),
            AdminFormElement::checkbox('visible', 'Показывать'),
            AdminFormElement::checkbox('active', 'Включена'),
            AdminFormElement::text('priority', 'Порядок вывода'),
            AdminFormElement::upload('bgimg', 'Фоновое изображение')
                        ->addValidationRule('image')
        ])->setHtmlAttribute('enctype', 'multipart/form-data');
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
