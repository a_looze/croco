<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;


class Users extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Список пользователей';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var \App\User
     */
    protected $model;


    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        // $this->addToNavigation($priority = 500, function() {
        //     return \App\User::count();
        // });

        // Событие срабатываемое в процессе создания записи (В случае если метод возвращает false, запись не будет создана)
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
        
        // Событие срабатываемое в процессе редактирования записи (В случае если метод возвращает false, запись не будет создана)
        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // return false;            
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()->with('roles')
           ->setHtmlAttribute('class', 'table-primary')
           ->setColumns(
               AdminColumn::text('id', 'ID пользователя')->setWidth('100px'),
               AdminColumn::text('name', 'Имя')->setWidth('100px'),
               AdminColumn::text('email', 'Email')->setWidth('100px'),
               AdminColumn::text('role', 'ID роли')->setWidth('100px'),
               AdminColumn::text('roles.title', 'Ключ роли')->setWidth('100px')
           )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $user = \App\User::where('id', $id)->get();
        if (!$user) {
            $role = $user->role;
        } else {
            $role = '';
        }

        $selectAr = [''=>' Выберите'];
        
        $roles = \App\Role::all();
        foreach ($roles->toArray() as $data) {
            $selectAr[$data['role']] = $data['title'];
        }
        // dd($selectAr);
        // dd($role);

        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('email', 'Email')
                        ->required()
                        ->addValidationRule('email')
                        ->addValidationRule('max:255')/*
                        ->addValidationRule('unique:users')*/,
            AdminFormElement::text('password', 'Пароль (чтобы изменить, введите новый пароль и подтверждение)')
                        /*->required()*/
                        ->addValidationRule('min:6')
                        ->addValidationRule('confirmed'),
            AdminFormElement::text('password_confirmation', 'Пароль для проверки')
                        /*->required()*/,
            AdminFormElement::select('role', 'Роль', $selectAr)
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        // return $this->onEdit(null);
        $selectAr = [''=>' Выберите'];
        
        $roles = \App\Role::all();
        foreach ($roles->toArray() as $data) {
            $selectAr[$data['role']] = $data['title'];
        }
        
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')
                        ->required()
                        ->addValidationRule('min:3')
                        ->addValidationRule('max:255'),
            AdminFormElement::text('email', 'Email')
                        ->required()
                        ->addValidationRule('email')
                        ->addValidationRule('max:255')
                        ->addValidationRule('unique:users'),
            AdminFormElement::text('password', 'Пароль')
                        ->required()
                        ->addValidationRule('min:6')
                        ->addValidationRule('confirmed'),
            AdminFormElement::text('password_confirmation', 'Пароль для проверки')
                        ->required(),
            AdminFormElement::select('role', 'Роль', $selectAr)
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id, \App\Helpers\Scenario $sc)
    {
        $class = $this->getClass();
        $obj = new $class;
        $user = $obj->whereId($id)->first();

        // шлем сообщение об удалении юзера ДО удаления, чтобы сохранились данные
        $sc->managerDeleteUser($id);

        // удаляем все статьи, файлы, комментарии, файлы...
        $files = \App\File::whereUserId($user->id)->delete(); // ->get();
        $articles = \App\Article::whereUserId($user->id)->delete(); // ->get();
        $answers = \App\Answer::whereUserId($user->id)->delete(); // ->get();
        $comments = \App\Comment::whereUserId($user->id)->delete(); // ->get();
        $ul = \App\UserLike::whereUserId($user->id)->delete(); // ->get();
        $ub = \App\Userbonus::whereUserId($user->id)->delete(); // ->get();
        $subs = \App\Subscribe::whereUserId($user->id)->delete(); // ->get();
        $subs2 = \App\Subscribe::whereChannelId($user->id)->delete(); // ->get();
        $el = \App\Eventlogs::whereUserId($user->id)->delete(); // ->get();

        // dd($el);
        
    }

    /**
     * @return void
     */
    public function deleting($id)
    {
        dd($id);//whereId($id)->first());
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
