<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'country_id', 'alias', 'notice_article_like', 'notice_answer_like', 'banner',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isRoot()
    {
        return $this->role === 9;
    }

    public function isManager()
    {
        return $this->role > 5;
    }

    public function roles()
    {
        return $this->hasOne('App\Role', 'role', 'role');
    }

    /**
     * Подписки на меня
     */
    public function mySubs()
    {
        return $this->hasManyThrough('App\User', 'App\Subscribe', 'user_id', 'id');
    }

    /**
     * Мои подписки на других
     */
    public function meSubs()
    {
        return $this->hasManyThrough('App\User', 'App\Subscribe', 'channel_id', 'id');
    }

    /**
     * Сколько людей подписано на канал юзера
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function SubscribedOnUser()
    {
        return $this->hasMany('App\Subscribe', 'channel_id');
    }

    /**
     * Подписки юзера
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function UserSubscribes()
    {
        return $this->hasMany('App\Subscribe');
    }

    public function setAliasAttribute()
    {
        $this->attributes['alias'] = $this->uniqueAlias();
    }

    /**
     * @param $data
     * @return string
     */
    public function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * create unique alias using base_64 and uniqid
     *
     * @param int $limit
     * @return string
     */
    public function uniqueAlias($limit = 7) {

        $data = substr(uniqid(), -$limit);
        return $this->base64url_encode($data);

    }

    // так как в админке не работает подтверждение пароля, принимаем меры
    public function save(array $options = [])
    {
        if ($this->password_confirmation) {
            unset($this->password_confirmation);
        }

        return parent::save($options);
    }
}
