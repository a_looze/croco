<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'user_id', 'article_id', 'preview_file_id', 'author_say', 'file_id'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function attach()
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }

    public function preview()
    {
        return $this->hasOne(File::class, 'id', 'preview_file_id');
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
