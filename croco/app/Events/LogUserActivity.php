<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LogUserActivity extends Event
{
    use SerializesModels;


    public $userId;
    public $eventId;
    public $targetId;
    public $comment;
    public $time;
    public $renew;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $array, $renew=false) 
    {

        $userObj = auth()->user();
        $this->userId = $userObj ? $userObj->id : 0;
        $this->eventId = isset($array['event_id']) ? $array['event_id'] : 0;
        $this->targetId = isset($array['target_id']) ? $array['target_id']: 0;
        $this->comment = isset($array['comment']) ? $array['comment'] : '';
        $this->time = time();
        $this->renew = $renew;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'user_id' => $this->userId,
            'event_id' => $this->eventId,
            'target_id' => $this->targetId,
            'comment' => $this->comment,
            'created_at' => $this->time,
            'renew' => $this->renew,
        ];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
