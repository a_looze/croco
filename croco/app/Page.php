<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use \KodiComponents\Support\Upload;
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'bgimg' => 'image', // or file | upload
    ];


    /**
     * 
     */
    public function getTemplateAttribute($value)
    {
        if (trim($value) == '') {
            return 'text';
        }
        return $value;
    }
}
