<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLike extends Model
{
    public function article()
    {
        return $this->belongsTo('App\Article', 'item_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }
}
