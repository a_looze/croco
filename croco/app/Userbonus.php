<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userbonus extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'bonus_amount',
    ];
}
