<?php
// табло
Route::get('', [
            'as' => 'admin.dashboard', 
            'uses' => '\App\Http\Controllers\AdminDashController@index',
          ]);

// черный список
Route::get('bl', [
            'as' => 'admin.bl', 
            'uses' => '\App\Http\Controllers\AdminBlackListController@index',
          ]);

//  список жалоб
Route::get('claim', [
            'as' => 'admin.claim', 
            'uses' => '\App\Http\Controllers\AdminClaimListController@index',
          ]);

//  список языков
Route::get('setlang', [
            'as' => 'admin.setlang', 
            'uses' => '\App\Http\Controllers\AdminLangSet@index',
          ]);
Route::get('savelangstr', [
            'as' => 'admin.savelangstr', 
            'uses' => '\App\Http\Controllers\AdminLangSet@saveLangStr',
          ]);

// Route::get('', ['as' => 'admin.dashboard', function () {
// 	$content = 'Define your dashboard here.';
// 	return AdminSection::view($content, 'Dashboard');
// }]);
