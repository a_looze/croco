<?php

use SleepingOwl\Admin\Navigation\Page;

// Default check access logic
// на страницы админки можно попасть только если role>5
AdminNavigation::setAccessLogic(function(Page $page) {
       return auth()->user()->isManager();
});

/*Также для разделов меню можно настраивать првила видимости. Процесс проверки прав доступа выглядит следующим образом: каждый объект меню может иметь свое локальное правило проверки прав

AdminNavigation::addPage(\App\Blog::class)->setAccessLogic(function() {
    return auth()->user()->isSuperAdmin();
})*/


return [
    [
        'title' => trans('sleeping_owl::lang.links.index_page'),
        'icon'  => 'fa fa-modx',
        'url'   => url('/'),
        'target' => '_blank',
        'priority'    => 0,
    ],
    [
        'title' => 'Панель',
        'icon'  => 'fa fa-dashboard',
        'url'   => route('admin.dashboard'),
        'priority'    => 1,
    ],
    [
        'title' => 'Черный список',
        'icon'  => 'fa fa-ban',
        'url'   => route('admin.bl'),
        'priority'    => 2,
    ],
    [
        'title' => 'Жалобы',
        'icon'  => 'fa fa-exclamation-triangle',
        'url'   => route('admin.claim'),
        'priority'    => 3,
    ],
    [
        'title' => 'Настройки',
        'icon' => 'fa fa-gears',
        'priority'    => 10,
        'pages' => [
            (new Page(\App\Setting::class))
                ->setIcon('fa fa-th-list')
                ->setTitle('Список')
                ->setPriority(0)
                ->setAccessLogic(function() {
                    return auth()->user()->isRoot();
                }),
            (new Page(\App\Category::class))
                ->setIcon('fa fa-folder-o')
                ->setTitle('Категории')
                ->setPriority(10)
                ->setAccessLogic(function() {
                    return auth()->user()->isRoot();
                }),
            (new Page(\App\Page::class))
                        ->setIcon('fa fa-pencil-square-o ')
                        ->setTitle('Статические страницы')
                        ->setPriority(20),
            (new Page(\App\Lang::class))
                        ->setIcon('fa fa-flag-o ')
                        ->setTitle('Языки сайта')
                        ->setPriority(40),
            (new Page(\App\Event::class))
                ->setIcon('fa fa-bell-o')
                ->setTitle('События')
                ->setPriority(90)
                ->setAccessLogic(function() {
                    return auth()->user()->isRoot();
                }),
        ]
    ],
    [
        'title' => 'Пользователи',
        'icon' => 'fa fa-group',
        'priority'    => 20,
        'pages' => [
            (new Page(\App\User::class))
                ->setIcon('fa fa-user')
                ->setTitle('Список')
                ->setPriority(0),
            (new Page(\App\Role::class))
                ->setIcon('fa fa-group')
                ->setTitle('Роли(*)')
                ->setPriority(10)
        ]
    ],
    [
        'title' => 'Установка языков',
        'icon'  => 'fa fa-braille ',
        'url'   => route('admin.setlang'),
        'priority'    => 30,
    ],
];

//
// AdminNavigation::addPage(\App\User::class)->setTitle('test')->setPages(function(Page $page) {
//    $page
//        ->addPage()
//        ->setTitle('Dashboard')
//        ->setUrl(route('admin.dashboard'))
//        ->setPriority(100);
//
//    $page->addPage(\App\User::class);
// });
//
// // or
//
// AdminSection::addMenuPage(\App\User::class);

// return [
    // [
    //     'title' => 'Панель',
    //     'icon'  => 'fa fa-dashboard',
    //     'url'   => route('admin.dashboard'),
    // ],

    // [
    //     'title' => 'Информация',
    //     'icon'  => 'fa fa-exclamation-circle',
    //     'url'   => route('admin.information'),
    // ],

    // Examples
    // [
    //    'title' => 'Content',
    //    'pages' => [
    //
    //        \App\User::class,
    //
    //        // or
    //
    //        (new Page(\App\User::class))
    //            ->setPriority(100)
    //            ->setIcon('fa fa-user')
    //            ->setUrl('users')
    //            ->setAccessLogic(function (Page $page) {
    //                return auth()->user()->isSuperAdmin();
    //            }),
    //
    //        // or
    //
    //        new Page([
    //            'title'    => 'News',
    //            'priority' => 200,
    //            'model'    => \App\News::class
    //        ]),
    //
    //        // or
    //        (new Page(/* ... */))->setPages(function (Page $page) {
    //            $page->addPage([
    //                'title'    => 'Blog',
    //                'priority' => 100,
    //                'model'    => \App\Blog::class
    //            ));
    //
    //            $page->addPage(\App\Blog::class);
    //        }),
    //
    //        // or
    //
    //        [
    //            'title'       => 'News',
    //            'priority'    => 300,
    //            'accessLogic' => function ($page) {
    //                return $page->isActive();
    //            },
    //            'pages'       => [
    //
    //                // ...
    //
    //            ]
    //        ]
    //    ]
    // ]
// ];