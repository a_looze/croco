<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'article_id', 'tag_id',
    ];

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
