<?php


namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => 'App\Http\Sections\Users',
        \App\Role::class => 'App\Http\Sections\Roles',
        \App\Lang::class => 'App\Http\Sections\Langs',
        \App\Setting::class => 'App\Http\Sections\Settings',
        \App\Category::class => 'App\Http\Sections\Categories',
        \App\Event::class => 'App\Http\Sections\Events',
        \App\Page::class => 'App\Http\Sections\Pages',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
