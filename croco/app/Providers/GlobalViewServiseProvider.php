<?php
/**
 * Заполнение "универсальных" данных, для всех шаблонов
 */

namespace App\Providers;

use Illuminate\Http\Request;

use Auth;
use App\Category;
use App\Page;
use App\Userbonus;
use App\UserAlert;
use App\Http\Requests;

use Illuminate\Support\ServiceProvider;

class GlobalViewServiseProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->runningInConsole() === false) {
            // список категорий
            $catObjAr = Category::where('active', 1)
                                ->where('visible', 1)
                                ->orderby('priority', 'asc')
                                ->get();

            view()->share('cats', $catObjAr);

            // список статических страниц
            $pageObjAr = Page::where('active', 1)
                                ->where('visible', 1)
                                ->orderby('priority', 'asc')
                                ->get();

            view()->share('pages', $pageObjAr);

            // количество бонусов
            // if (request()->is("profile", "profile/*")) {
            //     view()->composer('*', function($view) {
            //         $userBonus = Userbonus::where(['user_id' => Auth::user()->id])->first();
            //         $bonusCount = !$userBonus ? 0 : $userBonus->bonus_amount;
            //         $view->with('bonusCount', $bonusCount);
            //     });
            // }

            view()->composer('*', function($view) {
                $bonusCount = 0;
                $user = Auth::user();
                if (is_object($user)) {
                    $userBonus = Userbonus::where('user_id', $user->id)->first();
                    if (is_object($userBonus)) {
                        $bonusCount = $userBonus->bonus_amount;
                    }
                }
                $view->with('bonusCount', $bonusCount);
            });

            // сообщения юзеру
            view()->composer('*', function($view) {
                $alertCount = 0;
                $user = Auth::user();
                if (is_object($user)) {
                    $userAlerts = UserAlert::where('user_id', $user->id)
                                            ->where('isnew', 1)
                                            ->get();
                    if (count($userAlerts) > 0) {
                        $alertCount = count($userAlerts);
                    }
                }
                $view->with('alertCount', $alertCount);
            });
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
