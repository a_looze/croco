<?php

namespace App\Providers;
use App\Category;
use App\Page;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        if (app()->runningInConsole() === false) {
            // создаем условия для роутинга категорий - (dance|play|...)
            $catCollection = Category::where('active', 1)
                                        ->where('visible', 1)
                                        ->get();
            $tmpAr = [];
            foreach ($catCollection as $cat) {
                $tmpAr[] = $cat->alias;
            }

            $router->pattern('cat', '(' . implode('|', $tmpAr) . ')');

            // создаем условия для роутинга статических страниц
            $pageCollection = Page::where('active', 1)
                                        ->orderby('priority', 'asc')
                                        ->get();
            $tmpAr = [];
            foreach ($pageCollection as $page) {
                $tmpAr[] = $page->alias;
            }        
            $router->pattern('page', '(' . implode('|', $tmpAr) . ')');

            // псевдокатегории добавляем вручную
            $router->pattern('pseudocat', '(rec|new|popul|rev|like)');
        }

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
