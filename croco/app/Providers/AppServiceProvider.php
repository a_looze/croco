<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Lang;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // media article types
        //$typeArr = [1 => 'audio', 2 => 'video', 3 => 'image'];
        //view()->share('types', $typeArr);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
