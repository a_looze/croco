<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class ConfigServiseProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        config([
            'config/models.php',
        ]);
        // or
        view()->share('_mod', Config::get('models'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
