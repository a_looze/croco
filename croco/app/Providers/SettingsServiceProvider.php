<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // $settings = \App\Setting::lists('value', 'name')->all();
        // config()->set('settings', $settings);
        $script = request()->server->get('SCRIPT_FILENAME');
        $isCli = $script === 'artisan' || $script === './artisan';
        if (!$isCli) {
            $settings = \App\Setting::lists('value', 'name')->all();
            config()->set('settings', $settings);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
