<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [
        'rating', 'user_id', 'parent_id', 'target', 'preview_file_id', 'file_id', 'content',
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function attach()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

    public function preview()
    {
        return $this->hasOne(File::class, 'id', 'preview_file_id');
    }

    public function article()
    {
        return $this->belongsTo(Article::class, 'parent_id');
    }
}
