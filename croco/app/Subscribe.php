<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function channel()
    {
        return $this->belongsTo(User::class, 'channel_id', 'id'); 
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'channel_id', 'id'); 
    }
}
