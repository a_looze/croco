<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;

class Eventlogs extends Model
{

    // public $timestamps = false;

    protected $fillable = [
        'user_id', 'event_id', 'target_id', 'created_at', 'comment',
    ];

    public $events = [];

    /**
     * 
     */
    public function __construct( array $attributes = array())
    {
        $eventCollection = Event::all();
        foreach ($eventCollection as $e) {
            $this->events[$e->id] = $e;
        }

        parent::__construct($attributes);
    }

    
    /**
     * Преобразуем в текстовое описание
     * @todo доделать
     */
        
    // public function getCommentAttribute($value)
    // {
        // if (trim($value) != '') {
        //     $tmp = $value;
        //     $value = '';
        // } else {
        //     //
        // }
        // dd($this);
        
        // $tn = $this->target(User::class)->first();
        // dd($un);
        // switch ($this->event_id)
        // {
        //     case '3':
        //     case '4':
        //     case '5':
        //     case '6':
        //         return 'Юзер ' . $this->user_id . ': ' . $this->events[$this->event_id]['name'] . ' ' . $this->target_id 
        //                  . '; баллы ' . $this->events[$this->event_id]['bcost'];
        //     break;

        //     // case '15':
        //     //     dd($this);
        //     //     // dd($this->target(User::class)->get());
        //     //     // dd($this->buser()->get());
        //     //     return $this->event->name . '(' . $this->buser->name . ', ' . $this->target->name . ')';
        //     // break;

        //     default:
        //         return 'Юзер ' . $this->user_id . ': событие# ' . $this->event_id . ' == ' . $value;
        //     break;
        // }
        // return ucfirst($value);

        // $en = $this->event()->first()->name;
        // $un = $this->buser()->first()->name;

        // return $this->created_at->format('d.m.Y H:i') . ': ' . $en . ' (' . $un . '=>#' . $this->target_id . '), [*' . $value . ']';
        // $this->event()->first()
        // return $this->event()->first()->comment;
    // }

    public function buser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function target($class)
    {
        return $this->belongsTo($class, 'target_id', 'id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
