<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class File extends Model
{
    // public $timestamps = false;

    protected $fillable = [
        'type_id', 'user_id', 'alias', 'title', 'preview_id', 'title', 'description',
    ];

    private $thumbsDir = 'images/thumbs/';

    public function resize($w=100, $h=100)
    {
        $src = public_path() . '/' . config('settings.nophoto_path');
        $dir = $this->thumbsDir;
        $alias = 'nofoto';

        // перекомментировать для реальных данных
        // $src = public_path() . '/files/' . $this->user_id . '/3/' . $this->alias . '.jpg';
        //$path = public_path('assets/media/users/') . $article->user_id . '/'. $file->type_id . '/';
        $srcTest = public_path('assets/media/users/') . $this->user_id . '/3/' . $this->alias . '.jpg';

        if ($this->type_id == 3 && file_exists($srcTest)) {
            $src = $srcTest;
            $dir = $dir . $this->user_id . '/';
            $alias = $this->alias;
        }

        $resName = $alias . '-' . $w . '-' . $h . '.jpg';
        $resPath = $dir . $resName;
        $resFullPath = public_path() . '/' . $resPath;        

        if (!file_exists($resFullPath)) {
            if (!file_exists(public_path() . '/' . $dir)) {
                mkdir(public_path() . '/' . $dir);
            }
            $img = Image::make($src);
            $img->fit($w, $h)->save($resFullPath);
            $img->destroy();
        }

        return $resPath;
    }

    public function preview()
    {
        return $this->hasOne(File::class, 'id', 'preview_id');
    }
}
