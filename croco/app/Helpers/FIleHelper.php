<?php
/**
 * Created by PhpStorm.
 * User: user-1
 * Date: 30.09.2016
 * Time: 15:21
 */

namespace App\Helpers;

use Auth;
use App\File;
use Image;

class FileHelper {


    public $types = [
        1 => ['mp4', 'wmv', 'mpeg', 'ogv'],
        2 => ['mp3', 'mp4', 'oga', 'wma',],
        3 => ['jpg', 'jpeg', 'png', 'gif',]
    ];

    public $files;

    /**
     * Определяем type id по расширению файла
     *
     * @param $ext
     * @return bool|int|string
     */
    public function getTypeByExtension($ext)
    {
        $types = $this->types;
        foreach ($types as $typeId => $extArr) {
            if(in_array($ext, $extArr)) return $typeId;
        }

        return false;
    }

    /**
     * Получаем путь к файлу если есть
     *
     * @param $path
     * @param $type
     * @param $alias
     * @return bool|string
     */
    public function getFile($userId, $typeId, $alias)
    {
        foreach($this->types[$typeId] as $type) {
            $fullPath = public_path('assets/media/users').'/'.$userId.'/'.$typeId.'/'.$alias.'.'.$type;
            //dd($fullPath);
            if (file_exists($fullPath)) {
                return 'assets/media/users/'.$userId.'/'.$typeId.'/'.$alias.'.'.$type;
            }
        }
        return false;
    }

    /**
     * Получаем путь к изображению если есть
     *
     * @param $path
     * @param $type
     * @param $alias
     * @return bool|string
     */
    public function getImage($userId, $typeId, $alias)
    {
        foreach($this->types[$typeId] as $type) {
            $fullPath = public_path('assets/media/users').'/'.$userId.'/'.$typeId.'/'.$alias.'.jpg';
            //dd($fullPath);
            if (file_exists($fullPath)) {
                return 'assets/media/users/'.$userId.'/'.$typeId.'/'.$alias.'.jpg';
            }
        }
        return false;
    }

    /**
     * ПУть к файлу на основе id файла
     *
     * @param $id
     * @return string
     */
    public function getPathByFileId($id)
    {
        $file = File::find($id);
        return 'assets/media/users/'.$file->user_id.'/'.$file->type_id.'/'.$file->alias;
    }

    /**
     * Получает массив файлов
     *
     * @param $path
     * @return array
     */
    public function scanDir($path)
    {
        return array_diff(scandir($path), array('..', '.'));
    }

    /**
     * Получаем все файлы из библиотеки пользователя
     * формирует привью по умолчанию
     *
     * @param null $userId
     *
     * @return array
     */
    public function getUserLib($userId = null)
    {

        $userId = empty($userId) ? Auth::user()->id : intval($userId);
        $filesArr = File::where(['user_id' => $userId])->get()->toArray();

        $files = [];
        foreach($filesArr as $file) {
            $filePath = $this->getFile($file['user_id'], $file['type_id'], $file['alias']);
            /*if(!$filePath || (strpos($file['alias'], '-a') !== false) || (strpos($file['alias'], '-f') !== false)) { continue; }*/

            if (!$filePath) { continue; }

            if (
                strpos($file['alias'], '-a') !== false 
                || strpos($file['alias'], '-f') !== false
            ) 
            {
                $file['path'] = $filePath;
                $pFiles[$file['id']] = $file;
            } else {
                $file['path'] = $filePath;
                $files[$file['id']] = $file;
                $pFiles[$file['id']] = $file;
            }
            
        }
        
        // формируем привью для файлов
        foreach($files as $key => $file) {
            $files[$key]['preview'] = '';
            $id = $file['preview_id'];
            if (!empty($id) && $file['type_id'] == 1 && isset($pFiles[$id])) {
                $files[$key]['preview'] = $pFiles[$id]['path'];
            } elseif ($file['type_id'] == 3) {
                $files[$key]['preview'] = $file['path'];
            } else {
                // дефолтные превью для аудио и видео
                $type = $file['type_id'] == 1 ? 'video' : 'audio';
                $files[$key]['preview'] = config('filesystems.default_path.'.$type);
            }
        }

        return $files;
    }

    /**
     * Список файлов при аякс запросе
     *
     * @param null $userId
     * @return array
     */
    public function getUserLibAjax($sortBy, $dir, $userId = null) {

        $userId = empty($userId) ? Auth::user()->id : intval($userId);

        $filesArr = File::where(['user_id' => $userId])
            ->orderBy($sortBy, $dir)
            ->get()
            ->toArray();

        //dd($filesArr);

        $files = [];
        $fileIdArr = [];
        foreach($filesArr as $file) {
            $filePath = $this->getFile($file['user_id'], $file['type_id'], $file['alias']);
            if(!$filePath) { continue; }
            $file['path'] = $filePath;
            $files[] = $file;
            $fileIdArr[$file['id']] = $file['path'];
        }

        // формируем привью для файлов
        foreach($files as $key => $file) {
            $files[$key]['preview'] = '';
            $previewId = $file['preview_id'];
            if (!empty($previewId) && $file['type_id'] != 3 && isset($fileIdArr[$previewId])) {
                $files[$key]['preview'] = $fileIdArr[$previewId];
            } elseif ($file['type_id'] == 3) {
                $files[$key]['preview'] = $file['path'];
            } else {
                // дефолтные превью для аудио и видео
                $type = $file['type_id'] == 1 ? 'video' : 'audio';
                $files[$key]['preview'] = config('filesystems.default_path.'.$type);
            }
        }

        return $files;
    }


    public function draw()
    {

    }

} 