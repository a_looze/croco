<?php
namespace App\Helpers;
use Image;

class ImgHelper extends Helper
{
    protected static $thumbsDir = 'images/thumbs/';

    // public $alias = 'Img';

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    /**
     * Хелпер для преобразования картинок на лету
     * Usage:
     * ImgHelper::resize('from/source.png', 400, 200, 'to/dest.jpg')
     * или с учетом алиаса для класса
     * Img::resize('from/source.png', 400, 200, 'to/dest.jpg')
     */
    public static function resize($src, $w=100, $h=100, $dest='')
    {
        // исходим из мнения, что файлы будут лежать в папке public
        // if (is_object($src)) {
        //     $src = $src->alias . '.jpg';
        // }
        if (!file_exists($src)) {
            $src = public_path() . '/' . $src;
            if (!file_exists($src)) {
                $src = public_path() . '/' . config('settings.nophoto_path');
            }
        }

        // dd($src);

        $pathAr = pathinfo($src);
        /*array:4 [▼
          "dirname" => "files"
          "basename" => "51c750158c72c.jpg"
          "extension" => "jpg"
          "filename" => "51c750158c72c"
        ]*/

        if ($dest != '') {
            // !!!!не обрабатываем ситуацию, когда папки назначения не сущетвует
            // или если не указано имя файла для сохранения
            // хотя надо бы...
            $resPath = $dest;
            $resFullPath = public_path() . '/' . $dest;
        } else {
            $dir = self::$thumbsDir;
            $resName = $pathAr['filename'] . '-' . $w . '-' . $h . '.jpg';
            $resPath = $dir . $resName;        
        }

        $resFullPath = public_path() . '/' . $resPath; 

        if (!file_exists($resFullPath)) {
            if (!file_exists(public_path() . '/' . $dir)) {
                mkdir(public_path() . '/' . $dir);
            }
            $img = Image::make($src);
            if ($h === null || $w === null) {
                $img->resize($w, $h, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($resFullPath); 
            } else {
                $img->fit($w, $h)->save($resFullPath);
            }
            
            $img->destroy();
        }

        return $resPath;
    }
    
}