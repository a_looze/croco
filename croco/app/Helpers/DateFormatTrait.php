<?php
namespace App;
/**
 * Это тестовый трейт
 * Он в действующих моделях не нужен, но может быть подключен
 */

trait DateFormatTrait
{
    function getCreatedAtAttribute()
    {
        return '~~'; // . $this->created_at;
    }
}