<?php
/**
 * Created by PhpStorm.
 * User: user-1
 * Date: 13.10.2016
 * Time: 11:43
 */

namespace App\Helpers;

use Auth;
use App\File;
use Image;

class FileStore {

    public function storeFileWithPreview($data)
    {
        $file = $this->storeFile($data);

        $previewData = $data;
        $previewData['alias'] = $file->id.'-f';
        $previewData['type_id'] = 3;

        // привью для библиотеки
        $previewLib = $this->storePreviewFile($previewData);

        // привью для фронта
        $previewData['alias'] = $file->id.'-a';
        $previewFront = $this->storePreviewFile($previewData);

        // обновляем данные файла в библиотеке
        $file->alias = $file->id;
        $file->preview_id = $previewLib->id;
        $file->save();

        return ['file' => $file, 'previewLib' => $previewLib, 'previewFront' => $previewFront];
    }

    public function storeFile($data)
    {
        return $this->store($data);
    }

    public function storePreviewFile($data)
    {
        return $this->store($data);
    }

    public function store($data)
    {
        $fileModel = new File();
        $obj = $fileModel->create($data);
        return $obj;
    }


    /**
     * Перемещаем изображение
     * создаем привью для фронта и библиотеки
     *
     * @param array $data
     */
    public function moveImage(array $data)
    {
        $dir = public_path('assets/media/users').'/'.$data['user_id'].'/3';

        $result = $this->createDir($dir);

        $path = public_path('assets/media/users').'/'.$data['user_id'].'/3/'.$data['file_id'].'.jpg';
        $previewFrontPath = public_path('assets/media/users').'/'.$data['user_id'].'/3/'.$data['file_id'].'-a.jpg';
        $previewLibPath = public_path('assets/media/users').'/'.$data['user_id'].'/3/'.$data['file_id'].'-f.jpg';

        //$preview = Image::make($file->getRealPath())->fit('800', '500');
        $preview = Image::make($data['real_path']);

        // move images
        $preview->save($previewFrontPath);
        $preview->save($previewLibPath);

        $image = Image::make($data['real_path'])->encode('jpg');
        $image->save($path);
    }

    /**
     * Создает директорию, если нет
     *
     * @param $path
     * @param int $mode
     * @param bool $recursive
     * @return bool
     */
    public function createDir($path, $mode = 0777, $recursive = true)
    {
        if (!is_dir($path)) {
            return mkdir($path, $mode, $recursive);
        }
        return true;
    }


} 