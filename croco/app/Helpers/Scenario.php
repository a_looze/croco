<?php

namespace App\Helpers;

use Auth;
use Cookie;
use App\Category;
use App\Event;
use App\Events\LogUserActivity;
use App\Subscribe;
use App\Article;
use App\User;
use App\Userbonus;
use App\UserAlert;
use App\UserLike;
use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;

class Scenario {

    public $events;

    // data form db
    // public $targetObj;

    // id - channel, category, article
    public $target;


    public $sellerId;
    public $buyerId;

    public $seller;
    public $buyer;

    // public $subscribeType;


    public function __construct()
    {
        $this->getEvents();
        $this->setBuyerUser();
        //dd($this->events);
    }

    /**
     * =============================
     * WORK METHODS
     * =============================
     */

    /**
     * подписки/отписки
     */
    public function makeSubscribe($type)
    {
        $events = [
            'channel' => [3, 4],
            'category' => [5, 6],
        ];

        $targetUrlChunk = request()->input('from');

        if (!$this->getSubscribeTargetOrFail($targetUrlChunk, $type)) { 
            return false; 
        }

        $subsObj = Subscribe::where('channel_id', $this->target->id)
                            ->where('user_id', $this->buyer->id)
                            ->where('type', $type)
                            ->first();

        if (!$subsObj) {
            $subsObj = Subscribe::create();
            $subsObj->user_id = $this->buyer->id;
            $subsObj->channel_id = $this->target->id;
            $subsObj->type = $type; // category, channel
            $subsObj->save();
            $tp = 0;
        } else {
            $subsObj->delete();
            $tp = 1;
        }

        // начисляем бонусы
        $bCost = $this->refreshBonus($events[$type][$tp], $this->buyer, 'bcost');
        if ($type == 'channel') {
            $sCost = $this->refreshBonus($events[$type][$tp], $this->seller, 'scost');
        }
        

        // готовим сообщения
        if ($type == 'channel') {
            $buyerMessage = 'main.alert.buyer.' . $events[$type][$tp];
            $buyerData = ['seller' => $this->channelLink($this->seller), 'bonus' => abs($bCost)];
            $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

            $sellerMessage = 'main.alert.seller.' . $events[$type][$tp];
            $sellerData = ['buyer' => $this->channelLink($this->buyer), 'bonus' => abs($sCost)];
            $this->sendAlert($this->seller, $sellerMessage, $sellerData);

            $adminData = [
                        'buyer' => $this->channelLink($this->buyer),
                        'seller' => $this->channelLink($this->seller),
                        'sbonus' => abs($sCost),
                        'bbonus' => abs($bCost),
                        ];
        } else {
            $buyerMessage = 'main.alert.buyer.' . $events[$type][$tp];
            $buyerData = ['category' => $this->categoryLink($this->target), 'bonus' => abs($bCost)];
            $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

            $adminData = [
                        'buyer' => $this->channelLink($this->buyer),
                        'category' => $this->categoryLink($this->target),
                        'bbonus' => abs($bCost),
                        ];
        }

        $adminMessage = 'main.alert.admin.' . $events[$type][$tp];
        $comment = trans($adminMessage, $adminData);

        // пишем лог событий
        $data = [
                'event_id' => $events[$type][$tp], 
                'target_id' => $this->target->id,
                'comment' => $comment,
            ];
        event(new LogUserActivity($data));
        return $data;
    }

    /**
     * Добавляем бонусы за оценку автором ответа
     */
    public function changeAnswerResolution($answer, $resolution)
    {
        $this->target = $answer;
        // в данном случае активный юзер не buyer, а seller
        $this->seller = $this->buyer;
        $this->buyer = $answer->author()->first();        
        $eventId = 21 - $resolution; // 20 для "1", 21 для "0"

        // начисляем бонусы
        $bCost = $this->refreshBonus($eventId, $this->buyer, 'bcost');
        
        $buyerMessage = 'main.alert.buyer.' . $eventId;
        $buyerData = [
                        'seller' => $this->channelLink($this->seller),
                        'article' => $this->articleLinkByAnswer($this->target), 
                        'bonus' => abs($bCost)
                    ];

        $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

        $adminData = [
                    'buyer' => $this->channelLink($this->buyer),
                    'seller' => $this->channelLink($this->seller),
                    'article' => $this->articleLinkByAnswer($this->target),
                    'bbonus' => abs($bCost),
                    ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем в лог
        $data = [
                 'event_id' => $eventId,
                 'target_id' => $this->target->id,
                 'comment' => $comment,
                ];

        event(new LogUserActivity($data));
    }

    /**
     * Лайк/дизлайк
     */
    // public function changeRating(User $user, $target, $target_id, $action)
    public function changeRating($target, $target_id, $action)
    {
        /*
            7   Лайк вопроса
            8   Дизлайк вопроса
            9   Лайк ответа
            10  Дизлайк ответа
            11  Лайк комментария
            12  Дизлайк комментария
        */
        $actAr = [
            'article' => [
                'inc' => 7,
                'dec' => 8,
                'class' => \App\Article::class,
                'txt' => 'notice_article_like',
                // 'dectxt' => 'eventdislike',
            ],
            'answer' => [
                'inc' => 9,
                'dec' => 10,
                'class' => \App\Answer::class,
                'txt' => 'notice_answer_like',
                // 'dectxt' => 'eventadislike',
            ],
            'comment' => [
                'inc' => 11,
                'dec' => 12,
                'class' => \App\Comment::class,
                'txt' => 'notice_comment_like',
                // 'dectxt' => 'eventcdislike',
            ],
        ];

        $eventId = $actAr[$target][$action];
        $this->target = $actAr[$target]['class']::where('id', $target_id)->first();
        if (!is_object($this->target)) {
            dd('Target log error'); // да, вот так вот жестоко
        }
        $this->seller = $this->target->author()->first();

        // так как у комментариев и ответов нет собственной ссылки, получаем ссылку на вопрос
        $link = $this->articleLinkByAnswer($this->target);

        // если у данного юзера это лайк №1000, начисляем бонус
        $likesCollection = UserLike::where('user_id', $this->buyer->id)->get();
        if (count($likesCollection) == 1000) {
            $bCost = $this->refreshBonus($eventId, $this->buyer, 'bcost');

            if ($this->buyer->$actAr[$target]['txt'] > 0) {
                $buyerMessage = 'main.alert.buyer.' . $eventId;
                $buyerData = [
                                'bonus' => abs($bCost)
                            ];
                $this->sendAlert($this->buyer, $buyerMessage, $buyerData);
            }
        } else if (count($likesCollection) == 999 && $action == 'dec') {
            $bCost = $this->refreshBonus($eventId, $this->buyer, 'bcost');

            if ($this->buyer->$actAr[$target]['txt'] > 0) {
                $buyerMessage = 'main.alert.buyer.' . $eventId;
                $buyerData = [
                                'bonus' => abs($bCost)
                            ];
                $this->sendAlert($this->buyer, $buyerMessage, $buyerData);
            }
        } else {
            $bCost = 0;
        }

        if ($this->seller->$actAr[$target]['txt'] == 1) {
            $sellerMessage = 'main.alert.seller.' . $eventId;
            $sellerData = [
                            'buyer' => $this->channelLink($this->buyer), 
                            'article' => $link,
                        ];
            $this->sendAlert($this->seller, $sellerMessage, $sellerData);
        }
        

        $adminData = [
                    'buyer' => $this->channelLink($this->buyer),
                    'article' => $link,
                    'bbonus' => abs($bCost),
                    ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем в лог
        $data = [
                 'event_id' => $eventId,
                 'target_id' => $this->target->id,
                 'comment' => $comment,
                ];

        event(new LogUserActivity($data));
    }


    /**
     * Просмотр темы
     * !! добавить начисление бонусов за 100 000 просмотров
     */
    public function viewArticle(Article $article)
    {
        $article->views++;
        $article->save();

        // если юзер авторизован, обновляем для него последние просмотры
        // 20.10.2016 alooze - нет необходимости пока что писать эти данные в лог событий
        // $userObj = auth()->user();
        // if (is_object($userObj)) {
        //     $data = ['event_id' => 13,
        //              'target_id' => $article->id,
        //             ];

        //     event(new LogUserActivity($data, $renew = true));
        // }

        // для любого посетителя дописываем в куку вопрос к последним просмотренным
        $vList = request()->cookie('ut_vql');
        // dd($vList);
        if (!$vList || trim($vList) == '') {
            $vList = $article->id;
        } else {
            $tmpAr = explode('|', $vList);
            $vAr = [];
            foreach ($tmpAr as $qId) {
                if (intval($qId) > 0) {
                    $vAr[$qId] = $qId;
                }
            }
            $vAr[$article->id] = $article->id;
            $vList = implode('|', $vAr);
        }
        Cookie::queue('ut_vql', $vList, 14400); // 100 дней
    }

    /**
     * Бан юзера
     */
    public function banUser($userObj)
    {
        $eventId = 26;

        $this->seller = $this->buyer;
        $this->buyer = $userObj;
        $this->target = null;

        $buyerMessage = 'main.alert.buyer.' . $eventId;
        $buyerData = [];
        $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

        $adminData = [
                    'buyer' => $this->channelLink($this->buyer),
                    ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем в лог
        $data = [
                 'event_id' => $eventId,
                 'target_id' => $this->buyer->id, // хз
                 'comment' => $comment,
                ];

        event(new LogUserActivity($data));
    }

    /**
     * Удаление вопроса, ответа, коммента
     */
    public function rmObj($userObj)
    {
        $actAr = [
            'article' => 23,
            'answer' => 24,
            'comment' => 25,
        ];

        $eventId = $actAr[request()->input('target')];

        $this->seller = $this->buyer;
        $this->buyer = $userObj;
        $this->target = null;

        $buyerMessage = 'main.alert.buyer.' . $eventId;
        $buyerData = [];
        $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

        $adminData = [
                    'buyer' => $this->channelLink($this->buyer),
                    ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем в лог
        $data = [
                 'event_id' => $eventId,
                 'target_id' => $this->buyer->id, // фиг знает что сюда писать
                 'comment' => $comment,
                ];

        event(new LogUserActivity($data));
    }

    /**
     * Добавляем бонус юзеру за создание вопроса
     *
     * @param $articleId
     */
    public function addBonusForArticle($articleObj)
    {
        $eventId = 16; // add article
        $this->target = $articleObj;
        

        $bCost = $this->refreshBonus($eventId, $this->buyer);
        $buyerMessage = 'main.alert.buyer.' . $eventId;
        $buyerData = [
                        'article' => $this->articleLink($articleObj),
                        'bonus' => abs($bCost)
                    ];
        $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

        $catId = $articleObj->category_id;
        $catObj = Category::whereId($catId)->first();

        // вместо seller используем пользователей, подписанных на категорию/канал
        $channelId = $this->buyer->id;
        $usersSubs = Subscribe::where('channel_id', $channelId)
                            ->where('type', 'channel')
                            ->get();
        foreach ($usersSubs as $userSub) {
            $sellerMessage = 'main.alert.seller.' . $eventId;
            $sellerData = [
                            'buyer' => $this->channelLink($this->buyer), 
                            'category' => $this->categoryLink($catObj), 
                            'article' => $this->articleLink($articleObj),
                        ];
            $this->sendAlert($userSub->owner()->first(), $sellerMessage, $sellerData);
        }

        

        $usersSubs = Subscribe::where('channel_id', $catId)
                            ->where('type', 'category')
                            ->get();
        
        
        foreach ($usersSubs as $userSub) {
            $sellerMessage = 'main.alert.seller.' . $eventId;
            $sellerData = [
                            'buyer' => $this->channelLink($this->buyer), 
                            'category' => $this->categoryLink($catObj), 
                            'article' => $this->articleLink($articleObj),
                        ];
            $this->sendAlert($userSub->owner()->first(), $sellerMessage, $sellerData);
        }

        $adminData = [
                        'buyer' => $this->channelLink($this->buyer),
                        'article' => $this->articleLink($articleObj),
                        'bbonus' => abs($bCost),
                        ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем лог событий
        $data = [
                'event_id' => $eventId, 
                'target_id' => $this->target->id,
                'comment' => $comment,
            ];
        event(new LogUserActivity($data));
    }

    /**
     * Добавляем бонус юзеру за оставленный комментарий
     *
     * @param $commentId
     */
    public function addBonusForComment($commentObj)
    {
        $eventId = 17; // add comment
        $this->target = $commentObj;
        $article = $this->target->article()->first();
        $this->seller = $article->author()->first();

        $bCost = $this->refreshBonus($eventId, $this->buyer);
        $buyerMessage = 'main.alert.buyer.' . $eventId;
        $buyerData = [
                        'seller' => $this->channelLink($this->seller), 
                        'article' => $this->articleLink($article),
                        'bonus' => abs($bCost)
                    ];
        $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

        $sellerMessage = 'main.alert.seller.' . $eventId;
        $sellerData = [
                        'buyer' => $this->channelLink($this->buyer), 
                        'article' => $this->articleLink($article),
                    ];
        $this->sendAlert($this->seller, $sellerMessage, $sellerData);

        $adminData = [
                        'buyer' => $this->channelLink($this->buyer),
                        'seller' => $this->channelLink($this->seller),
                        'article' => $this->articleLink($article),
                        'bbonus' => abs($bCost),
                        ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем лог событий
        $data = [
                'event_id' => $eventId, 
                'target_id' => $this->target->id,
                'comment' => $comment,
            ];
        event(new LogUserActivity($data));
    }

    /**
     * Добавляем бонус юзеру за ответ на вопрос
     *
     * @param $answerId
     */
    public function addBonusForAnswer($answerObj)
    {
        $eventId = 18; // add answer
        $this->target = $answerObj;
        $article = $this->target->article()->first();
        $this->seller = $article->author()->first();

        // проверяем, не было ли у текущего юзера подсмотренного ответа в данной теме
        $currentCookieVal = request()->cookie('ut_aav'); 
        if (!$currentCookieVal || trim($currentCookieVal) == '') {
            $noBonus = false;
        } else {
            $tmpAr = explode('|', $currentCookieVal);
            if (!in_array($article->id, $tmpAr)) {
                $noBonus = false;
            } else {
                $noBonus = true;
            }
        }

        if (!$noBonus) {
            $bCost = $this->refreshBonus($eventId, $this->buyer);
        } else {
            $bCost = 0;
        }

        $buyerMessage = 'main.alert.buyer.' . $eventId;
        $buyerData = [
                        'seller' => $this->channelLink($this->seller), 
                        'article' => $this->articleLink($article),
                        'bonus' => abs($bCost)
                    ];
        $this->sendAlert($this->buyer, $buyerMessage, $buyerData);

        $sellerMessage = 'main.alert.seller.' . $eventId;
        $sellerData = [
                        'buyer' => $this->channelLink($this->buyer), 
                        'article' => $this->articleLink($article),
                    ];
        $this->sendAlert($this->seller, $sellerMessage, $sellerData);

        $adminData = [
                        'buyer' => $this->channelLink($this->buyer),
                        'seller' => $this->channelLink($this->seller),
                        'article' => $this->articleLink($article),
                        'bbonus' => abs($bCost),
                        ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем лог событий
        $data = [
                'event_id' => $eventId, 
                'target_id' => $this->target->id,
                'comment' => $comment,
            ];
        event(new LogUserActivity($data));
    }

    /**
     * Добавляем бонус юзеру за регистрацию по реферальной ссылке
     *
     * @param $userObj
     */
    public function addBonusForReferrer($userObj)
    {
        $eventId = 22; 
        $this->seller = $userObj;
        $this->target = $this->buyer;

        $sCost = $this->refreshBonus($eventId, $userObj, 'scost');
        $sellerMessage = 'main.alert.seller.' . $eventId;
        $sellerData = [
                        'buyer' => $this->channelLink($this->buyer), 
                        'bonus' => abs($sCost)
                    ];
        $this->sendAlert($this->seller, $sellerMessage, $sellerData);

        $adminData = [
                        'buyer' => $this->channelLink($this->buyer),
                        'seller' => $this->channelLink($this->seller),
                        'article' => $this->articleLink($article),
                        'sbonus' => abs($sCost),
                        ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        // пишем лог событий
        $data = [
                'event_id' => $eventId, 
                'target_id' => $this->target->id,
                'comment' => $comment,
            ];
        event(new LogUserActivity($data));
    }

    /**
     * Уведомления для пользователя
     *
     * @param $userTarget
     * @param $eventId
     * @param $msg
     */
    public function userGotPersonalAsk($userObj, $article)
    {
        $this->seller = $userObj;
        $eventId = 19;

        $sellerMessage = 'main.alert.seller.' . $eventId;
        $sellerData = [
                    'buyer' => $this->channelLink($this->buyer),
                    'article' => $this->articleLink($article),
                    ];

        $this->sendAlert($this->seller, $sellerMessage, $sellerData);

        $adminData = [
                    'buyer' => $this->channelLink($this->buyer),
                    'seller' => $this->channelLink($this->seller),
                    'article' => $this->articleLink($article),
                    ];
        $adminMessage = 'main.alert.admin.' . $eventId;
        $comment = trans($adminMessage, $adminData);

        $data = [
            'event_id' => $eventId,
            'target_id' => $userObj->id,
            'comment' => $comment,
        ];

        event(new LogUserActivity($data));
    }

    public function getEvents()
    {
        $this->events = Event::all();
    }

    /**
     * удаление юзера в админке
     */
    public function managerDeleteUser($id)
    {
        $user = User::whereId($id)->first();
        $comment = $user->name . ' #' . $id . ', ' . $user->email;
        $data = ['event_id' => 15,
                 'target_id' => $id,
                 'comment' => $comment,
                ];

        event(new LogUserActivity($data));
    }

    /**
     * =============================
     * HELPERS
     * =============================
     */

    /**
     * Вспомогательный метод для создания и отправки почты
     * @param $to email
     * @param $subj subject
     * @param array $data массив данных для заполнения шаблона письма
     * $data = ['h1' => '', 'content' => '', 'bonusTxt' => '']
     */
    public function sendEmail($to, $subj, $data)
    {
        return Mail::send('layouts.mail', $data, function ($message) use ($to, $subj) {
            $message->from('no-reply@commfi.net', 'Commfi');
            $message->to($to);
            $message->subject($subj);
        });
    }

    /**
     * Вспомогательный метод для отправки сообщения о событии юзеру в БД
     * @param $user адресат
     * @param $message языковая строка
     * @param $mData дополнительные данные для языковой строки
     */
    public function dbAlert($user, $message, $mData=[])
    {
        if (is_object($user)) {
            $alert = UserAlert::create();
            $alert->user_id = $user->id;
            $alert->comment = $message;
            $alert->comment_data = json_encode($mData);
            $alert->isnew = 1;
            $alert->save();
        }
    }

    /**
     * Отправляем сообщения юзерам о событиях
     */
    public function sendAlert($user, $message, $mData=[])
    {
        // пишем данные в лог для юзера
        $this->dbAlert($user, $message, $mData);

        // для отправки почты готовим подстановщики в шаблон
        $mailData = [
                    'h1' => 'Новое событие на сайте commfi.net',
                    'content' => trans($message, $mData),
                    'bonusTxt' => '', // пока что в резерве, бонусы указаны в теле сообщения
                    ];
        $subj = 'Новости с commfi.net';
        $this->sendEmail($user->email, $subj, $mailData);
    }

    /**
     * Проверям существование цели (канал, категория и т.д.)
     *
     * @param $target
     * @return bool
     * @return object/false пользователь (канал) или категория
     */
    public function getSubscribeTargetOrFail($target, $type)
    {
        $target = trim($target, '/');
        switch ($type) {
            case 'channel':
                // если пришли со страницы канала
                if (substr($target, 0, 2) == 'u/') {
                    list($u, $alias) = explode('/', $target);
                    if (!$alias || $alias == '' || strlen($alias) < 6) {
                        return false;
                    }

                    $userCollection = User::where('alias', $alias)->get();
                    if (count($userCollection) !== 1) {
                        return false;
                    }
                }

                // если пришли со страницы вопроса
                if (substr($target, 0, 2) == 'v/') {
                    list($v, $alias) = explode('/', $target);
                    if (!$alias || $alias == '' || strlen($alias) < 6) {
                        return false;
                    }

                    $article = Article::where('alias', $alias)->first();
                    if (!is_object($article)) {
                        return false;
                    }

                    $userCollection = $article->author()->get();
                    if (count($userCollection) !== 1) {
                        return false;
                    }
                }
                
                $this->target = $userCollection[0];
                $this->setSellerUser($userCollection[0]);
            break;

            case 'category':
                $this->target = Category::where('alias', $target)
                    ->where('active', 1)
                    ->where('visible', 1)
                    ->first();
            break;

            default:
                return false;
            break;
        }

        return $this->target ?: false;
    }

    /**
     * Владелец канала, вопроса, ответа
     *
     * @param $seller
     */
    public function setSellerUser($sellerObj)
    {
        $this->seller = $sellerObj;
        $this->sellerId = $sellerObj->id;
    }


    /**
     * Юзер, который совершает действие
     */
    public function setBuyerUser()
    {
        if (auth()->user()) {
            $this->buyer = auth()->user();
            $this->buyerId = $this->buyer->id;
        } else {
            $this->buyer = null;
            $this->buyerId = 0;
        }
    }

    /**
     * Пересчитываем бонусы
     * !! метод глючный, много возможностей для читерства
     * В случае доработки в первую очередь добавить проверку на наличие события в БД, за которое начисляется бонус
     * Если событие уже было - не начислять; если не было - не вычитать из 0
     *
     * @param $eventId
     * @param $userObj
     * @param string $userCost // bcost - кто, scost - кому
     */
    public function refreshBonus($eventId, $userObj, $userCost = 'bcost')
    {
        $currentEvent = Event::find($eventId);

        $userBonus = Userbonus::firstOrCreate(['user_id' => $userObj->id]);

        if ($currentEvent) {
            $userBonus->bonus_amount += $currentEvent->$userCost;
            $userBonus->save();
            return $currentEvent->$userCost;
        }
        return false;
    }

    /**
     * Создает ссылку для фронта
     *
     * @param $href
     * @param $text
     * @param string $class
     * @param string $id
     * @return string
     */
    public function createHtmlLink($href, $text, $class='', $id='')
    {
        if ($class != '') {
            $class = ' class="' . $class . '"';
        }

        if ($id != '') {
            $id = ' id="' . $id . '"';
        }

        if ($text == '') {
            $text = 'Link';
        }

        return '<a href="'.$href.'"' . $id . $class . '>' . $text . '</a>';
    }

    /**
     * Ссылка на канал юзера
     */
    public function channelLink($user)
    {
        return $this->createHtmlLink(action('UserPagesController@index', ['alias' => $user->alias]), $user->name);
    }

    /**
     * Ссылка на категорию
     */
    public function categoryLink($cat)
    {
        return $this->createHtmlLink(url($cat->alias), trans($cat->langkey));
    }

    /**
     * Ссылка на вопрос по ответу
     */
    public function articleLinkByAnswer($answer)
    {
        return $this->createHtmlLink(url('/v/' . $answer->article->alias), $answer->article->title);
    }

    /**
     * Ссылка на вопрос по ответу
     */
    public function articleLink($article)
    {
        return $this->createHtmlLink(url('/v/' . $article->alias), $article->title);
    }

    public function getSubsCount($subscribeType)
    {
        $subsCollection = Subscribe::where('channel_id', $this->target->id)
                                    ->where('type', $subscribeType)
                                    ->get();
        return count($subsCollection);
    }

} 