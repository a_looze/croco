<?php

namespace App\Helpers;

use App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class Country {

    public $currentLang;

    public function __construct()
    {
        $this->getCurrentLang();
    }

    public function getList()
    {
        $lang = $this->currentLang;
        $lang = 'en';
        include (storage_path('app/countries/'.$lang.'.inc.php'));
        return $_country_lang;
    }

    public function getCurrentLang()
    {
        $this->currentLang = Session::get('locale') ? : config()->get('app.locale');
    }

} 