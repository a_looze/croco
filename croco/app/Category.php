<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \KodiComponents\Support\Upload;
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'bgimg' => 'image', // or file | upload
    ];
}
